// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocalizeRouterModule, LocalizeRouterSettings, LocalizeParser } from '@gilsdav/ngx-translate-router';
import { LocalizeRouterHttpLoader } from '@gilsdav/ngx-translate-router-http-loader';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { SharedNewPasswordComponent } from './shared/components/new-password/new-password.component';
import {​​AuthGuardService as AuthGuard}​​ from './core/services/auth-guard.service';

export function HttpLoaderFactory(translate: TranslateService, location: Location, settings: LocalizeRouterSettings, http: HttpClient) {
  settings.defaultLangFunction = (languages, _cachedLang, browserLang) => {
    return browserLang || languages[1];
  };
  return new LocalizeRouterHttpLoader(translate, location, settings, http, `${environment.apiUrl}/locales/list`);
}

const routes: Routes = [
  {
    path: '',
    redirectTo: 'casino',
    pathMatch: 'full'
  },
  {
    path: 'sportsbook',
    loadChildren: () => import('./sportsbook/sportsbook.module').then(m => m.SportsbookModule)
  },
  {
    path: 'casino',
    loadChildren: () => import('./casino/casino.module').then(m => m.CasinoModule)
  },
  {
    path: 'UpdatePassword',
    component: SharedNewPasswordComponent
  },
  {​​
    path: 'deposit',
    loadChildren: () => import('./shared/components/account/deposit/deposit.module').then(m => m.SharedDepositModule),
    canLoad: [AuthGuard]
  }​​,
  {
    path: '**',
    loadChildren: () => import('./shared/components/not-found/not-found.module').then(m => m.SharedNotFoundModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}),
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: HttpLoaderFactory,
        deps: [TranslateService, Location, LocalizeRouterSettings, HttpClient]
      }
    })
  ],
  exports: [RouterModule, LocalizeRouterModule]
})


export class AppRoutingModule {
}
