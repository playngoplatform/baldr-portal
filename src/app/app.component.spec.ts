// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { TestBed, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SharedHeaderComponent } from './shared/components/header/header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedFooterComponent } from './shared/components/footer/footer.component';
import { SiteTermsComponent } from './casino/shared/layout/site-terms/site-terms.component';
import {
  ALWAYS_SET_PREFIX, CACHE_MECHANISM, CACHE_NAME, DEFAULT_LANG_FUNCTION,
  LocalizeParser,
  LocalizeRouterPipe,
  LocalizeRouterService,
  LocalizeRouterSettings,
  USE_CACHED_LANG
} from '@gilsdav/ngx-translate-router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
import {
  MissingTranslationHandler,
  TranslateCompiler,
  TranslateLoader,
  TranslateParser,
  TranslateService,
  TranslateStore, USE_DEFAULT_LANG, USE_STORE
} from '@ngx-translate/core';
import {ApiService, AppService, AuthenticationService, UtilsService} from './core/services';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ToasterModule} from 'angular2-toaster';

describe('AppComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSmartModalModule,
        HttpClientModule,
        HttpClientTestingModule,
        ToasterModule.forRoot()
      ],
      declarations: [
        AppComponent,
        SharedHeaderComponent,
        SharedFooterComponent,
        SiteTermsComponent,
        LocalizeRouterPipe
      ],
      providers: [
        { provide: USE_DEFAULT_LANG },
        { provide: USE_STORE },
        { provide: USE_CACHED_LANG },
        { provide: ALWAYS_SET_PREFIX },
        { provide: CACHE_MECHANISM },
        { provide: CACHE_NAME },
        { provide: DEFAULT_LANG_FUNCTION },
        LocalizeRouterService,
        LocalizeParser,
        TranslateService,
        TranslateStore,
        TranslateLoader,
        TranslateCompiler,
        TranslateParser,
        MissingTranslationHandler,
        LocalizeRouterSettings,
        AppService,
        ApiService,
        UtilsService,
        AuthenticationService,
        NgxSmartModalService
      ]
    }).compileComponents();
  }));
  it('should create the app', waitForAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, waitForAsync(() => {
    // const fixture = TestBed.createComponent(AppComponent);
    // const app = fixture.debugElement.componentInstance;
    // console.log(app);
    // expect(app.title).toEqual('Frontend');
  }));
  it('should render title in a h1 tag', waitForAsync(() => {
    // const fixture = TestBed.createComponent(AppComponent);
    // fixture.detectChanges();
    // const compiled = fixture.debugElement.nativeElement;
    // expect(compiled.querySelector('h1').textContent).toContain('Welcome to frontend!');
  }));
});
