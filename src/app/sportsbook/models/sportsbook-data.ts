// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import * as moment from 'moment';

export enum SportsbookSocketMessageType {
  MarketUpdated = 0,
  FixtureUpdated = 1,
  CompetitionUpdated = 4
}

export enum SportsbookFixtureType {
  Match = 0,
  Competition = 1
}

export enum SportsbookFixtureOfferedLive {
  Yes = 0,
  No = 1,
  Probably = 2
}

export enum SportsbookFixtureStatus {
  Scheduled = 0,
  Started = 1,
  Finished = 2,
  Discontinued = 3,
  Cancelled = 4
}

export enum SportsbookFixtureParticipantType {
  Team = 0,
  Player = 1,
  Other = 2
}

export enum SportsbookFixtureParticipantStatus {
  Active = 0,
  Inactive = 1
}

export enum SportsbookFixtureParticipantFieldAdvantage {
  Unknown = 0,
  Home = 1,
  Away = 2,
  Neutral = 3,
  NotApplicable = 4
}

export enum SportsbookFixtureMarketAccRestriction {
  Deny = 0,
  DenyWithinFixture = 1,
  Allow = 2
}

export enum SportsbookFixtureMarketStatus {
  Opened = 0,
  OffTheBoard = 1,
  Suspended = 2,
  Removed = 3,
  Closed = 4,
  Cancelled = 5,
  Resulted = 6
}

export enum SportsbookSportCompetitionFixtureMarketTypeLevel {
  Fixture = 0,
  Participant = 1,
  SubParticipant = 2
}

export enum SportsbookSportCompetitionFixtureMarketTypePointsType {
  NotApplicable = 0,
  HandicapHalfPoints = 1,
  TotalHalfPoints = 2,
  Range = 3,
  HandicapWholePoints = 4,
  HandicapWithQuarterPoints = 5,
  TotalWholePoints = 6,
  TotalWithQuarterPoints = 7,
  Rank = 8,
  NonNegativeWholePoints = 9,
  ExactOutcome = 10
}

export enum SportsbookSportCompetitionFixtureMarketSelectionType {
  Over = 0,
  Under = 1,
  Exact = 2,
  Draw = 3,
  FirstParticipant = 4,
  LastParticipant = 5,
  TournamentEntry = 6,
  TheField = 7,
  Yes = 8,
  No = 9,
  Odd = 10,
  Even = 11,
  SubParticipant = 12,
  Range = 13,
  ExactOutcome = 14
}

export enum SportsbookSportCompetitionFixtureMarketSelectionStatus {
  Opened = 0,
  OffTheBoard = 1,
  Suspended = 2,
  Removed = 3,
  Closed = 4,
  Voided = 5,
  Won = 6,
  HalfWonHalfPushed = 7,
  HalfLostHalfPushed = 8,
  Lost = 9,
  Pushed = 10,
  Unverified = 11
}

export class SportsbookMenuItem {
  id: number = null;
  categoryId: number = null;
  categoryName: string = null;
  name: string = null;
  amountCompetitions: number = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
      if (obj['competitions']) {
        this.amountCompetitions = obj['competitions'].length;
      }
    }
  }
}

export class SportsbookDataFeed {
  sourceId: number = null;
  sourceName: string = null;
  sports: SportsbookSport[] = [];

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          if (key === 'sports') {
            for (let index = 0; index < obj.sports.length; index++) {
              this[key][index] = new SportsbookSport(obj.sports[index]);
            }
            continue;
          }
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSport {
  id: number = null;
  categoryId: number = null;
  categoryName: string = null;
  name: string = null;
  periods: SportsbookSportPeriod[] = [];
  units: SportsbookSportUnit[] = [];
  competitions: SportsbookSportCompetition[] = [];

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          switch (key) {
            case 'periods':
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportPeriod(obj[key][index]);
              }
              break;
            case 'units':
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportUnit(obj[key][index]);
              }
              break;
            case 'competitions':
              if (obj[key] === null) {
                this[key] = [];
                break;
              }
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportCompetition(obj[key][index]);
              }
              break;
            default:
              this[key] = obj[key];
              break;
          }
        }
      }
    }
  }
}

export class SportsbookSportPeriod {
  id: number = null;
  sportId: number = null;
  parentId: number = null;
  level: number = null;
  name: string = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportUnit {
  id: number = null;
  sportId: number = null;
  name:  string = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportCompetition {
  id: number = null;
  sportId: number = null;
  competitionName: string = null;
  group: string = null;
  matchUpCount: number = null;
  fixtures: SportsbookSportCompetitionFixture[] = [];

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportCompetitionFixture {
  competitionId: number = null;
  competitionName: string = null;
  ext: any = null; // It represents sport specific properties. (Field is optional)
  fixtureId: number = null;
  fixtureName: string = null;
  fixtureSeqNum: number = null;
  group: string = null;
  markets: SportsbookSportCompetitionFixtureMarket[] = [];
  offeredLive: SportsbookFixtureOfferedLive = null;
  participants: SportsbookSportCompetitionFixtureParticipant[] = [];
  periods: SportsbookSportCompetitionFixturePeriod[] = [];
  scheduledAt: moment.Moment = null;
  sportId: number = null;
  sportName: string = null;
  status: SportsbookFixtureStatus = null;
  subParticipants: SportsbookSportCompetitionFixtureSubParticipant[] = [];
  type: SportsbookFixtureType = null;
  updatedAt: moment.Moment = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          switch (key) {
            case 'periods':
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportCompetitionFixturePeriod(obj[key][index]);
              }
              break;
            case 'participants':
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportCompetitionFixtureParticipant(obj[key][index]);
              }
              break;
            case 'subParticipants':
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportCompetitionFixtureSubParticipant(obj[key][index]);
              }
              break;
            case 'markets':
              if (obj[key] === null) {
                this[key] = [];
                break;
              }
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportCompetitionFixtureMarket(obj[key][index]);
              }
              break;
            default:
              this[key] = obj[key];
              break;
          }
        }
      }
      this.scheduledAt = this.scheduledAt ? moment.utc(this.scheduledAt) : null;
      this.updatedAt = this.updatedAt ? moment.utc(this.updatedAt) : null;
    }
  }
}

export class SportsbookSportCompetitionFixtureMarket {
  accRestriction: SportsbookFixtureMarketAccRestriction = null;
  competitionId: number = null;
  expiresAt: moment.Moment = null;
  fixtureId: number = null;
  isLive: boolean = null;
  marketCategory: number = null;
  marketId: number = null;
  marketName: string = null;
  marketSeqNum: number = null;
  participantId: number = null;
  periodId: number = null;
  points: number = null;
  pricedAt: moment.Moment = null;
  selections: SportsbookSportCompetitionFixtureMarketSelection[] = [];
  sportId: number = null;
  status: SportsbookFixtureMarketStatus = null;
  subParticipantId: number = null;
  type: SportsbookSportCompetitionFixtureMarketType = null;
  unitId: number = null;
  unitName: string = null;
  updatedAt: moment.Moment = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          switch (key) {
            case 'selections':
              for (let index = 0; index < obj[key].length; index++) {
                this[key][index] = new SportsbookSportCompetitionFixtureMarketSelection(obj[key][index]);
              }
              break;
            case 'type':
                this[key] = new SportsbookSportCompetitionFixtureMarketType(obj[key]);
              break;
            default:
              this[key] = obj[key];
              break;
          }
        }
      }
      this.expiresAt = this.expiresAt ? moment.utc(this.expiresAt) : null;
      this.pricedAt = this.pricedAt ? moment.utc(this.pricedAt) : null;
      this.updatedAt = this.updatedAt ? moment.utc(this.updatedAt) : null;
    }
  }
}

export class SportsbookSportCompetitionFixtureMarketSelection {
  altPoints: number = null;
  maxStake: number = null;
  participantId: number = null;
  points: number = null;
  price: number = null; // Odds
  selectionId: number = null;
  selectionName: string = null;
  sortOrder: number = null;
  status: SportsbookSportCompetitionFixtureMarketSelectionStatus = null;
  subParticipantId: number = null;
  type: SportsbookSportCompetitionFixtureMarketSelectionType = null;
  isPriceUpdated: boolean = false;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportCompetitionFixtureMarketType {
  level: SportsbookSportCompetitionFixtureMarketTypeLevel = null;
  marketTypeId: number = null;
  marketTypeName: string = null;
  pointsType: SportsbookSportCompetitionFixtureMarketTypePointsType = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportCompetitionFixturePeriod {
  level: number = null;
  parentId: number = null;
  participantResults: SportsbookSportCompetitionFixturePeriodParticipantResult[] = [];
  periodId: number = null;
  periodName: string = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          if (key === 'participantResults') {
            for (let index = 0; index < obj[key].length; index++) {
              this[key].push(new SportsbookSportCompetitionFixturePeriodParticipantResult(obj[key][index]));
            }
            continue;
          }
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportCompetitionFixturePeriodParticipantResult {
  isRemoved: boolean = null;
  isResulted: boolean = null;
  participantId: number = null;
  result: any = null;
  unitId: number = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportCompetitionFixtureParticipant {
  fieldAdvantage: SportsbookFixtureParticipantFieldAdvantage = null;
  participantId: number = null;
  participantName: string = null;
  sortOrder: number = null;
  status: SportsbookFixtureParticipantStatus = null;
  type: SportsbookFixtureParticipantType = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsbookSportCompetitionFixtureSubParticipant {
  fieldAdvantage: SportsbookFixtureParticipantFieldAdvantage = null;
  participantId: number = null;
  participantName: string = null;
  sortOrder: number = null;
  status: SportsbookFixtureParticipantStatus = null;
  type: SportsbookFixtureParticipantType = null;

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}
