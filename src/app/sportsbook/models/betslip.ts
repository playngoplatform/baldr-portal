// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { SportsbookSportCompetitionFixture, SportsbookSportCompetitionFixtureMarket, SportsbookSportCompetitionFixtureMarketSelection } from './sportsbook-data';

export class SportsbookBetslipItem {
  sportId: number;
  fixtureId: number;
  marketId: number;
  selectionId: number;
  marketSeqNumber: number;
  fixture: SportsbookSportCompetitionFixture;
  market: SportsbookSportCompetitionFixtureMarket;
  selection: SportsbookSportCompetitionFixtureMarketSelection;
}
