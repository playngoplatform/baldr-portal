// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { SportsbookSportCompetition, SportsbookSportCompetitionFixture } from './sportsbook-data';

export class SportsbookCategoryGroup {
  groupName: string = null;
  totalGroupMatchupCount: number = null;
  competitions: SportsbookSportCompetition[] = [];
}

export class CategoryCompetitionFixtureGroup {
  dateGroup: string;
  matchupCount: number;
  fixtures: SportsbookSportCompetitionFixture[] = [];
}
