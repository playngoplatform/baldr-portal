// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export class SportsBookLobbyWidget {
  header: string = null;
  cols: number = null;
  rows: number = null;
  icon: string = null;
  categories: SportsBookLobbyWidgetCategory[] = [];

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          if (key === 'categories') {
            for (let index = 0; index < obj[key].length; index++) {
              this[key].push(new SportsBookLobbyWidgetCategory(obj[key][index]));
            }
            continue;
          }
          this[key] = obj[key];
        }
      }
    }
  }
}

export class SportsBookLobbyWidgetCategory {
  name: string = null;
  sportId: number = null;
  competitionId: number = null;
  marketCategory: number = 1; // default to resultedMarket.

  constructor(obj: any = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}
