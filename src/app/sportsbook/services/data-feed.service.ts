// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import {
  SportsbookDataFeed,
  SportsbookMenuItem,
  SportsbookSport,
  SportsbookSportCompetition,
  SportsbookSportCompetitionFixture,
  SportsbookSportCompetitionFixtureMarket
} from '../models/sportsbook-data';
import { SportsbookEngineService } from './sportsbook-engine.service';

@Injectable({
  providedIn: 'root',
})
export class SportsbookDataFeedService implements OnDestroy {
  private _dataSportUpdateEventSubject: Subject<SportsbookSport> = new Subject();
  private _dataCompetitonUpdateEventSubject: Subject<SportsbookSportCompetition> = new Subject();
  private _dataFixtureUpdateEventSubject: Subject<SportsbookSportCompetitionFixture> = new Subject();
  private _staticSportDataRequest$: Subscription;
  private _competitionFixturesDataRequest$: Subscription;
  private _competitionFixturesAndMarketsDataRequest$: Subscription;
  private _fixtureMarketsDataRequest$: Subscription;
  private _competitionUpdated$: Subscription;
  private _fixtureUpdated$: Subscription;
  private _marketUpdated$: Subscription;
  dataFeed: SportsbookDataFeed = new SportsbookDataFeed();
  sportsMenu: SportsbookMenuItem[] = [];
  dataSportUpdateEvent$: Observable<SportsbookSport>;
  dataCompetitionUpdateEvent$: Observable<SportsbookSportCompetition>;
  dataFixtureUpdateEvent$: Observable<SportsbookSportCompetitionFixture>;

  constructor(
    private _sportsBookEngineService: SportsbookEngineService
  ) {
    this._staticSportDataRequest$ = this._sportsBookEngineService.staticSportDataRequest$.subscribe(res => {
      this._updateStaticDataFeed(res);
    });
    this._competitionFixturesDataRequest$ = this._sportsBookEngineService.competitionFixturesDataRequest$.subscribe(res => {
      this._setCompetitionFixtures(res.sportId, res.competitionId, res.fixtures);
    });
    this._competitionFixturesAndMarketsDataRequest$ = this._sportsBookEngineService.competitionFixturesAndMarketsDataRequest$.subscribe(
      res => {
        this._setCompetitionFixturesWithMarkets(res.sportId, res.competitionId, res.fixturesAndMarkets);
      }
    );
    this._fixtureMarketsDataRequest$ = this._sportsBookEngineService.fixtureMarketsDataRequest$.subscribe(res => {
      this._setFixtureMarkets(res.sportId, res.competitionId, res.fixtureId, res.fixtureMarkets);
    });
    this._competitionUpdated$ = this._sportsBookEngineService.competitionUpdated$.subscribe(res => {
      this._updateCompetition(res);
    });
    this._fixtureUpdated$ = this._sportsBookEngineService.fixtureUpdated$.subscribe(res => {
      this._updateFixture(res);
    });
    this._marketUpdated$ = this._sportsBookEngineService.marketUpdated$.subscribe(res => {
      this._updateMarket(res);
    });
    this.dataSportUpdateEvent$ = this._dataSportUpdateEventSubject.asObservable();
    this.dataCompetitionUpdateEvent$ = this._dataCompetitonUpdateEventSubject.asObservable();
    this.dataFixtureUpdateEvent$ = this._dataFixtureUpdateEventSubject.asObservable();
  }

  private _updateCompetition(competition: SportsbookSportCompetition): void {
    for (let s = 0; s < this.dataFeed.sports.length; s++) {
      if (this.dataFeed.sports[s].id === competition.sportId) {
        for (let c = 0; c < this.dataFeed.sports[s].competitions.length; c++) {
          if (this.dataFeed.sports[s].competitions[c].id === competition.id) {
            competition.fixtures = this.dataFeed.sports[s].competitions[c].fixtures;
            this.dataFeed.sports[s].competitions[c] = competition;
            this._dataCompetitonUpdateEventSubject.next(competition);
            break;
          }
          if (c === this.dataFeed.sports[s].competitions.length - 1) {
            this.dataFeed.sports[s].competitions.push(competition);
            this._dataCompetitonUpdateEventSubject.next(competition);
            this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
            this._setSportsMenu();
            break;
          }
        }
        if (this.dataFeed.sports[s].competitions.length === 0) {
          this.dataFeed.sports[s].competitions.push(competition);
          this._dataCompetitonUpdateEventSubject.next(competition);
          this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
          this._setSportsMenu();
        }
        break;
      }
    }
  }

  private _updateFixture(fixture: SportsbookSportCompetitionFixture): void {
    for (let s = 0; s < this.dataFeed.sports.length; s++) {
      if (this.dataFeed.sports[s].id === fixture.sportId) {
        for (let c = 0; c < this.dataFeed.sports[s].competitions.length; c++) {
          if (this.dataFeed.sports[s].competitions[c].id === fixture.competitionId) {
            for (let f = 0; f < this.dataFeed.sports[s].competitions[c].fixtures.length; f++) {
              if (this.dataFeed.sports[s].competitions[c].fixtures[f].fixtureId === fixture.fixtureId) {
                fixture.markets = this.dataFeed.sports[s].competitions[c].fixtures[f].markets;
                this.dataFeed.sports[s].competitions[c].fixtures[f] = fixture;
                this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
                this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
                this._dataFixtureUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c].fixtures[f]);
                break;
              }
              if (f === this.dataFeed.sports[s].competitions[c].fixtures.length - 1) {
                this.dataFeed.sports[s].competitions[c].fixtures.push(fixture);
                this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
                this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
                this._dataFixtureUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c].fixtures[f]);
                break;
              }
            }
            if (this.dataFeed.sports[s].competitions[c].fixtures.length === 0) {
              this.dataFeed.sports[s].competitions[c].fixtures.push(fixture);
              this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
              this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
              this._dataFixtureUpdateEventSubject.next(fixture);
            }
            break;
          }
        }
        break;
      }
    }
  }

  private _updateMarket(market: SportsbookSportCompetitionFixtureMarket): void {
    for (let s = 0; s < this.dataFeed.sports.length; s++) {
      if (this.dataFeed.sports[s].id === market.sportId) {
        for (let c = 0; c < this.dataFeed.sports[s].competitions.length; c++) {
          if (this.dataFeed.sports[s].competitions[c].id === market.competitionId) {
            for (let f = 0; f < this.dataFeed.sports[s].competitions[c].fixtures.length; f++) {
              if (this.dataFeed.sports[s].competitions[c].fixtures[f].fixtureId === market.fixtureId) {
                for (let m = 0; m < this.dataFeed.sports[s].competitions[c].fixtures[f].markets.length; m++) {
                  if (this.dataFeed.sports[s].competitions[c].fixtures[f].markets[m].marketId === market.marketId) {
                    this.dataFeed.sports[s].competitions[c].fixtures[f].markets[m] = market;
                    this._dataFixtureUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c].fixtures[f]);
                    this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
                    break;
                  }
                  if (m === this.dataFeed.sports[s].competitions[c].fixtures[f].markets.length - 1) {
                    this.dataFeed.sports[s].competitions[c].fixtures[f].markets.push(market);
                    this._dataFixtureUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c].fixtures[f]);
                    this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
                    this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
                    break;
                  }
                }
                if (this.dataFeed.sports[s].competitions[c].fixtures[f].markets.length === 0) {
                  this.dataFeed.sports[s].competitions[c].fixtures[f].markets.push(market);
                  this._dataFixtureUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c].fixtures[f]);
                  this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
                  this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
                }
                break;
              }
            }
            break;
          }
        }
        break;
      }
    }
  }

  private _updateStaticDataFeed(staticDataFeed: SportsbookDataFeed): void {
    if (this.dataFeed.sourceId === null) {
      this.dataFeed = staticDataFeed;
      this._setSportsMenu();
      return;
    }
  }

  private _setCompetitionFixtures(
    sportId: number,
    competitionId: number,
    fixtures: SportsbookSportCompetitionFixture[]
  ): void {
    for (let s = 0; s < this.dataFeed.sports.length; s++) {
      if (this.dataFeed.sports[s].id === sportId) {
        for (let c = 0; c < this.dataFeed.sports[s].competitions.length; c++) {
          if (this.dataFeed.sports[s].competitions[c].id === competitionId) {
            for (let f = 0; f < fixtures.length; f++) {
              for (let sf = 0; sf < this.dataFeed.sports[s].competitions[c].fixtures.length; sf++) {
                if (fixtures[f].fixtureId === this.dataFeed.sports[s].competitions[c].fixtures[sf].fixtureId) {
                  fixtures[f].markets = this.dataFeed.sports[s].competitions[c].fixtures[sf].markets;
                  this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
                  this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
                  break;
                }
              }
            }
            this.dataFeed.sports[s].competitions[c].fixtures = fixtures;
            break;
          }
        }
        break;
      }
    }
  }

  private _setCompetitionFixturesWithMarkets(
    sportId: number,
    competitionId: number,
    fixturesAndMarkets: SportsbookSportCompetitionFixture[]
  ): void {
    for (let s = 0; s < this.dataFeed.sports.length; s++) {
      if (this.dataFeed.sports[s].id === sportId) {
        for (let c = 0; c < this.dataFeed.sports[s].competitions.length; c++) {
          if (this.dataFeed.sports[s].competitions[c].id === competitionId) {
            this.dataFeed.sports[s].competitions[c].fixtures = fixturesAndMarkets;
            this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
            this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
            break;
          }
        }
        break;
      }
    }
  }

  private _setFixtureMarkets(
    sportId: number,
    competitionId: number,
    fixtureId: number,
    markets: SportsbookSportCompetitionFixtureMarket[]
  ): void {
    for (let s = 0; s < this.dataFeed.sports.length; s++) {
      if (this.dataFeed.sports[s].id === sportId) {
        for (let c = 0; c < this.dataFeed.sports[s].competitions.length; c++) {
          if (this.dataFeed.sports[s].competitions[c].id === competitionId) {
            for (let f = 0; f < this.dataFeed.sports[s].competitions[c].fixtures.length; f++) {
              if (this.dataFeed.sports[s].competitions[c].fixtures[f].fixtureId === fixtureId) {
                this.dataFeed.sports[s].competitions[c].fixtures[f].markets = markets;
                this._dataFixtureUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c].fixtures[f]);
                this._dataCompetitonUpdateEventSubject.next(this.dataFeed.sports[s].competitions[c]);
                this._dataSportUpdateEventSubject.next(this.dataFeed.sports[s]);
                break;
              }
            }
          }
        }
        break;
      }
    }
  }

  private _setSportsMenu(): void {
    const uniqueGroup = [];
    this.sportsMenu = [];
    for (let index = 0; index < this.dataFeed.sports.length; index++) {
      const menuItem = new SportsbookMenuItem(this.dataFeed.sports[index]);
      switch (menuItem.categoryId) {
        case 2:
          if (!uniqueGroup.includes(menuItem.categoryId)) {
            this.sportsMenu.push(menuItem);
            uniqueGroup.push(menuItem.categoryId);
          }
          break;
        default:
          this.sportsMenu.push(menuItem);
          break;
      }
    }
    this.sportsMenu.sort((a, b) => {
      if (b.categoryId === 2) {
        return a.name.localeCompare(b.categoryName);
      }
      if (a.categoryId === 2) {
        return a.categoryName.localeCompare(b.name);
      }
      return a.name.localeCompare(b.name);
    });
  }

  ngOnDestroy(): void {
    this._staticSportDataRequest$.unsubscribe();
    this._competitionFixturesDataRequest$.unsubscribe();
    this._competitionFixturesAndMarketsDataRequest$.unsubscribe();
    this._fixtureMarketsDataRequest$.unsubscribe();
    this._competitionUpdated$.unsubscribe();
    this._fixtureUpdated$.unsubscribe();
    this._marketUpdated$.unsubscribe();
  }
}
