// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/core/services/http-request.service';
import {
  SportsbookDataFeed,
  SportsbookSocketMessageType,
  SportsbookSportCompetition,
  SportsbookSportCompetitionFixture,
  SportsbookSportCompetitionFixtureMarket } from '../models/sportsbook-data';
import * as signalR from '@aspnet/signalr';
import { QueryParams } from 'src/app/core/models/request.model';

@Injectable({
  providedIn: 'root',
})
export class SportsbookEngineService {
  private _hubConnection: signalR.HubConnection;
  private _competitionUpdated$: Subject<SportsbookSportCompetition> = new Subject();
  private _fixtureUpdated$: Subject<SportsbookSportCompetitionFixture> = new Subject();
  private _marketUpdated$: Subject<SportsbookSportCompetitionFixtureMarket> = new Subject();
  private _staticSportDataRequest$: Subject<SportsbookDataFeed> = new ReplaySubject(1);
  private _competitionFixturesDataRequest$: Subject<{
    sportId: number, competitionId: number, fixtures: SportsbookSportCompetitionFixture[]
 }> = new Subject();
  private _competitionFixturesAndMarketsDataRequest$: Subject<{
     sportId: number, competitionId: number, fixturesAndMarkets: SportsbookSportCompetitionFixture[]
  }> = new Subject();
  private _fixtureMarketsDataRequest$: Subject<{
    sportId: number, competitionId: number, fixtureId: number, fixtureMarkets: SportsbookSportCompetitionFixtureMarket[]
  }> = new Subject();
  competitionUpdated$: Observable<SportsbookSportCompetition>;
  fixtureUpdated$: Observable<SportsbookSportCompetitionFixture>;
  marketUpdated$: Observable<SportsbookSportCompetitionFixtureMarket>;
  baseApiUrl: string = 'https://softwarealliance.info/api/'; // for development purpose at the moment
  socketDataUrl: string = 'https://socket.softwarealliance.info:444/OddsHub'; // for development purpose at the moment
  staticSportDataRequest$: Observable<SportsbookDataFeed>;
  competitionFixturesDataRequest$: Observable<{
    sportId: number, competitionId: number, fixtures: SportsbookSportCompetitionFixture[]
  }>;
  competitionFixturesAndMarketsDataRequest$: Observable<{
    sportId: number, competitionId: number, fixturesAndMarkets: SportsbookSportCompetitionFixture[]
  }>;
  fixtureMarketsDataRequest$: Observable<{
    sportId: number, competitionId: number, fixtureId: number; fixtureMarkets: SportsbookSportCompetitionFixtureMarket[]
  }>;

  constructor(
    private _httpRequestService: HttpRequestService
  ) {
    this.staticSportDataRequest$ = this._staticSportDataRequest$.asObservable();
    this.competitionFixturesDataRequest$ = this._competitionFixturesDataRequest$.asObservable();
    this.competitionFixturesAndMarketsDataRequest$ = this._competitionFixturesAndMarketsDataRequest$.asObservable();
    this.fixtureMarketsDataRequest$ = this._fixtureMarketsDataRequest$.asObservable();
    this.competitionUpdated$ = this._competitionUpdated$.asObservable();
    this.fixtureUpdated$ = this._fixtureUpdated$.asObservable();
    this.marketUpdated$ = this._marketUpdated$.asObservable();
  }

  getStaticSportsFeed(): Observable<SportsbookDataFeed> {
    return this._httpRequestService.get(this.baseApiUrl + 'feed/get-sports-feed').pipe(
      map(res => {
        const mappedDataFeed = new SportsbookDataFeed(res);
        this._staticSportDataRequest$.next(mappedDataFeed);
        return mappedDataFeed;
      })
    );
  }

  getCompetitionFixturesAndMarkets(
    sportId: number,
    competitionId: number
  ): Observable<{ sportId: number, competitionId: number, fixturesAndMarkets: SportsbookSportCompetitionFixture[] }> {
    const params = new QueryParams();
    params.sportsId = sportId.toString();
    params.competitionId = competitionId.toString();
    return this._httpRequestService.get(this.baseApiUrl + 'feed/get-competition-fixtures-detail', params).pipe(
      map(res => {
        const response = {
          sportId,
          competitionId,
          fixturesAndMarkets: []
        };
        response.fixturesAndMarkets = [];
        for (let index = 0; index < res.data.result.length; index++) {
          response.fixturesAndMarkets.push(new SportsbookSportCompetitionFixture(res.data.result[index]));
        }
        this._competitionFixturesAndMarketsDataRequest$.next(response);
        return response;
      })
    );
  }

  getCompetitionFixtures(
    sportId: number,
    competitionId: number
  ): Observable<{ sportId: number, competitionId: number, fixtures: SportsbookSportCompetitionFixture[] }> {
    const params = new QueryParams();
    params.sportsId = sportId.toString();
    params.competitionId = competitionId.toString();
    return this._httpRequestService.get(this.baseApiUrl + 'feed/get-competition-fixtures', params).pipe(
      map(res => {
        const response = {
          sportId,
          competitionId,
          fixtures: []
        };
        response.fixtures = [];
        for (let index = 0; index < res.data.result.length; index++) {
          response.fixtures.push(new SportsbookSportCompetitionFixture(res.data.result[index]));
        }
        this._competitionFixturesDataRequest$.next(response);
        return response;
      })
    );
  }

  getFixtureMarkets(
    competitionId: number,
    fixtureId: number
  ): Observable<{ sportId: number, competitionId: number, fixtureId: number; fixtureMarkets: SportsbookSportCompetitionFixtureMarket[] }> {
    const params = new QueryParams();
    params.competitionId = competitionId.toString();
    params.fixtureId = fixtureId.toString();
    return this._httpRequestService.get(this.baseApiUrl + 'feed/get-fixture-markets', params).pipe(
      map(res => {
        const response = {
          sportId: null,
          competitionId,
          fixtureId,
          fixtureMarkets: []
        };
        if (res.data.result.length > 0) {
          response.sportId = res.data.result[0].sportId;
          for (let index = 0; index < res.data.result.length; index++) {
            response.fixtureMarkets.push(new SportsbookSportCompetitionFixtureMarket(res.data.result[index]));
          }
        }
        this._fixtureMarketsDataRequest$.next(response);
        return response;
      })
    );
  }

  initiateSportsbookEngine(): void {
    this._startDataFeedSocketConnection();
    this._recieveOddsListener();
    this.getStaticSportsFeed().subscribe();
  }

  private _startDataFeedSocketConnection(): void {
    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(this.socketDataUrl)
      .build();
    this._hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  private _recieveOddsListener(): void {
    this._hubConnection.on('ReceiveOdds', message => {
      message = JSON.parse(message);
      if (message && message.data) {
        switch (message.data.type) {
          case SportsbookSocketMessageType.MarketUpdated:
            const marketUpdated = new SportsbookSportCompetitionFixtureMarket(message.data.result);
            this._handleSocketMessageMarketUpdated(marketUpdated);
            break;
          case SportsbookSocketMessageType.FixtureUpdated:
            const fixtureUpdated = new SportsbookSportCompetitionFixture(message.data.result);
            this._handleSocketMessageFixtureUpdated(fixtureUpdated);
            break;
          case SportsbookSocketMessageType.CompetitionUpdated:
            const competitionUpdated = new SportsbookSportCompetition(message.data.result);
            this._handleSocketMessageCompetitionUpdated(competitionUpdated);
            break;
          default:
            break;
        }
      }
    });
  }

  private _handleSocketMessageCompetitionUpdated(competitionUpdated: SportsbookSportCompetition): void {
    this._competitionUpdated$.next(competitionUpdated);
  }

  private _handleSocketMessageFixtureUpdated(fixtureUpdated: SportsbookSportCompetitionFixture): void {
    this._fixtureUpdated$.next(fixtureUpdated);
  }

  private _handleSocketMessageMarketUpdated(marketUpdated: SportsbookSportCompetitionFixtureMarket): void {
    this._marketUpdated$.next(marketUpdated);
  }
}
