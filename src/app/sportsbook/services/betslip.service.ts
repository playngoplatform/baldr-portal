// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { SportsbookBetslipItem } from '../models/betslip';

@Injectable({
  providedIn: 'root',
})
export class SportsbookBetslipService {
  betslip: SportsbookBetslipItem[] = [];
  private _updateEvent$: Subject<SportsbookBetslipItem[]> = new Subject();
  updateEvent$: Observable<SportsbookBetslipItem[]>;
  validToPlaceBet: boolean;

  constructor() {
    this.updateEvent$ = this._updateEvent$.asObservable();
    this._readBetslipLocalStorage();
  }

  addSelection(betslipItem: SportsbookBetslipItem): void {
    if (!this.isSelectionInBetslip(betslipItem.selectionId)) {
      this.betslip.push(betslipItem);
      this._setBetslipToLocalStorage();
      this._updateEvent$.next(this.betslip);
    }
  }

  removeSelection(betslipItem: SportsbookBetslipItem): void {
    this.betslip = this.betslip.filter(item => item.selectionId !== betslipItem.selectionId);
    this._setBetslipToLocalStorage();
    this._updateEvent$.next(this.betslip);
  }

  isSelectionInBetslip(selectionId: number): boolean {
    return !!this.betslip.find(item => item.selectionId === selectionId);
  }

  resetBetslip(): void {
    this.betslip = [];
    localStorage.setItem('sportsbookBetslip', JSON.stringify([]));
    this._updateEvent$.next(this.betslip);
  }

  private _setBetslipToLocalStorage(): void {
    localStorage.setItem('sportsbookBetslip', JSON.stringify(this.betslip));
  }

  private _readBetslipLocalStorage(): void {
    const betslipLocalStorage = JSON.parse(localStorage.getItem('sportsbookBetslip'));
    this.betslip = betslipLocalStorage ? betslipLocalStorage : [];
  }
}
