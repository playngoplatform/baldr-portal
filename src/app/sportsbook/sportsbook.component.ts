// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../core/models/config.model';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { SportsbookEngineService } from './services/sportsbook-engine.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { SportsbookBetslipService } from './services/betslip.service';
import { SportsbookBetSlipComponent } from './components/bet-slip/bet-slip.component';

@Component({
  selector: 'app-sportsbook',
  templateUrl: './sportsbook.component.html',
  styleUrls: ['./sportsbook.component.scss']
})
export class SportsbookComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;
  private _betSlip$: Subscription;
  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;
  routerOutletActive: boolean = false;
  isBetslipOpen: boolean;

  control = new FormControl('');
  sports: string[] = ['American football', 'Baseball', 'Basket', 'Boxing', 'Cricket', 'Dart', 'Esport', 'Football', 'Golf', 'Handball', 'Icehockey', 'MMA', 'Rugby league', 'Rugby Union', 'Ski jumping', 'Snooker', 'Tennis', 'Volleyball'];
  filteredSports: Observable<string[]>;

  constructor(
    private _portalService: PortalService,
    private _sportsbookEngineService: SportsbookEngineService,
    private _bottomSheet: MatBottomSheet,
    private _betslipService: SportsbookBetslipService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;
    this._sportsbookEngineService.initiateSportsbookEngine();
  }

  ngOnInit(): void {
    this.filteredSports = this.control.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    if (this._betslipService.betslip.length > 0) {
      this.openBetslip();
    } else {
      this.isBetslipOpen = false;
    }

    this._betSlip$ = this._betslipService.updateEvent$.subscribe(res => {
      if (res) {
        if (this._betslipService.betslip.length === 0 && this.isBetslipOpen === true) {
          this.hideBetSlip();
        } else if (this._betslipService.betslip.length <= 1 && this.isBetslipOpen === false) {
          this.openBetslip();
        }
      }
    });
  }

  private _filter(value: string): string[] {
    const filterValue = this._normalizeValue(value);
    return this.sports.filter(sport => this._normalizeValue(sport).includes(filterValue));
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  onActivate(): void {
    setTimeout(() => {
      this.routerOutletActive = true;
    });
  }

  onDeactivate(): void {
    setTimeout(() => {
      this.routerOutletActive = false;
    });
  }

  hideBetSlip(): void {
    this._bottomSheet.dismiss(SportsbookBetSlipComponent);
    this.isBetslipOpen = false;
  }

  openBetslip(): void {
    this._bottomSheet.open(SportsbookBetSlipComponent, {
      hasBackdrop: false,
      closeOnNavigation: false,
      panelClass: 'bet-slip-panel'
    });
    this.isBetslipOpen = true;
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
    this._betSlip$.unsubscribe();
  }
}
