// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SportsbookRoutingModule } from './sportsbook-routing.module';
import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';


// Components
import { SportsbookComponent } from './sportsbook.component';
import { SportsbookDataFeedService } from './services/data-feed.service';
import { SportsbookEngineService } from './services/sportsbook-engine.service';
import { SportsbookSportsMenuComponent } from './components/sports-menu/sports-menu.component';
import { SportsbookSportsContextMenuComponent } from './components/sports-context-menu/sports-context-menu.component';
import { SportsbookSportsLobbyComponent } from './views/sports-lobby/sports-lobby.component';
import { SportsbookSportsLobbyWidgetComponent } from './views/sports-lobby/widget/widget.component';
import { SportsbookSportsLobbyWidgetMarketBetItemComponent } from './views/sports-lobby/widget/market-bet-item/market-bet-item.component';
import { SportsbookBetslipService } from './services/betslip.service';
import { SportsbookCategorySharedMarketBetItemComponent } from './views/category/shared/market-bet-item/market-bet-item.component';
import { SportsbookCategoryPhysicalSportSportComponent } from './views/category/physical-sport/sport/sport.component';
import { SportsbookCategoryPhysicalSportGroupComponent } from './views/category/physical-sport/group/group.component';
import { SportsbookCategoryPhysicalSportCompetitionComponent } from './views/category/physical-sport/competition/competition.component';
import { SportsbookCategoryEsportSportComponent } from './views/category/e-sport/sport/sport.component';
import { SportsbookCategoryEsportGroupComponent } from './views/category/e-sport/group/group.component';
import { SportsbookCategoryEsportCompetitionComponent } from './views/category/e-sport/competition/competition.component';
import { SportsbookEventComponent } from './views/event/event.component';
import { SportsbookBetSlipComponent } from './components/bet-slip/bet-slip.component';

@NgModule({
  declarations: [
    SportsbookComponent,
    SportsbookSportsMenuComponent,
    SportsbookSportsContextMenuComponent,
    SportsbookSportsLobbyComponent,
    SportsbookSportsLobbyWidgetComponent,
    SportsbookSportsLobbyWidgetMarketBetItemComponent,
    SportsbookCategorySharedMarketBetItemComponent,
    SportsbookCategoryPhysicalSportSportComponent,
    SportsbookCategoryPhysicalSportGroupComponent,
    SportsbookCategoryPhysicalSportCompetitionComponent,
    SportsbookCategoryEsportSportComponent,
    SportsbookCategoryEsportGroupComponent,
    SportsbookCategoryEsportCompetitionComponent,
    SportsbookEventComponent,
    SportsbookBetSlipComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SportsbookRoutingModule,
    SharedModule,
    FlexLayoutModule,
  ],
  providers: [
    SportsbookDataFeedService,
    SportsbookEngineService,
    SportsbookBetslipService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    MaterialModule,
    SharedModule,
    FlexLayoutModule
  ]
})
export class SportsbookModule { }
