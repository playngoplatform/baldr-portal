// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { DOCUMENT } from '@angular/common';
import { ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AuthenticationService, UserModel } from 'src/app/core';
import { SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { SharedLoginComponent } from 'src/app/shared/components/login/login.component';
import { SportsbookBetslipItem } from '../../models/betslip';
import { SportsbookBetslipService } from '../../services/betslip.service';

@Component({
  selector: 'app-sportsbook-bet-slip',
  templateUrl: './bet-slip.component.html',
  styleUrls: ['./bet-slip.component.scss']
})
export class SportsbookBetSlipComponent implements OnInit, OnDestroy {
  private _combinedStake$: Subscription;
  private _upDateSelection$: Subscription;
  private _betslip$: Subscription;
  betslipOpen: boolean = true;
  itemOdds: number[];
  totalOdds: number;
  potentialPayout: number;
  priceIsUpdated: boolean = false;
  stakeToHigh: boolean = false;
  maxStakeValue: number[];
  marketExpired: boolean;
  combinedBets: number[];

  currentUser: UserModel;

  combinedStakeForm = new FormGroup({
    combinedStake: new FormControl(),
  });

  singleStakeForm = new FormGroup({
    singleStake: new FormControl('', Validators.required),
  });

  constructor(
    @Inject(DOCUMENT) private document: any,
    public betslipService: SportsbookBetslipService,
    private changeDetectorRef: ChangeDetectorRef,
    public betButton: ChangeDetectorRef,
    public _dialog: MatDialog,
    private _translate: TranslateService,
    private authService: AuthenticationService,
    private authenticationService: AuthenticationService,
    private _snackBar: MatSnackBar,
  ) {}

  ngOnInit(): void {
    this._betslip$ = this.betslipService.updateEvent$.subscribe(res => {
      if (res) {
        this.calculateTotalOdds();
        this.calculatePotentialPayout();
        this.changeDetectorRef.detectChanges();
      }
    });
    if (this.authService.isAuthenticated()) {
      this.currentUser = this.authService.currentUser();
      this.authenticationService.refresh().subscribe(user => {
        this.currentUser = user;
      });
    }
    // Fixes IOS bug regarding viewHeight
    const vh = window.innerHeight * 0.01;
    this.document.documentElement.style.setProperty('--vh', `${vh}px`);
    window.addEventListener('resize', () => {
      // tslint:disable-next-line:no-shadowed-variable
      const vh = window.innerHeight * 0.01;
      this.document.documentElement.style.setProperty('--vh', `${vh}px`);
    });
    this.calculateTotalOdds();
    this.calculatePotentialPayout();
  }

  calculateTotalOdds(): void {
    this.combinedBets = this.betslipService.betslip.map(item => item.selection.price);
    this.totalOdds = this.combinedBets.reduce(function(sum, item) {
      return sum * item;
    }, 1);
  }

  calculatePotentialPayout(): void {
    this._combinedStake$ = this.combinedStakeForm.valueChanges.subscribe(value => {
      this.potentialPayout = value.combinedStake * this.totalOdds;
     });
    this.potentialPayout = this.totalOdds * this.combinedStakeForm.controls.combinedStake.value;
  }

  upDateSelections(): void {
    this._upDateSelection$ = this.betslipService.updateEvent$.subscribe(res => {
      if (res) {
        return this.betslipService.betslip;
      }
    });
  }

  acceptNewOdds(): void {
    this.priceIsUpdated = false;
  }

  placeCombinedBet(): void {
    this._snackBar.open(
      this._translate.instant('Betslip.Your_bet_is_placed'),
      this._translate.instant('Ok'),
      { duration: SNACKBAR_DURATION }
    );
    alert('Combined bet is placed');
    this.removeAll();
  }

  placeSingleBet(): void {
    this._snackBar.open(
      this._translate.instant('Betslip.Your_bet_is_placed'),
      this._translate.instant('Ok'),
      { duration: SNACKBAR_DURATION }
    );
    alert('Single bet is placed');
    this.removeAll();
  }

  openLoginDialog(): void {
    this._dialog.open(SharedLoginComponent, {
      height: 'calc(var(--vh, 1vh) * 100)',
      width: '100vw',
      maxWidth: '100vw',
      maxHeight: 'calc(var(--vh, 1vh) * 100)',
      panelClass: 'fullscreen-dialog'
    }).afterClosed();
  }

  toggleBetslip(): void {
    this.betslipOpen = !this.betslipOpen;
  }

  removeSelection(betslipItem: SportsbookBetslipItem): void {
    this.betslipService.removeSelection(betslipItem);
  }

  removeAll(): void {
    this.betslipService.resetBetslip();
  }

  ngOnDestroy(): void {
    if (this._combinedStake$) {
      this._combinedStake$.unsubscribe();
    }
    if (this._upDateSelection$) {
      this._upDateSelection$.unsubscribe();
    }
    this._betslip$.unsubscribe();
  }

}
