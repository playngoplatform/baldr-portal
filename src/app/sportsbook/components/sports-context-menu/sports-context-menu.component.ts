// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, ChangeDetectorRef, OnDestroy, DoCheck, Input } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../core/models/config.model';
import { AuthenticationService, UserModel } from 'src/app/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-sportsbook-sports-context-menu',
  templateUrl: './sports-context-menu.component.html',
  styleUrls: ['./sports-context-menu.component.scss']
})
export class SportsbookSportsContextMenuComponent implements OnInit, OnDestroy, DoCheck {
  private _mobileQueryListener: () => void;
  @Input() accountDrawer: MatSidenav;
  @Input() sportsMenu: MatSidenav;
  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  currentUser: UserModel;

  constructor(
    private _PortalService: PortalService,
    private authService: AuthenticationService,
    private authenticationService: AuthenticationService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._PortalService.isMobile;
    this.isTablet = this._PortalService.isTablet;
    this.isDesktop = this._PortalService.isDesktop;
   }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.currentUser = this.authService.currentUser();
      this.authenticationService.refresh().subscribe(user => {
        this.currentUser = user;
      });
    }
  }

  ngDoCheck() {
    if (this.authService.isAuthenticated()) {
      this.currentUser = this.authService.currentUser();
    }
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
