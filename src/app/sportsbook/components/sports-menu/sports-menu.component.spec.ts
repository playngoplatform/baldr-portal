// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsbookSportsMenuComponent } from './sports-menu.component';

describe('SportsbookSportsMenuComponent', () => {
  let component: SportsbookSportsMenuComponent;
  let fixture: ComponentFixture<SportsbookSportsMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportsbookSportsMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsbookSportsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
