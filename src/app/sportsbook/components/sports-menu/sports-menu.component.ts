// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, ChangeDetectorRef, OnDestroy, Input } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../core/models/config.model';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SportsbookDataFeedService } from '../../services/data-feed.service';
import { MatSidenav } from '@angular/material/sidenav';
import { SportsbookSport } from '../../models/sportsbook-data';

@Component({
  selector: 'app-sportsbook-sports-menu',
  templateUrl: './sports-menu.component.html',
  styleUrls: ['./sports-menu.component.scss']
})
export class SportsbookSportsMenuComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;
  @Input() sportsMenu: MatSidenav;

  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  constructor(
    public dataFeedService: SportsbookDataFeedService,
    private _PortalService: PortalService,
    private _matIconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._PortalService.isMobile;
    this.isTablet = this._PortalService.isTablet;
    this.isDesktop = this._PortalService.isDesktop;

    this._matIconRegistry.addSvgIcon(
      'american_football',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/american-football.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'baseball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/baseball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'basketball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/basketball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'boxing',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/boxing.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'cricket',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/cricket.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'dart',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/dart.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'esport',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/esport.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'football',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/football.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'golf',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/golf.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'handball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/handball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'icehockey',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/icehockey.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'mma',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/mma.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'rugby',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/rugby.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'ski_jumping',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/ski_jumping.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'snooker',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/snooker.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'tennis',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/tennis.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'volleyball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/volleyball.svg')
    );
  }

  ngOnInit(): void {
  }

  toggleExpanded(subMenu): void {
    subMenu.classList.contains('expanded') ? subMenu.classList.remove('expanded') : subMenu.classList.add('expanded');
  }

  createSportCategoryUrl(sport: SportsbookSport): string {
    switch (sport.categoryId) {
      case 1:
        return '/sportsbook/category/1/' + sport.name.toLowerCase().replace(/ /g, '_') + '/' + sport.id;
      case 2:
        return '/sportsbook/category/2/e-sport';
      default:
        return '/sportsbook/category/' + sport.categoryId + '/' + sport.name.toLowerCase().replace(/ /g, '_') + '/' + sport.id;
    }
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }
}
