// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { SportsbookComponent } from './sportsbook.component';
import { SportsbookSportsLobbyComponent } from './views/sports-lobby/sports-lobby.component';
import { SportsbookCategoryPhysicalSportSportComponent } from './views/category/physical-sport/sport/sport.component';
import { SportsbookCategoryPhysicalSportGroupComponent } from './views/category/physical-sport/group/group.component';
import { SportsbookCategoryPhysicalSportCompetitionComponent } from './views/category/physical-sport/competition/competition.component';
import { SportsbookCategoryEsportSportComponent } from './views/category/e-sport/sport/sport.component';
import { SportsbookCategoryEsportGroupComponent } from './views/category/e-sport/group/group.component';
import { SportsbookCategoryEsportCompetitionComponent } from './views/category/e-sport/competition/competition.component';
import { SportsbookEventComponent } from './views/event/event.component';

import { AuthGuardService as AuthGuard } from '../core/services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: SportsbookComponent,
    children: [
      {
        path: 'home',
        component: SportsbookSportsLobbyComponent
      },
      {
        path: 'category/1/:sport/:sportId',
        component: SportsbookCategoryPhysicalSportSportComponent
      },
      {
        path: 'category/1/:sport/:sportId/:group',
        component: SportsbookCategoryPhysicalSportGroupComponent
      },
      {
        path: 'category/1/:sport/:sportId/:group/:competition/:competitionId',
        component: SportsbookCategoryPhysicalSportCompetitionComponent
      },
      {
        path: 'category/2/e-sport',
        component: SportsbookCategoryEsportSportComponent
      },
      {
        path: 'category/2/e-sport/:sport/:sportId',
        component: SportsbookCategoryEsportGroupComponent
      },
      {
        path: 'category/2/e-sport/:sport/:sportId/:competition/:competitionId',
        component: SportsbookCategoryEsportCompetitionComponent
      },
      {
        path: 'event',
        component: SportsbookEventComponent
      }
    ]}
  ];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    LocalizeRouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
    LocalizeRouterModule
  ],
  providers: [AuthGuard]
})
export class SportsbookRoutingModule {}
