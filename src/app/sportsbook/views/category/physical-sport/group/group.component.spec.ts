// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsbookCategoryPhysicalSportGroupComponent } from './group.component';

describe('SportsbookCategoryPhysicalSportGroupComponent', () => {
  let component: SportsbookCategoryPhysicalSportGroupComponent;
  let fixture: ComponentFixture<SportsbookCategoryPhysicalSportGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportsbookCategoryPhysicalSportGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsbookCategoryPhysicalSportGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
