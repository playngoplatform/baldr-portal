// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsbookCategoryPhysicalSportSportComponent } from './sport.component';

describe('SportsCategoryComponent', () => {
  let component: SportsbookCategoryPhysicalSportSportComponent;
  let fixture: ComponentFixture<SportsbookCategoryPhysicalSportSportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportsbookCategoryPhysicalSportSportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsbookCategoryPhysicalSportSportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
