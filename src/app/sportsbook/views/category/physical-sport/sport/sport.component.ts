// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../../../core/models/config.model';
import { fadeIn } from 'src/app/shared/animations/fade.animation';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { SportsbookDataFeedService } from 'src/app/sportsbook/services/data-feed.service';
import { SportsbookEngineService } from 'src/app/sportsbook/services/sportsbook-engine.service';
import { SportsbookSport, SportsbookSportCompetition, SportsbookSportCompetitionFixture, SportsbookSportCompetitionFixtureMarket } from 'src/app/sportsbook/models/sportsbook-data';
import { SportsbookCategoryGroup } from 'src/app/sportsbook/models/sportsbook-category';
import * as moment from 'moment';

@Component({
  selector: 'app-sportsbook-category-physical-sport-sport',
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.scss'],
  animations: [fadeIn]
})
export class SportsbookCategoryPhysicalSportSportComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;

  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  progressLoader: boolean = true;
  competitionLoaders: { [key: string]: { [key: string]: boolean } } = {};
  showProgressBar: boolean = false;
  showFixtures: boolean = false;

  sportId: number;

  sportData: SportsbookSport;
  competitionsGroupedByCountry: { [key: string]: SportsbookCategoryGroup };
  groupNames: string [] = [];
  displayMarketStatus: number[] = [0, 2];
  displayFixtureStatus: number[] = [0, 1];
  amountBettableMarkets: number;
  toggleExpansionCompetitions: { [key: number]: boolean } = {};

  constructor(
    private _portalService: PortalService,
    private _sportsbookEngineService: SportsbookEngineService,
    private _dataFeedService: SportsbookDataFeedService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _localize: LocalizeRouterService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;
  }

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.sportId = parseInt(params['sportId'], 10);
    });
    this._sportsbookEngineService.staticSportDataRequest$.subscribe(() => {
      this.progressLoader = false;
      this.sportData = this._dataFeedService.dataFeed.sports.find(sport => sport.id === this.sportId);
      if (!this.sportData) {
        this._router.navigate([this._localize.translateRoute('/404')]);
      }
      this._prepareSportData(this.sportData);
    }, () => {
      this.progressLoader = false;
    });
    this._dataFeedService.dataSportUpdateEvent$.subscribe(res => {
      if (res.id === this.sportId) {
        this._prepareSportData(res);
      }
    });
  }

  getFixturesForCompetition(onOpen: boolean, competition: SportsbookSportCompetition, groupName: string): void {
    if (onOpen) {
      this.showProgressBar = true;
      this.competitionLoaders[groupName][competition.competitionName] = true;
      this._sportsbookEngineService.getCompetitionFixturesAndMarkets(competition.sportId, competition.id).subscribe(() => {
        this.showProgressBar = false;
        this.showFixtures = true;
        this.competitionLoaders[groupName][competition.competitionName] = false;
      }, () => {
        this.competitionLoaders[groupName][competition.competitionName] = false;
        this.showProgressBar = false;
      });
    }
  }

  navigateHome(): void {
    this._router.navigate([this._localize.translateRoute('/sportsbook/home')]);
  }

  isDateToday(date: moment.Moment): boolean {
    return moment().isSame(date, 'day');
  }

  displayMarket(market: SportsbookSportCompetitionFixtureMarket, fixture: SportsbookSportCompetitionFixture): boolean {
    let showMarket = false;
    showMarket = market.marketCategory === 1 && this.displayMarketStatus.includes(market.status) ? true : showMarket;
    if (showMarket) {
      const markets = this._filterResultedMarkets(fixture.markets);
      showMarket = markets.length > 1 && market.selections.length <= 2 ? false : showMarket;
    }
    return showMarket;
  }

  displayMarketLive(market: SportsbookSportCompetitionFixtureMarket, fixture: SportsbookSportCompetitionFixture): boolean {
    let showMarket = false;
    showMarket = market.marketCategory === 1 && this.displayMarketStatus.includes(market.status) && fixture.offeredLive ? true : showMarket;
    if (showMarket) {
      const markets = this._filterResultedMarkets(fixture.markets);
      showMarket = markets.length > 1 && market.selections.length <= 2 ? false : showMarket;
      showMarket = showMarket && market.isLive ? showMarket : false;
    }
    return showMarket;
  }

  fixtureContainDisplayMarkets(fixture: SportsbookSportCompetitionFixture): boolean {
    let status = false;
    fixture.markets.forEach(market => {
      if (this.displayMarket(market, fixture)) {
        status = true;
      }
    });
    return status;
  }

  toggleCompetitionExpansion(id: number): void {
    this.toggleExpansionCompetitions[id] = !this.toggleExpansionCompetitions[id];
  }

  private _prepareSportData(sport: SportsbookSport): void {
    this.competitionsGroupedByCountry = {};
    for (let c = 0; c < sport.competitions.length; c++) {
      const competition = sport.competitions[c];
      competition.fixtures = this._sortFixturesByStartTime(competition.fixtures);
      if (competition.group === '') {
        this.competitionsGroupedByCountry['Miscellaneous'] = this.competitionsGroupedByCountry['Miscellaneous']
          || new SportsbookCategoryGroup();
        this.competitionsGroupedByCountry['Miscellaneous'].competitions.push(competition);
        this.competitionsGroupedByCountry['Miscellaneous'].totalGroupMatchupCount += competition.matchUpCount;
        this.competitionLoaders['Miscellaneous'] = {};
        continue;
      }
      if (!this.competitionsGroupedByCountry[competition.group]) {
        this.competitionLoaders[competition.group] = {};
        this.competitionsGroupedByCountry[competition.group] = new SportsbookCategoryGroup();
      }
      this.competitionsGroupedByCountry[competition.group].competitions.push(competition);
      this.competitionsGroupedByCountry[competition.group].totalGroupMatchupCount += competition.matchUpCount;
    }
    this.groupNames = Object.getOwnPropertyNames(this.competitionsGroupedByCountry).sort();
    this.groupNames.forEach((group: string) => {
      this.competitionsGroupedByCountry[group].competitions.sort(this._sortCompetitionsAlphabetically);
    });
  }

  private _filterResultedMarkets(markets: SportsbookSportCompetitionFixtureMarket[]): SportsbookSportCompetitionFixtureMarket[] {
    return markets.filter(market => market.marketCategory === 1 && market.marketName === 'Regulation Time Winner');
  }

  private _sortFixturesByStartTime(fixtures: SportsbookSportCompetitionFixture[]) {
    return fixtures.sort((a, b) => a.scheduledAt.isBefore(b.scheduledAt) ? -1 : 1 );
  }

  private _sortCompetitionsAlphabetically(a: SportsbookSportCompetition, b: SportsbookSportCompetition): number {
    if ( a.competitionName < b.competitionName) {
      return -1;
    }
    if ( a.competitionName > b.competitionName ) {
      return 1;
    }
    return 0;
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
