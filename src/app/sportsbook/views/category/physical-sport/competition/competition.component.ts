// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../../../core/models/config.model';
import { fadeIn } from 'src/app/shared/animations/fade.animation';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { SportsbookEngineService } from 'src/app/sportsbook/services/sportsbook-engine.service';
import { SportsbookSport, SportsbookSportCompetition, SportsbookSportCompetitionFixture, SportsbookSportCompetitionFixtureMarket } from 'src/app/sportsbook/models/sportsbook-data';
import { SportsbookDataFeedService } from 'src/app/sportsbook/services/data-feed.service';
import * as moment from 'moment';
import { CategoryCompetitionFixtureGroup } from 'src/app/sportsbook/models/sportsbook-category';

export interface OutrightsFixture {
  competitorName: string;
  selectionType: number;
  price: number;
  status: number;
}

@Component({
  selector: 'app-sportsbook-category-physical-sport-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss'],
  animations: [fadeIn]
})
export class SportsbookCategoryPhysicalSportCompetitionComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;

  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  displayMarketStatus: number[] = [0, 2];
  displayFixtureStatus: number[] = [0, 1];

  progressLoader: boolean = true;
  showProgressBar: boolean = false;
  showFixtures: boolean = false;
  hideDateDivider: boolean = false;

  sportId: number;
  groupName: string;
  competitionId: number;

  sportData: SportsbookSport;
  competition: SportsbookSportCompetition;
  fixtureGroups: CategoryCompetitionFixtureGroup[] = [];
  toggleExpansions: { [key: string]: boolean } = {
    today: false,
    tomorrow: false,
    future: false
  };

  outrightsFixtures: OutrightsFixture[] = [
    {
      competitorName: 'Liverpool',
      status: 0,
      selectionType: 3,
      price: 1.75
    },
    {
      competitorName: 'Manchester City',
      status: 0,
      selectionType: 3,
      price: 1.95
    },
    {
      competitorName: 'Tottenham',
      status: 0,
      selectionType: 3,
      price: 2.35
    },
    {
      competitorName: 'Arsenal',
      status: 0,
      selectionType: 3,
      price: 2.45
    }
  ];

  constructor(
    private _sportsbookEngineService: SportsbookEngineService,
    private _dataFeedService: SportsbookDataFeedService,
    private _portalService: PortalService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _localize: LocalizeRouterService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;
  }

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.groupName = params['group'] ? params['group'][0].toUpperCase() + params['group'].slice(1) : null;
      this.sportId = parseInt(params['sportId'], 10);
      this.competitionId = parseInt(params['competitionId'], 10);
      this.getFixturesForCompetition(this.sportId, this.competitionId);
    });
    this._sportsbookEngineService.staticSportDataRequest$.subscribe(() => {
      this.progressLoader = false;
      this.sportData = this._dataFeedService.dataFeed.sports.find(sport => sport.id === this.sportId);
      if (!this.sportData) {
        this._router.navigate([this._localize.translateRoute('/404')]);
      }
      this._prepareCompetitionData(this.sportData, this.competitionId);
      if (!this.competition) {
        this._router.navigate([this._localize.translateRoute('/404')]);
      }
    }, () => {
      this.progressLoader = false;
    });
    this._dataFeedService.dataSportUpdateEvent$.subscribe(res => {
      if (res.id === this.sportId) {
        this.sportData = res;
        this._prepareCompetitionData(res, this.competitionId);
      }
    });
  }

  toggleCompetitionExpansion(date: string): void {
    this.toggleExpansions[date] = !this.toggleExpansions[date];
  }

  private _prepareCompetitionData(sportData: SportsbookSport, competitionId: number) {
    this.fixtureGroups = [];
    this.competition = sportData.competitions.find(competition => competition.id === competitionId);
    this.competition.fixtures = this._sortFixturesByStartTime(this.competition.fixtures);
    const todayFixtureGroup = new CategoryCompetitionFixtureGroup();
    todayFixtureGroup.dateGroup = 'today';
    const tomorrowFixtureGroup = new CategoryCompetitionFixtureGroup();
    tomorrowFixtureGroup.dateGroup = 'tomorrow';
    const futureFixtureGroup = new CategoryCompetitionFixtureGroup();
    futureFixtureGroup.dateGroup = 'future';
    this.competition.fixtures.forEach(fixture => {
      if (fixture.scheduledAt.isSame(moment(), 'day')) {
        todayFixtureGroup.fixtures.push(fixture);
        return;
      }
      if (fixture.scheduledAt.isSame(moment().add(1, 'day'), 'day')) {
        tomorrowFixtureGroup.fixtures.push(fixture);
        return;
      }
      if (fixture.scheduledAt.isAfter(moment().add(1, 'day').endOf('day'), 'day')) {
        futureFixtureGroup.fixtures.push(fixture);
        return;
      }
    });
    this.fixtureGroups.push(todayFixtureGroup);
    this.fixtureGroups.push(tomorrowFixtureGroup);
    this.fixtureGroups.push(futureFixtureGroup);
    this.fixtureGroups = this.fixtureGroups.map(group => {
      group.matchupCount = group.fixtures.length;
      return group;
    });
  }

  getFixturesForCompetition(sportId: number, competitionId: number): void {
    this._sportsbookEngineService.getCompetitionFixturesAndMarkets(sportId, competitionId).subscribe(() => {
      this.progressLoader = false;
      this.showFixtures = true;
    }, () => {
      this.progressLoader = false;
    });
  }

  displayMarket(market: SportsbookSportCompetitionFixtureMarket, fixture: SportsbookSportCompetitionFixture): boolean {
    let showMarket = false;
    showMarket = market.marketCategory === 1 && this.displayMarketStatus.includes(market.status) ? true : showMarket;
    if (showMarket) {
      const markets = this._filterResultedMarkets(fixture.markets);
      showMarket = markets.length > 1 && market.selections.length <= 2 ? false : showMarket;
    }
    return showMarket;
  }

  fixtureContainDisplayMarkets(fixture: SportsbookSportCompetitionFixture): boolean {
    let status = false;
    fixture.markets.forEach(market => {
      if (this.displayMarket(market, fixture)) {
        status = true;
      }
    });
    return status;
  }

  isDateToday(date: moment.Moment): boolean {
    return moment().isSame(date, 'day');
  }

  navigateHome(): void {
    this._router.navigate([this._localize.translateRoute('/sportsbook/home')]);
  }

  private _filterResultedMarkets(markets: SportsbookSportCompetitionFixtureMarket[]): SportsbookSportCompetitionFixtureMarket[] {
    return markets.filter(market => market.marketCategory === 1 && market.marketName === 'Regulation Time Winner');
  }

  private _sortFixturesByStartTime(fixtures: SportsbookSportCompetitionFixture[]) {
    return fixtures.sort((a, b) => a.scheduledAt.isBefore(b.scheduledAt) ? -1 : 1 );
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
