// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../../../core/models/config.model';
import { fadeIn } from 'src/app/shared/animations/fade.animation';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

export interface CompetitionGroup {
  competitionName: string; matchupCount: number;
    fixtures?: {
      fixtureName: string,
      competitionName: string,
      marketCount: number,
      sportName: string,
      group: string,
      isLive: boolean,
      scheduledAt: string,
      fixtureTeam1: string,
      fixtureTeam2: string,
      selections?: {
        selectionName: string,
        status: number,
        price: number,
        selectionType: number,
        selectionItem: string
      }[];
    }[];
}

@Component({
  selector: 'app-sportsbook-category-e-sport-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
  animations: [fadeIn]
})
export class SportsbookCategoryEsportGroupComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;

  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  progressLoader: boolean = true;
  showProgressBar: boolean = false;
  showFixtures: boolean = false;
  hideDateDivider: boolean = false;

  sportParam: string;
  groupParam: string;

  competitionGroups: CompetitionGroup[] = [
    {competitionName: 'Dreamhack Masters Europe', matchupCount: 1, fixtures: [
      {
        fixtureName: 'Cloud9 - Team Spirit',
        competitionName: 'Dreamhack Masters Europe',
        marketCount: 14,
        isLive: false,
        fixtureTeam1: 'Cloud9',
        fixtureTeam2: 'Team Spirit',
        sportName: 'E-sport',
        group: 'Dota 2',
        scheduledAt: '13:00', selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'Cloud9', price: 2.15 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Team Spirit', price: 1.64 },
      ]}
    ]},
    {competitionName: 'Dreamhack Masters Spring 2020: Asia', matchupCount: 1, fixtures: [
      {
        fixtureName: 'TyLoo - Invictus Gaming',
        competitionName: 'Dreamhack Masters Spring 2020: Asia',
        marketCount: 10,
        isLive: true,
        fixtureTeam1: 'TyLoo',
        fixtureTeam2: 'Invictus Gaming',
        sportName: 'E-sport',
        group: 'Dota 2',
        scheduledAt: '09:00', selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'TyLoo', price: 1.12 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Leeds United', price: 5.30 },
      ]}
    ]}
  ];

  constructor(
    private _portalService: PortalService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _localize: LocalizeRouterService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;
  }

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.sportParam = params['sport'];
      this.groupParam = params['group'];
    });

    setTimeout(() => {
      this.progressLoader = false;
    }, 1000);
  }

  navigateHome(): void {
    this._router.navigate([this._localize.translateRoute('/sportsbook/home')]);
  }

  getFixtures(): void {
    this.showProgressBar = true;
    setTimeout(() => {
      this.showFixtures = true;
      this.showProgressBar = false;
    }, 1000);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
