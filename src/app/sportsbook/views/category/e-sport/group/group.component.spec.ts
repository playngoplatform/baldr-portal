// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsbookCategoryEsportGroupComponent } from './group.component';

describe('GroupComponent', () => {
  let component: SportsbookCategoryEsportGroupComponent;
  let fixture: ComponentFixture<SportsbookCategoryEsportGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportsbookCategoryEsportGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsbookCategoryEsportGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
