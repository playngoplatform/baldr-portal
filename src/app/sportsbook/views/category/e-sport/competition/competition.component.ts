// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../../../core/models/config.model';
import { fadeIn } from 'src/app/shared/animations/fade.animation';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';


export interface FixtureGroup {
  dateGroup: string; matchupCount: number;
    fixtures?: {
      fixtureName: string,
      competitionName: string,
      marketCount: number,
      sportName: string,
      group: string,
      isLive: boolean,
      scheduledAt: string,
      fixtureTeam1: string,
      fixtureTeam2: string,
      selections?: {
        selectionName: string,
        status: number,
        price: number,
        selectionType: number,
        selectionItem: string
      }[];
    }[];
}

export interface OutrightsFixture {
  competitorName: string;
  selectionType: number;
  price: number;
  status: number;
}

@Component({
  selector: 'app-sportsbook-category-e-sport-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss'],
  animations: [fadeIn]
})
export class SportsbookCategoryEsportCompetitionComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;

  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  progressLoader: boolean = true;
  showProgressBar: boolean = false;
  showFixtures: boolean = false;
  hideDateDivider: boolean = false;

  sportParam: string;
  groupParam: string;
  competitionParam: string;
  sanitizedParam: string;

  fixtureGroups: FixtureGroup[] = [
    { dateGroup: 'Today', matchupCount: 5, fixtures: [
      {
        fixtureName: 'Natus Vincere - Mudgolems',
        competitionName: 'Epic League Division 1',
        marketCount: 20,
        isLive: false,
        fixtureTeam1: 'Natus Vincere',
        fixtureTeam2: 'Mudgolems',
        sportName: 'E-sport',
        group: 'Dota 2',
        scheduledAt: '14:00',
        selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'Natus Vincere', price: 1.68 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Mudgolems', price: 2.10 },
      ]},
      {
        fixtureName: 'Nigma - Vikin.gg',
        competitionName: 'Epic League Division 1',
        marketCount: 20,
        isLive: true,
        fixtureTeam1: 'Nigma',
        fixtureTeam2: 'Vikin.gg',
        sportName: 'Football',
        group: 'Sweden',
        scheduledAt: '19:00',
        selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'Nigma', price: 1.95 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Vikin.gg', price: 1.75 },
      ]}
    ] },
    { dateGroup: 'Tomorrow', matchupCount: 5, fixtures: [
      {
        fixtureName: 'Natus Vincere - Mudgolems',
        competitionName: 'Epic League Division 1',
        marketCount: 20,
        isLive: false,
        fixtureTeam1: 'Natus Vincere',
        fixtureTeam2: 'Mudgolems',
        sportName: 'E-sport',
        group: 'Dota 2',
        scheduledAt: '14:00',
        selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'Natus Vincere', price: 1.68 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Mudgolems', price: 2.10 },
      ]},
      {
        fixtureName: 'Nigma - Vikin.gg',
        competitionName: 'Epic League Division 1',
        marketCount: 20,
        isLive: true,
        fixtureTeam1: 'Nigma',
        fixtureTeam2: 'Vikin.gg',
        sportName: 'Football',
        group: 'Sweden',
        scheduledAt: '19:00',
        selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'Nigma', price: 1.95 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Vikin.gg', price: 1.75 },
      ]}
    ] },
    { dateGroup: 'Future', matchupCount: 5, fixtures: [
      {
        fixtureName: 'Natus Vincere - Mudgolems',
        competitionName: 'Epic League Division 1',
        marketCount: 20,
        isLive: false,
        fixtureTeam1: 'Natus Vincere',
        fixtureTeam2: 'Mudgolems',
        sportName: 'E-sport',
        group: 'Dota 2',
        scheduledAt: '14:00',
        selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'Natus Vincere', price: 1.68 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Mudgolems', price: 2.10 },
      ]},
      {
        fixtureName: 'Nigma - Vikin.gg',
        competitionName: 'Epic League Division 1',
        marketCount: 20,
        isLive: true,
        fixtureTeam1: 'Nigma',
        fixtureTeam2: 'Vikin.gg',
        sportName: 'Football',
        group: 'Sweden',
        scheduledAt: '19:00',
        selections: [
          { selectionType: 4, status: 3, selectionItem: '1', selectionName: 'Nigma', price: 1.95 },
          { selectionType: 5, status: 0, selectionItem: '2', selectionName: 'Vikin.gg', price: 1.75 },
      ]}
    ] }
  ];

  outrightsFixtures: OutrightsFixture[] = [
    {
      competitorName: 'Nigma',
      status: 0,
      selectionType: 3,
      price: 1.75
    },
    {
      competitorName: 'Vikin.gg',
      status: 0,
      selectionType: 3,
      price: 1.95
    },
    {
      competitorName: 'Cloud9',
      status: 0,
      selectionType: 3,
      price: 2.35
    },
    {
      competitorName: 'Team Spirit',
      status: 0,
      selectionType: 3,
      price: 2.45
    }
  ];

  constructor(
    private _portalService: PortalService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _localize: LocalizeRouterService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;
  }

   ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.sportParam = params['sport'];
      this.groupParam = params['group'];
      this.competitionParam = params['competition'];
    });

    setTimeout(() => {
      this.progressLoader = false;
    }, 1000);

    this.sanitizedParam = this.competitionParam.replace(/_/g, ' ');
    this.getFixtures();
  }

  navigateHome(): void {
    this._router.navigate([this._localize.translateRoute('/sportsbook/home')]);
  }

  getFixtures(): void {
    this.showProgressBar = true;
    setTimeout(() => {
      this.showFixtures = true;
      this.showProgressBar = false;
    }, 1000);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
