// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsbookCategoryEsportCompetitionComponent } from './competition.component';

describe('CompetitionComponent', () => {
  let component: SportsbookCategoryEsportCompetitionComponent;
  let fixture: ComponentFixture<SportsbookCategoryEsportCompetitionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportsbookCategoryEsportCompetitionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsbookCategoryEsportCompetitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
