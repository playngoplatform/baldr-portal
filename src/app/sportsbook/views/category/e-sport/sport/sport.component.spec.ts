// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsbookCategoryEsportSportComponent } from './sport.component';

describe('SportsbookCategoryEsportSportComponent', () => {
  let component: SportsbookCategoryEsportSportComponent;
  let fixture: ComponentFixture<SportsbookCategoryEsportSportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportsbookCategoryEsportSportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsbookCategoryEsportSportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
