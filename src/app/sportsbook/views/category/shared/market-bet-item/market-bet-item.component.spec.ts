// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorySharedMarketBetItemComponent } from './market-bet-item.component';

describe('MarketBetItemComponent', () => {
  let component: CategorySharedMarketBetItemComponent;
  let fixture: ComponentFixture<CategorySharedMarketBetItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategorySharedMarketBetItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorySharedMarketBetItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
