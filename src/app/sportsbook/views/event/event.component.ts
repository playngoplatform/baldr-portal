// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MEDIA_BREAK_POINT } from 'src/app/core/models/config.model';
import { PortalService } from 'src/app/core/services/portal.service';
import { fadeIn } from 'src/app/shared/animations/fade.animation';

export interface Market {
  marketName: string;
    type: {marketTypeId: number};
    selections?: {
      selectionName: string;
      participantId: number;
      points: number;
      type: number;
      price: number;
    }[];
}
export interface Fixture {
  fixtureName: string;
  competitionName: string;
  sportName: string;
  group: string;
  scheduledAt: string;
  participants?: {
    participantName: string,
    participantId: number,
    sortOrder: number;
  }[];
}
@Component({
  selector: 'app-sportsbook-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  animations: [fadeIn]
})
export class SportsbookEventComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;

  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  progressLoader: boolean = true;

  // Dummy data
  fixture: Fixture = {
    fixtureName: 'Southampton vs Liverpool',
    competitionName: 'Premier League',
    sportName: 'Football',
    group: 'England',
    scheduledAt: '27 dec, 16:00',
    participants: [
      { participantName: 'Southampton', participantId: 319385, sortOrder: 1 },
      { participantName: 'Liverpool', participantId: 319386, sortOrder: 2 },
    ]
  };

  markets: Market[] = [
    {
      marketName: 'Regulation Time Winner',
      type: {
        marketTypeId: 112
      },
      selections: [
        {selectionName: 'Southampton', points: null, type: 4, price: 4.85, participantId: 319385},
        {selectionName: 'Draw', points: null, type: 5, price: 4.66, participantId: null},
        {selectionName: 'Liverpool', points: null, type: 3, price: 1.64, participantId: 319386}
    ]},
    {
      marketName: 'Regulation Time Total Goals Over/Under',
      type: {
        marketTypeId: 134
      },
      selections: [
        {selectionName: 'Over', points: 3.5, type: 4, price: 2.62, participantId: null},
        {selectionName: 'Under', points: 3.5, type: 3, price: 3.50, participantId: null}
    ]},
    {
      marketName: 'Regulation Time Goal Handicap',
      type: {
        marketTypeId: 133
      },
      selections: [
        {selectionName: 'Southampton', points: 0.25, type: 4, price: 2.89, participantId: 319385},
        {selectionName: 'Liverpool', points: -0.25, type: 5, price: 1.46, participantId: 319386}
    ]},
  ];
  // Dummy data ends

  constructor(
    private _portalService: PortalService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.progressLoader = false;
    }, 1000);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
