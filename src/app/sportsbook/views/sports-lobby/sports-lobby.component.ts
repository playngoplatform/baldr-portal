// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, ChangeDetectorRef, OnDestroy, Input } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../core/models/config.model';
import { fadeIn } from '../../../shared/animations/fade.animation';
import { SportsBookLobbyWidget } from '../../models/lobby-widget';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

export interface IconNavigation {
  navItemName: string;
  icon: string;
}

@Component({
  selector: 'app-sportsbook-sports-lobby',
  templateUrl: './sports-lobby.component.html',
  styleUrls: ['./sports-lobby.component.scss'],
  animations: [fadeIn]
})

export class SportsbookSportsLobbyComponent implements OnInit, OnDestroy {
  @Input() sportLobbyWidget: any;
  @Input() sportLobbyWidgetBetItem: any;
  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;
  progressLoader: boolean = true;

  widgets: SportsBookLobbyWidget[] = [
    new SportsBookLobbyWidget({
      header: 'What´s up',
      cols: 1,
      rows: 1,
      icon: 'whatsup',
      categories: [
        {
          name: 'Football',
          sportId: 29,
          competitionId: 1980,
          marketCategory: 1
        },
        {
          name: 'CS:GO',
          sportId: 12006,
          competitionId: 209276,
          marketCategory: 1
        }
      ]
    }),
    new SportsBookLobbyWidget({
      header: 'Tennis',
      cols: 1,
      rows: 1,
      icon: 'whatsup',
      categories: []
    }),
    new SportsBookLobbyWidget({
      header: 'Icehockey',
      cols: 1,
      rows: 1,
      icon: 'whatsup',
      categories: []
    }),
    new SportsBookLobbyWidget({
      header: 'Cricket',
      cols: 1,
      rows: 1,
      icon: 'whatsup',
      categories: []
    })
  ];

  iconNavSports: IconNavigation[] = [
    {navItemName: 'Football', icon: 'football'},
    {navItemName: 'Futsal', icon: 'football'},
    {navItemName: 'Basket', icon: 'basketball'},
    {navItemName: 'Golf', icon: 'golf'},
    {navItemName: 'Esport', icon: 'esport'},
    {navItemName: 'Boxing', icon: 'boxing'},
    {navItemName: 'American football', icon: 'american_football'},
    {navItemName: 'Baseball', icon: 'baseball'},
    {navItemName: 'Softball', icon: 'baseball'},
    {navItemName: 'Cricket', icon: 'cricket'},
    {navItemName: 'Dart', icon: 'dart'},
    {navItemName: 'Handball', icon: 'handball'},
    {navItemName: 'Icehockey', icon: 'icehockey'},
    {navItemName: 'MMA', icon: 'mma'},
    {navItemName: 'Rugby League', icon: 'rugby'},
    {navItemName: 'Rugby Union', icon: 'rugby'},
    {navItemName: 'Rugby Union Sevens', icon: 'rugby'},
    {navItemName: 'Rugby League Nines', icon: 'rugby'},
    {navItemName: 'Rugby', icon: 'rugby'},
    {navItemName: 'Ski jumping', icon: 'ski_jumping'},
    {navItemName: 'Snooker', icon: 'snooker'},
    {navItemName: 'Tennis', icon: 'tennis'},
    {navItemName: 'Volleyball', icon: 'volleyball'},
    {navItemName: 'Beach volleyball', icon: 'volleyball'},
    {navItemName: 'Volleyball (Points)', icon: 'volleyball'},
    {navItemName: 'Horse racing', icon: 'horse_racing'},
    {navItemName: 'Floorball', icon: 'floorball'},
    {navItemName: 'Table tennis', icon: 'table_tennis'},
    {navItemName: 'Olympics', icon: 'olympics'},
    {navItemName: 'Water polo', icon: 'water_polo'},
    {navItemName: 'Alpine skiing', icon: 'alpine_skiing'},
    {navItemName: 'Freestyle skiing', icon: 'alpine_skiing'},
    {navItemName: 'Snowboarding', icon: 'snowboarding'},
    {navItemName: 'F1', icon: 'f1'},
    {navItemName: 'Motorsport', icon: 'f1'},
    {navItemName: 'Badminton', icon: 'badminton'},
    {navItemName: 'Cycling', icon: 'cycling'},
    {navItemName: 'Figure skating', icon: 'ice_skating'},
    {navItemName: 'Short track', icon: 'ice_skating'},
    {navItemName: 'Curling', icon: 'curling'},
    {navItemName: 'Cross country', icon: 'cross_country'},
    {navItemName: 'Nordic combined', icon: 'cross_country'},
    {navItemName: 'Bobsleigh', icon: 'bobsleigh'},
    {navItemName: 'Biathlon', icon: 'biathlon'},
    {navItemName: 'Luge', icon: 'luge'},
    {navItemName: 'Skeleton', icon: 'skeleton'},
    {navItemName: 'Squash', icon: 'squash'},
    {navItemName: 'Bandy', icon: 'floorball'},
    {navItemName: 'Chess', icon: 'chess'},
    {navItemName: 'Poker', icon: 'poker'},
    {navItemName: 'Crossfit', icon: 'crossfit'},
    {navItemName: 'Politics', icon: 'politics'},
    {navItemName: 'Entertainment', icon: 'entertainment'},
    {navItemName: 'Drone racing', icon: 'drone_racing'},
  ];

  constructor(
    private _portalService: PortalService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private _domSanitizer: DomSanitizer,
    private _matIconRegistry: MatIconRegistry
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;

    this._matIconRegistry.addSvgIcon(
      'american_football',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/american-football.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'baseball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/baseball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'basketball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/basketball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'boxing',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/boxing.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'cricket',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/cricket.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'dart',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/dart.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'esport',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/esport.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'football',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/football.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'golf',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/golf.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'handball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/handball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'icehockey',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/icehockey.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'mma',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/mma.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'rugby',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/rugby.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'ski_jumping',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/ski_jumping.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'snooker',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/snooker.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'tennis',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/tennis.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'volleyball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/volleyball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'horse_racing',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/horse_racing.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'floorball',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/floor_ball.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'table_tennis',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/table_tennis.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'olympics',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/olympics.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'water_polo',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/water_polo.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'alpine_skiing',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/alpine_skiing.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'snowboarding',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/snowboarding.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'f1',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/f1.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'badminton',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/badminton.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'cycling',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/cycling.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'ice_skating',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/ice_skating.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'curling',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/curling.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'cross_country',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/cross_country.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'bobsleigh',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/bobsleigh.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'biathlon',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/biathlon.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'luge',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/luge.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'skeleton',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/skeleton.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'squash',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/squash.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'chess',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/chess.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'poker',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/poker.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'crossfit',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/crossfit.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'politics',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/politics.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'entertainment',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/entertainment.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'drone_racing',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../../../../assets/icons/drone_racing.svg')
    );
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.progressLoader = false;
    }, 1000);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
