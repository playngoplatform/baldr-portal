// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription, zip } from 'rxjs';
import { first } from 'rxjs/operators';
import { fadeIn } from 'src/app/shared/animations/fade.animation';
import { SportsBookLobbyWidget, SportsBookLobbyWidgetCategory } from 'src/app/sportsbook/models/lobby-widget';
import { SportsbookSportCompetition, SportsbookSportCompetitionFixture } from 'src/app/sportsbook/models/sportsbook-data';
import { SportsbookDataFeedService } from 'src/app/sportsbook/services/data-feed.service';
import { SportsbookEngineService } from 'src/app/sportsbook/services/sportsbook-engine.service';

@Component({
  selector: 'app-sportsbook-sports-lobby-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss'],
  animations: [fadeIn]
})
export class SportsbookSportsLobbyWidgetComponent implements OnInit, OnDestroy {
  @Input() widget: SportsBookLobbyWidget;
  private _dataFeedCompetitionEvent$: Subscription;
  loadingFixture: boolean = true;
  categoryCompetitionData: { [key: number]: { competition: SportsbookSportCompetition, categoryId: number } } = {};
  displayMarketStatus: number[] = [0, 2];

  constructor(
    public dataFeedService: SportsbookDataFeedService,
    private _sportsbookEngineService: SportsbookEngineService
  ) { }

  ngOnInit(): void {
    this._dataFeedCompetitionEvent$ = this.dataFeedService.dataCompetitionUpdateEvent$.subscribe(res => {
      for (let i = 0; i < this.widget.categories.length; i++) {
        const category = this.widget.categories[i];
        if (category.sportId === res.sportId && category.competitionId === res.id) {
          this.categoryCompetitionData[category.sportId] = {
            competition: null,
            categoryId: null
          };
          res.fixtures = this._processFixtures(res.fixtures, category);
          this.categoryCompetitionData[category.sportId].competition = res;
          this.categoryCompetitionData[category.sportId].categoryId = this._getCategoryId(category.sportId);
        }
      }
    });
    const requests = [];
    this.widget.categories.forEach(item => {
      if (item.sportId !== undefined && item.competitionId !== undefined) {
        requests.push(this.getCompetitionFixturesAndMarkets(item.sportId, item.competitionId));
      }
    });
    zip(...requests).subscribe(() => {
      this.loadingFixture = false;
    });
  }

  getCompetitionFixturesAndMarkets(sportId: number, competitionId: number): Observable<{
    sportId: number, competitionId: number, fixturesAndMarkets: SportsbookSportCompetitionFixture[]
  }> {
    return this._sportsbookEngineService.getCompetitionFixturesAndMarkets(sportId, competitionId).pipe(first());
  }

  private _getCategoryId(sportId: number): number {
    const sport = this.dataFeedService.dataFeed.sports.find(sportItem => sportItem.id === sportId);
    return sport ? sport.categoryId : null;
  }

  private _processFixtures(
    fixtures: SportsbookSportCompetitionFixture[],
    widgetCategory: SportsBookLobbyWidgetCategory
  ): SportsbookSportCompetitionFixture[] {
    const processedFixtures = this._filterMarkets(fixtures, widgetCategory);
    const filteredFixtures = processedFixtures.filter(fixture => fixture.markets.length > 0);
    return this._sortFixturesByStartTime(filteredFixtures);
  }

  private _filterMarkets(
    fixtures: SportsbookSportCompetitionFixture[],
    widgetCategory: SportsBookLobbyWidgetCategory
  ): SportsbookSportCompetitionFixture[] {
    if (widgetCategory.marketCategory) {
      for (let f = 0; f < fixtures.length; f++) {
        fixtures[f].markets = fixtures[f].markets.filter(market => market.marketCategory === widgetCategory.marketCategory);
        if (fixtures[f].markets.length > 1) {
          fixtures[f].markets = fixtures[f].markets.filter(market => market.selections.length > 2);
        }
      }
    }
    return fixtures;
  }

  private _sortFixturesByStartTime(fixtures: SportsbookSportCompetitionFixture[]) {
    return fixtures.sort((a, b) => a.scheduledAt.isBefore(b.scheduledAt) ? -1 : 1 );
  }

  ngOnDestroy() {
    this._dataFeedCompetitionEvent$.unsubscribe();
  }
}
