// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportsbookSportsLobbyWidgetMarketBetItemComponent } from './market-bet-item.component';

describe('SportsbookSportsLobbyWidgetMarketBetItemComponent', () => {
  let component: SportsbookSportsLobbyWidgetMarketBetItemComponent;
  let fixture: ComponentFixture<SportsbookSportsLobbyWidgetMarketBetItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SportsbookSportsLobbyWidgetMarketBetItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SportsbookSportsLobbyWidgetMarketBetItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
