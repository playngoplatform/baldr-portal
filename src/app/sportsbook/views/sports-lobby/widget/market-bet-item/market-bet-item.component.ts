// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { Subscription } from 'rxjs';
import { pairwise, startWith } from 'rxjs/operators';
import { SportsbookBetslipItem } from 'src/app/sportsbook/models/betslip';
import {
  SportsbookSportCompetitionFixture,
  SportsbookSportCompetitionFixtureMarket,
  SportsbookSportCompetitionFixtureMarketSelectionType
} from 'src/app/sportsbook/models/sportsbook-data';
import { SportsbookBetslipService } from 'src/app/sportsbook/services/betslip.service';

@Component({
  selector: 'app-sportsbook-sports-lobby-widget-market-bet-item',
  templateUrl: './market-bet-item.component.html',
  styleUrls: ['./market-bet-item.component.scss']
})
export class SportsbookSportsLobbyWidgetMarketBetItemComponent implements OnInit, OnChanges, OnDestroy {
  @Input() market: SportsbookSportCompetitionFixtureMarket;
  @Input() fixture: SportsbookSportCompetitionFixture;
  @Input() categoryId: number;
  private _marketSelectionForm$: Subscription;
  private _betslipEvent$: Subscription;
  marketSelectionForm: FormGroup = new FormGroup({ selection: new FormControl() });

  constructor(
    private _betslipService: SportsbookBetslipService,
    private _router: Router,
    private _localize: LocalizeRouterService
  ) { }

  ngOnInit(): void {
    this._marketSelectionForm$ = this.marketSelectionForm.valueChanges.pipe(
      startWith(<{ selection: number[] }>null),
      pairwise()
    ).subscribe(([prev, next]: [{ selection: number[] }, { selection: number[] }]) => {
      if (next.selection.length > 1 && prev) {
        const oldSelection = next.selection.find(selection => prev.selection.includes(selection));
        const newSelection = next.selection.find(selection => !prev.selection.includes(selection));
        this._betslipService.removeSelection(this._createBetslipItem(oldSelection));
        this._betslipService.addSelection(this._createBetslipItem(newSelection));
        this.marketSelectionForm.controls.selection.setValue([newSelection]);
        return;
      }
      if (prev === null && next.selection.length > 1) {
        this._betslipService.removeSelection(this._createBetslipItem(next.selection[0]));
        this._betslipService.addSelection(this._createBetslipItem(next.selection[1]));
        this.marketSelectionForm.controls.selection.setValue([next.selection[1]]);
        return;
      }
      if (next.selection.length === 0 && prev) {
        this._betslipService.removeSelection(this._createBetslipItem(prev.selection[0]));
        return;
      }
      if (next && next.selection.length === 1) {
        this._betslipService.addSelection(this._createBetslipItem(next.selection[0]));
      } else {
        this._removeAllSelectionsFromBetslip();
      }
    });
    this._betslipEvent$ = this._betslipService.updateEvent$.subscribe(() => {
      this._markSelectionsInBetslip(this.market);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.market && this.market && this.market.type.marketTypeId === 112) {
      const first = this.market.selections.find(
        selection => selection.type === SportsbookSportCompetitionFixtureMarketSelectionType.FirstParticipant
      );
      const second = this.market.selections.find(
        selection => selection.type === SportsbookSportCompetitionFixtureMarketSelectionType.LastParticipant
      );
      const draw = this.market.selections.find(selection => selection.type === SportsbookSportCompetitionFixtureMarketSelectionType.Draw);
      this.market.selections = [first, draw, second];
    }
    if (changes.market && changes.market.currentValue && changes.market.previousValue) {
      for (let i = 0; i < changes.market.currentValue.selections.length; i++) {
        const selectionCurrent = changes.market.currentValue.selections[i];
        const selectionPrevious = changes.market.previousValue.selections.find(item => item.selectionId === selectionCurrent.selectionId);
        this.market.selections[i].isPriceUpdated = selectionPrevious &&
          selectionCurrent &&
          selectionCurrent.price !== selectionPrevious.price;
      }
    }
    this._markSelectionsInBetslip(changes.market.currentValue);
  }

  navigateToEvent(): void {
    this._router.navigate([this._localize.translateRoute('/sportsbook/event')]);
  }

  private _removeAllSelectionsFromBetslip(): void {
    this.market.selections.forEach(selection => {
      this._betslipService.removeSelection(this._createBetslipItem(selection.selectionId));
    });
  }

  private _createBetslipItem(selectionId: number): SportsbookBetslipItem {
    const betslipItem = new SportsbookBetslipItem();
    betslipItem.sportId = this.fixture.sportId;
    betslipItem.fixtureId = this.fixture.fixtureId;
    betslipItem.marketId = this.market.marketId;
    betslipItem.selectionId = selectionId;
    betslipItem.marketSeqNumber = this.market.marketSeqNum;
    betslipItem.fixture = this.fixture;
    betslipItem.market = this.market;
    betslipItem.selection = this.market.selections.find(selection => selection.selectionId === selectionId);
    return betslipItem;
  }

  private _markSelectionsInBetslip(market: SportsbookSportCompetitionFixtureMarket): void {
    let selected = false;
    market.selections.forEach(selection => {
      if (this._betslipService.isSelectionInBetslip(selection.selectionId) && !selected) {
        this.marketSelectionForm.controls.selection.setValue([selection.selectionId]);
        selected = true;
        return;
      }
    });
    if (!selected) {
      this.marketSelectionForm.controls.selection.setValue([], { emitEvent: false });
    }
  }

  ngOnDestroy(): void {
    this._betslipEvent$.unsubscribe();
    this._marketSelectionForm$.unsubscribe();
  }
}
