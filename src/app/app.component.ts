// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { UserModel, AuthenticationService } from './core';
import { Subscription } from 'rxjs';
import { ConfigService } from './core/services/config.service';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('accountDrawer') accountDrawer: MatSidenav;
  private _currentPlayer$: Subscription;
  currentPlayer: UserModel;

  constructor(
    private _localize: LocalizeRouterService,
    private _authenticationService: AuthenticationService,
    public configService: ConfigService
  ) {}

  ngOnInit() {
    this._currentPlayer$ = this._authenticationService.currentPlayer$.subscribe(res => {
      this.currentPlayer = res;
    });
  }

  changeLanguage(lang: string): void {
    this._localize.changeLanguage(lang);
  }

  ngOnDestroy(): void {
    this._currentPlayer$.unsubscribe();
  }
}
