// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasinoRoutingModule } from './casino-routing.module';
import { CasinoSharedModule } from './shared/shared.module';

// Components
import { CasinoComponent } from './casino.component';
import { HomeComponent } from './home/home.component';
import { BannerComponent } from './home/banner/banner.component';
import { SiteTermsComponent } from './shared/layout/site-terms/site-terms.component';
import { ModalModule } from './modal/modal.module';
import { ToasterModule } from 'angular2-toaster';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import { CasinoContextMenuComponent } from './shared/casino-context-menu/casino-context-menu.component';

@NgModule({
  declarations: [
    CasinoComponent,
    HomeComponent,
    BannerComponent,
    SiteTermsComponent,
    CasinoContextMenuComponent
  ],
  imports: [
    CommonModule,
    CasinoRoutingModule,
    CasinoSharedModule,
    ModalModule,
    ToasterModule,
    MaterialModule,
    FlexLayoutModule,
    SharedModule
  ]
})
export class CasinoModule { }
