// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, Input, DoCheck, HostListener } from '@angular/core';

import {ItemCarousel, ItemGame} from '../../../core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.less']
})
export class NewsComponent implements DoCheck {
  @Input() itemsSlider: ItemCarousel[];
  @Input() items: ItemGame[] = [];
  @Input() handleClick: Function;
  @Input() isLoading: boolean;
  @Input() title = '';
  @Input() code = '';
  isActiveSliderTiles = false;
  isActiveAllTiles = false;
  public windowWidth: number = window.innerWidth;

  constructor() { }

  @HostListener('window:resize') onWindowResize() {
    this.windowWidth = window.innerWidth;
  }

  ngDoCheck() {
    if ((!this.isLoading && this.itemsSlider.length && this.items.length > 6) ||
      (!this.isLoading && !this.itemsSlider.length && this.items.length > 10)) {
      this.isActiveSliderTiles = true;
    } else {
      this.isActiveSliderTiles = false;
    }
  }

  isActiveSlider() {
    return !this.isLoading && this.itemsSlider.length;
  }

  setActiveAllTiles() {
    this.isActiveSliderTiles = false;
    this.isActiveAllTiles = true;
  }

  getShowButtonBlockCaption() {
    return ((!this.isLoading && this.itemsSlider.length && this.items.length > 6) ||
      (!this.isLoading && !this.itemsSlider.length && this.items.length > 10)) && !this.isActiveAllTiles;
  }

  getItems() {
    return this.isActiveAllTiles && this.isActiveSlider() ? this.items.slice(0, 6) : this.items;
  }
}
