// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {Component, Input } from '@angular/core';

@Component({
  selector: 'app-block-caption',
  templateUrl: './block-caption.component.html',
  styleUrls: ['./block-caption.component.less']
})
export class BlockCaptionComponent {
  @Input() title = '';
  @Input() isShowButton = false;
  @Input() handleClick: Function;
  @Input() id = '';

  constructor() {
  }
}
