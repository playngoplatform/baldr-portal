// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { BlockCaptionComponent } from './block-caption.component';

describe('BlockCaptionComponent', () => {
  const component = new BlockCaptionComponent();

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
