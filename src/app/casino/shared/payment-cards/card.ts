// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export class Card {
  constructor(
    public image: string,
    public title: string,
    public className: string) {}
}
