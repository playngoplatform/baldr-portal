// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';

import { Card } from './card';

@Component({
  selector: 'app-payment-cards',
  templateUrl: './payment-cards.component.html',
  styleUrls: ['./payment-cards.component.less']
})
export class PaymentCardsComponent implements OnInit {
  items: Card[];

  constructor() {
    this.items = [
      new Card('nets.svg', 'Nets', 'nets'),
      new Card('visa.svg', 'VISA', 'visa'),
      new Card('mc_vrt_pos.svg', 'MasterCard', 'masterCard')
    ];
  }

  ngOnInit() {
  }

}
