// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, Input, Inject } from '@angular/core';
import { ItemGame, GameService, GameUtilsService, AuthenticationService, GameMode } from 'src/app/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Component({
  selector: 'app-game-launcher-buttons',
  templateUrl: './game-launcher-buttons.component.html',
  styleUrls: ['./game-launcher-buttons.component.less']
})
export class GameLauncherButtonsComponent implements OnInit {
  @Input() data: ItemGame;
  @Input() link: string = '/';
  @Input() bigImage: boolean = false;

  private _gameIsLaunching: boolean = false;

  constructor(
    @Inject(DOCUMENT) private _document: Document,
    private _gameService: GameService,
    private _gameUtilsService: GameUtilsService,
    private _ngxSmartModelService: NgxSmartModalService,
    private _authenticationService: AuthenticationService,
    private _router: Router,
    private _localize: LocalizeRouterService
  ) { }

  ngOnInit() {
  }

  openDemoGame(): void {
    this._openGame(GameMode.Demo);
  }

  openRealGame(): void {
    const isLoggedIn = this._authenticationService.currentUser();
    if (!isLoggedIn) {
      this._router.navigate([this._localize.translateRoute('/casino/login')]);
      return;
    }
    this._openGame(GameMode.Real);
  }

  private _openGame(mode: GameMode): void {
    if (this._gameIsLaunching) {
      return;
    }
    const { provider, gameId, width, height } = this.data;
    const gameContainer = this._gameService.gameContainerRef;

    this._gameUtilsService.openGame(
      { provider, gameId, width, height },
      mode,
      gameContainer,
      () => {
        this._document.body.classList.add('active-modal-game');
        this._ngxSmartModelService.getModal('gameModal').open();
        this._gameIsLaunching = false;
      });

    this._gameIsLaunching = true;
  }

}
