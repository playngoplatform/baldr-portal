// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { GameLauncherButtonsComponent } from './game-launcher-buttons.component';

describe('GameLauncherButtonsComponent', () => {
  let component: GameLauncherButtonsComponent;
  let fixture: ComponentFixture<GameLauncherButtonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GameLauncherButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameLauncherButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
