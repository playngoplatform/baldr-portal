// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';

import { AppService, LocalesModel, LocalesAgeValidationModel } from '../../../../core';
import * as $ from 'jquery';

@Component({
  selector: 'app-site-terms',
  templateUrl: './site-terms.component.html',
  styleUrls: ['./site-terms.component.less']
})
export class SiteTermsComponent implements OnInit, AfterViewInit {
  public open = false;
  public data: LocalesAgeValidationModel = {};

  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    private appService: AppService
  ) {
    this.appService.locales$.subscribe((data: LocalesModel) => {
      this.data = data.ageValidation;
    });
  }

  ngOnInit() {
    this.initPage();
  }

  initPage() {
    const permission = window.localStorage.getItem('permission');
    if (!permission) {
      $('.page').addClass('blur-block');
      this.open = true;
    }
  }

  ngAfterViewInit() {
    this.ngxSmartModalService.getModal('siteTerms').onClose.subscribe(() => {
      $('.page').removeClass('blur-block');
    });
  }

  onAgree() {
    this.ngxSmartModalService.close('siteTerms');
    window.localStorage.setItem('permission', 'true');
  }

  onDesagree() {
    window.location.href = 'http://google.com';
  }
}
