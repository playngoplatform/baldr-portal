// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SiteTermsComponent } from './site-terms.component';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { ApiService, AppService } from '../../../../core/services';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { LocalizeRouterMockService } from '../../../../../../tests/localize-router-mock-service';

describe('SiteTermsComponent', () => {
  let component: SiteTermsComponent;
  let fixture: ComponentFixture<SiteTermsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxSmartModalModule,
        HttpClientModule,
        HttpClientTestingModule
      ],
      declarations: [ SiteTermsComponent ],
      providers: [
        NgxSmartModalService,
        AppService,
        ApiService,
        {provide: LocalizeRouterService, useValue: LocalizeRouterMockService},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
