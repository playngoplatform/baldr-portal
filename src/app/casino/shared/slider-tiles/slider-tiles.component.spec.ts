// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { SliderTilesComponent } from './slider-tiles.component';
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { LazyLoadImageModule } from 'ng-lazyload-image';

describe('SliderTilesComponent', () => {
  let component: SliderTilesComponent;
  let fixture: ComponentFixture<SliderTilesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [LazyLoadImageModule],
      declarations: [ SliderTilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderTilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
