// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
  NgZone,
  AfterViewChecked
} from '@angular/core';

import { ItemGame, ItemNews } from '../../../core';

@Component({
  selector: 'app-slider-tiles',
  templateUrl: './slider-tiles.component.html',
  styleUrls: ['./slider-tiles.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class SliderTilesComponent implements AfterViewInit, OnChanges, OnDestroy, AfterViewChecked {
  @Input() items: ItemNews[] | ItemGame[];
  @Input() isActiveSlider = false;
  @Input() handleClick: Function;
  @ViewChild('slider', { static: true }) sliderRef: ElementRef;
  private slider: any;
  private isUpdateSlider = false;

  constructor(private zone: NgZone) { }
  ngAfterViewInit() {
    this.initSlider();
  }

  ngOnChanges(changes: SimpleChanges) {
    const { items: { currentValue, previousValue }} = changes;
    if (previousValue && (currentValue.length !== previousValue.length)) {
      this.isUpdateSlider = true;
      this.destroySlider();
    } else {
      this.isUpdateSlider = false;
    }
  }

  ngAfterViewChecked() {
    if (this.isUpdateSlider) { this.initSlider(); }
  }

  getCountSlides() {
    return this.isActiveSlider ? 3 : 5;
  }

  initSlider() {
    this.zone.runOutsideAngular(() => {
      this.slider = jQuery(this.sliderRef.nativeElement).owlCarousel({
        margin: 0,
        loop: false,
        items: this.getCountSlides(),
        dots: true,
        nav: false,
        slideBy: 5,
        responsiveClass: true,
        responsive: {
          0: {
            items: 2,
            slideBy: 2,
            nav: true,
            dots: false
          },
          768: {
            items: 3,
            slideBy: 3,
            nav: false,
            dots: true
          },
          992: {
            items: this.getCountSlides(),
            slideBy: 3,
            nav: false,
            dots: true
          }
        }
      });
    });
  }

  destroySlider() {
    this.slider.trigger('destroy.owl.carousel');
  }

  ngOnDestroy() {
    this.destroySlider();
  }
}
