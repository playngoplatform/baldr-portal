// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {
  Component,
  AfterViewInit,
  Input,
  OnChanges,
  SimpleChanges,
  NgZone,
  ViewChild,
  ElementRef,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';

import { ItemCategory, UtilsService } from '../../../core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements AfterViewInit, OnChanges, OnDestroy {
  @Input() items: ItemCategory[];
  private slider: any;
  @ViewChild('sliderMenu', { static: true }) sliderRef: ElementRef;
  @Input() search: string;
  @Output() searchChange = new EventEmitter<string>();

  constructor(
    private zone: NgZone,
    private utilsService: UtilsService
  ) { }


  onSearchChange(model: string) {
    this.search = model;
    this.searchChange.emit(model);
  }

  ngAfterViewInit() {
    this.initSlider();
  }


  ngOnDestroy() {
    this.destroySlider();
  }

  initSlider() {
    this.zone.runOutsideAngular(() => {
      const slider = jQuery(this.sliderRef.nativeElement);
      const per = this.slider = slider.find('.slider-games');
      if (per.hasClass('owl-loaded') ) {
      } else {
        per.owlCarousel({
          dots: false,
          nav: true,
          responsive: {
            0: {
              items: 3
            },
            600: {
              items: 4
            },
            1199: {
              items: 5
            }
          }
        });
      }
    });
  }

  destroySlider() {
    if (this.slider && this.slider.trigger) {
      this.slider.trigger('destroy.owl.carousel');
    }
  }

  updateSlider() {
    this.destroySlider();
    setTimeout(() => {
      this.initSlider();
    }, 100);

  }


  ngOnChanges(changes: SimpleChanges) {
    if (changes.items) {
      const { items: { currentValue, previousValue }} = changes;
      if (previousValue && (currentValue.length !== previousValue.length) ) {
        this.updateSlider();
      } else {
        this.updateSlider();
      }
    }
  }

  onClick(event, id) {
    event.stopPropagation();
    event.preventDefault();

    this.utilsService.scrollTo(`#${id}`, -100);
  }

}
