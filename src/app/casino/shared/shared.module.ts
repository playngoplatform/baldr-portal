// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ReactiveFormsModule } from '@angular/forms';
import { MenuComponent } from './menu/menu.component';
import { NewsComponent } from './news/news.component';
import { SliderTilesComponent } from './slider-tiles/slider-tiles.component';
import { BlockCaptionComponent } from './block-caption/block-caption.component';
import { PaymentCardsComponent } from './payment-cards/payment-cards.component';
import { FormsModule } from '@angular/forms';
import { ModalConfirmationComponent } from './modal/confirmation/confirmation.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { GameLauncherButtonsComponent } from './game-launcher-buttons/game-launcher-buttons.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    LazyLoadImageModule,
    LocalizeRouterModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSmartModalModule,
    InfiniteScrollModule
  ],
  declarations: [
    MenuComponent,
    NewsComponent,
    BlockCaptionComponent,
    GameLauncherButtonsComponent,
    SliderTilesComponent,
    PaymentCardsComponent,
    ModalConfirmationComponent,
    GameLauncherButtonsComponent
  ],
  exports: [
    MenuComponent,
    NewsComponent,
    BlockCaptionComponent,
    GameLauncherButtonsComponent,
    SliderTilesComponent,
    PaymentCardsComponent,
    TranslateModule,
    ModalConfirmationComponent,
    NgxSmartModalModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule
  ]
})
export class CasinoSharedModule { }
