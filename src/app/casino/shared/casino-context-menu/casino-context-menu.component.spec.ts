// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CasinoContextMenuComponent } from './casino-context-menu.component';

describe('SportsContextMenuComponent', () => {
  let component: CasinoContextMenuComponent;
  let fixture: ComponentFixture<CasinoContextMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CasinoContextMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CasinoContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
