// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-modal-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.less']
})
export class ModalConfirmationComponent implements OnInit {

  constructor(
    public ngxSmartModalService: NgxSmartModalService
  ) { }

  ngOnInit() { }

  confirm(): void {
    const confirmationModal = this.ngxSmartModalService.getModal('confirmation');
    confirmationModal.setData({
      response: true
    }, true);
    confirmationModal.close();
  }
}
