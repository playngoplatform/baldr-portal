// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';
import { NotFoundService, NotFoundModel } from '../../core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.less']
})
export class NotFoundComponent implements OnInit {
  data: NotFoundModel = {};
  constructor(
    private notFoundService: NotFoundService
  ) { }

  ngOnInit() {
    this.getDataPage();
  }

  getDataPage() {
    this.notFoundService.get()
      .subscribe(data => {
        this.data = data;
      });
  }
}
