// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { CasinoComponent } from './casino.component';
import { AuthGuardService as AuthGuard } from '../core/services/auth-guard.service';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { BonusComponent } from './bonus/bonus.component';

const routes: Routes = [
  {
    path: '',
    component: CasinoComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
        pathMatch: 'full'
      },
      {
        path: 'bonus',
        component: BonusComponent
      },
      {
        path: '**',
        loadChildren: () => import('./not-found/not-found.module').then(m => m.NotFoundModule)
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LocalizeRouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
    LocalizeRouterModule
  ],
  providers: [AuthGuard]
})
export class CasinoRoutingModule {}
