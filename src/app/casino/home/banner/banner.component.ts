// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';

import { HomeService, BannerModel } from '../../../core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.less']
})
export class BannerComponent implements OnInit {
  public items: BannerModel[] = [];

  constructor(private homeService: HomeService) {}

  ngOnInit() {
    this.getDataPage();
  }

  getDataPage() {
    this.homeService.getBanners().subscribe(items => {
      this.items = items;
    });
  }

  getLink(item: BannerModel) {
    const { action, object_id: objectId } = item;
    if (action === 'play') { return `game/${objectId}`; }
    return `bonus/${objectId}`;
  }

}
