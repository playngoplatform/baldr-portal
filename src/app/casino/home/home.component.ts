// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {
  AppService,
  AuthenticationService,
  GameUtilsService,
  HomeModel,
  HomeService,
  ItemCategory,
  UtilsService,
  GameService
} from '../../core';
import { NavigationStart, Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('game', { static: true }) gameContainerRef: ElementRef;
  @ViewChild('login') login: ElementRef;

  categoriesGames: ItemCategory[] = [];
  data: HomeModel = {};
  isLoading = false;
  searchStr = '';
  navigationSubscription;
  showRegistrationPopup = false;
  activeProvider = '';

  constructor(
    private homeService: HomeService,
    public ngxSmartModalService: NgxSmartModalService,
    private utilsService: UtilsService,
    private gameUtilsService: GameUtilsService,
    private appService: AppService,
    private authService: AuthenticationService,
    public router: Router,
    private _gameService: GameService
  ) {
    this.appService.categoriesGames$.subscribe(data => {
      this.categoriesGames = data;
    });
    this.authService.isAuthenticated$.subscribe((() => {
      this.getDataPage();
    }));

    router.events.subscribe(
      (event: NavigationStart) => {
        if (event.navigationTrigger === 'popstate') {
          this.resetGameWindow();
        }
      });
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.getDataPage();
    this._gameService.gameContainerRef = this.gameContainerRef;
  }

  ngAfterViewInit() {
    this.ngxSmartModalService.getModal('gameModal').onAnyCloseEvent.subscribe(() => {
      this.resetGameWindow();
    });
  }

  filterData(data) {
    return {
      ...data,
      games: data.games.filter(item => this.utilsService.isMobile() ? item.isMobile : item.isDesktop)
    };
  }

  resetGameWindow() {
    $('body').removeClass('active-modal-game');
    this.gameUtilsService.closeGame();
  }

  getDataPage() {
    this.isLoading = true;

    this.homeService.getAll()
      .subscribe(data => {
        this.isLoading = false;
        this.data = this.filterData(data);
      });
  }

  getItemsNews(code) {
    const items = this.data.games || [];
    const search = this.searchStr.toLowerCase().trim();
    const provider = this.activeProvider;
    return items.reduce((acc, item) => {
      return item.category === code && item.name.toLowerCase().includes(search)
        && (!provider || item.provider === provider.toLowerCase()) ? [...acc, item] : acc;
    }, []);
  }

  getItemsSliderNews(code) {
    const items = this.data.carousels || [];
    return items.reduce((acc, item) => (item.category === code ? [...acc, item] : acc), []);
  }

}
