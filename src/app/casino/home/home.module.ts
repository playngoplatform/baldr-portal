// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { CasinoSharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { BannerComponent } from './banner/banner.component';
import { RegistrationModule } from '../registration/registration.module';
import { CustomerServiceModule } from '../customer-service/customer-service.module';
import { TermsConditionsModule } from '../shared/terms-conditions/terms-conditions.module';
import { ResponsibleGamingModule } from '../responsible-gaming/responsible-gaming.module';
import { PersonalDataPolicyModule } from '../personal-data-policy/personal-data-policy.module';
import { AboutUsModule } from '../about-us/about-us.module';
import { LoginModule } from '../login/login.module';
import { CookiesModule } from '../cookies/cookies.module';

@NgModule({
  declarations: [
    HomeComponent,
    BannerComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CasinoSharedModule,
    RegistrationModule,
    TermsConditionsModule,
    CustomerServiceModule,
    ResponsibleGamingModule,
    PersonalDataPolicyModule,
    AboutUsModule,
    CookiesModule,
    LoginModule
  ]
})
export class HomeModule {
}
