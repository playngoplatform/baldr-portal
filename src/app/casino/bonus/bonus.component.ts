// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';

import { BonusService, BonusesModel, ParseTextModel, UtilsService } from '../../core';

@Component({
  selector: 'app-bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.less']
})
export class BonusComponent implements OnInit {
  data: BonusesModel = {};
  parseText: ParseTextModel;

  constructor(
    private bonuseService: BonusService,
    utilsService: UtilsService
  ) {
    this.parseText = utilsService.parseText;
  }

  ngOnInit() {
    this.getDataPage();
  }

  getDataPage() {
    this.bonuseService.getAll().subscribe(data => {
      this.data = data;
    });
  }

}
