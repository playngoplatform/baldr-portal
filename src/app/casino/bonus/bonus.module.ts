// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BonusRoutingModule } from './bonus-routing.module';
import { CasinoSharedModule } from '../shared/shared.module';

// Components
import { BonusComponent } from './bonus.component';
import {SafeHtml} from '../../core/pipes/safe-html.pipe';

@NgModule({
  declarations: [
    BonusComponent,
    SafeHtml
  ],
  imports: [
    CommonModule,
    BonusRoutingModule,
    CasinoSharedModule
  ]
})
export class BonusModule { }
