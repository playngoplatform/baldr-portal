// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, forwardRef, Inject, OnDestroy, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { UserModel } from '../core';
import { MEDIA_BREAK_POINT } from '../core/models/config.model';
import { ConfigService } from '../core/services/config.service';
import { PortalService } from '../core/services/portal.service';

@Component({
  selector: 'app-casino',
  templateUrl: './casino.component.html',
  styleUrls: ['./casino.component.less']
})
export class CasinoComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;
  currentPlayer: UserModel;

  constructor(
    @Inject(forwardRef(() => AppComponent))
    private _appComponent: AppComponent,
    public configService: ConfigService,
    private _portalService: PortalService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;
   }

  ngOnInit(): void {
    this.currentPlayer = this._appComponent.currentPlayer;
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

}
