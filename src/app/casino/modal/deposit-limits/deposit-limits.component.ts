// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { PlayerService, PlayerProtectionLimits, AuthenticationService } from '../../../core';
import { ToasterService } from 'angular2-toaster';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-modal-deposit-limits',
  templateUrl: './deposit-limits.component.html',
  styleUrls: ['./deposit-limits.component.less']
})
export class ModalDepositLimitsComponent implements OnInit, OnDestroy {
  private _depositLimits$: Subscription;
  depositLimits: FormGroup = new FormGroup({
    single: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    day: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    week: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    month: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)])
  });

  playerLimits: PlayerProtectionLimits;
  enableSaveButton: boolean = false;

  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    private _playerService: PlayerService,
    private _authenticationService: AuthenticationService,
    private _translate: TranslateService,
    private _toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.getPlayerLimits();
    this._depositLimits$ = this.depositLimits.valueChanges.subscribe(
      res => {
        this.enableSaveButton = res.day > 0 || res.week > 0 || res.month > 0;
      }
    );
  }

  getPlayerLimits(): void {
    this._playerService.getProtectionLimits().subscribe(res => {
      this.playerLimits = res.currentLimits;
    });
  }

  numericOnly(event): boolean { // restrict e,+,-,E characters in  input type number
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode === 101 || charCode === 69 || charCode === 45 || charCode === 43) {
      return false;
    }
    return true;
  }

  submitDepositLimits(): void {
    if (this.playerLimits) {
      this.playerLimits.depositLimits.day = this.depositLimits.value.day ? parseInt(this.depositLimits.value.day, 10) : null;
      this.playerLimits.depositLimits.week = this.depositLimits.value.week ? parseInt(this.depositLimits.value.week, 10) : null;
      this.playerLimits.depositLimits.month = this.depositLimits.value.month ? parseInt(this.depositLimits.value.month, 10) : null;
      this.playerLimits.depositLimits.single = this.depositLimits.value.single ? parseInt(this.depositLimits.value.single, 10) : null;
      this._playerService.updateProtectionLimits(this.playerLimits).subscribe(() => {
        this._authenticationService.refresh().subscribe(() => {
          this.ngxSmartModalService.closeLatestModal();
        });
        this._toasterService.pop('success', this._translate.instant('Limits.Deposit_limit'),
        this._translate.instant('Limits.Deposit_limit_updated'));
      }, e => {
        this._toasterService.pop('error', this._translate.instant('Limits.Deposit_limit_failed'), e.error.error);
      });
    }
  }

  ngOnDestroy() {
    this._depositLimits$.unsubscribe();
  }
}
