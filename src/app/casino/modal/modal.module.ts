// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CasinoSharedModule } from '../shared/shared.module';
import { ModalDepositLimitsComponent } from '../modal/deposit-limits/deposit-limits.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RealityCheckComponent } from '../modal/reality-check/reality-check.component';

@NgModule({
  declarations: [
    ModalDepositLimitsComponent,
    RealityCheckComponent
  ],
  imports: [
    CommonModule,
    CasinoSharedModule,
    ReactiveFormsModule
  ],
  exports: [
    ModalDepositLimitsComponent,
    RealityCheckComponent
  ]
})
export class ModalModule {
}
