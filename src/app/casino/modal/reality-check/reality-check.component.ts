// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { PlayerService, PlayerBetWinSummary } from '../../../core';
import * as moment from 'moment';
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigService } from '../../../core/services/config.service';

@Component({
  selector: 'app-reality-check',
  templateUrl: './reality-check.component.html',
  styleUrls: ['./reality-check.component.less']
})
export class RealityCheckComponent implements OnInit, OnDestroy {
  private _currentPlayer$: Subscription;
  showRealityCheck: boolean;
  playerCurrency: string;
  totalBet: number;
  totalWin: number;
  resultPlaying: number;
  timer: number;
  timeLoggedIn: moment.Moment;
  lastLoginTime: moment.Moment;
  enabled: boolean;

  constructor(
    private _authenticationService: AuthenticationService,
    private _playerService: PlayerService,
    private _configService: ConfigService
  ) {
    this.timer = this._configService.config.realityCheck.timer;
    this.enabled = this._configService.config.realityCheck.enabled;
  }

  ngOnInit() {
    if (this.enabled) {
      this._currentPlayer$ = this._authenticationService.currentPlayer$.subscribe(res => {
        if (res) {
          this._getLoggedInTime();
          this._startTimer();
        }
      });
    }
  }

  logout(): void {
    this._authenticationService.logout();
    this.showRealityCheck = false;
  }

  closeRealityCheck(): void {
    this.showRealityCheck = false;
    this._startTimer();
  }

  getBetWinSummary(): Observable<PlayerBetWinSummary> {
    const to = moment();
    return this._playerService.getBetWinSummary(this.lastLoginTime, to).pipe(map(res => {
      this.playerCurrency = res.playerCurrency;
      this.totalBet = res.bonusMoneyBet + res.realMoneyBet;
      this.totalWin = res.bonusMoneyWin + res.realMoneyWin;
      this.resultPlaying = this.totalWin - this.totalBet;
      return res;
    }));
  }

  private _getLoggedInTime(): void {
    this.lastLoginTime = this._authenticationService.getLastLoginTime();
    this._calculateTimeLoggedIn();
  }

  private _calculateTimeLoggedIn(): void {
    this.timeLoggedIn = moment.utc(moment.duration(moment().utc().diff(this.lastLoginTime)).asMilliseconds());
  }

  private _startTimer(): void {
    setTimeout(() => {
      this._calculateTimeLoggedIn();
      this.getBetWinSummary().subscribe(
        () => {
          this.showRealityCheck = true;
        },
        () => {
          this.showRealityCheck = true;
        });
    }, this.timer);
  }

  ngOnDestroy() {
    this._currentPlayer$.unsubscribe();
  }
}
