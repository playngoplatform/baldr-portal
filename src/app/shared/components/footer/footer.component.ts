// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { MediaMatcher } from '@angular/cdk/layout';
import { Component, OnInit, ChangeDetectorRef, OnDestroy, Input } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MEDIA_BREAK_POINT } from 'src/app/core/models/config.model';
import { PortalService } from 'src/app/core/services/portal.service';

@Component({
  selector: 'app-shared-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class SharedFooterComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;
  @Input() sportsMenu: MatSidenav;
  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  constructor(
    private _PortalService: PortalService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._PortalService.isMobile;
    this.isTablet = this._PortalService.isTablet;
    this.isDesktop = this._PortalService.isDesktop;
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }
}
