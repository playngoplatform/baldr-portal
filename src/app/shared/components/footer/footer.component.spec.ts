// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedFooterComponent } from './footer.component';
import { LocalizeRouterMockPipe } from '../../../../../tests/localize-router-mock-pipe';
import { RouterTestingModule } from '@angular/router/testing';

describe('SharedFooterComponent', () => {
  let component: SharedFooterComponent;
  let fixture: ComponentFixture<SharedFooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ SharedFooterComponent,
        LocalizeRouterMockPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
