// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import { SharedLoginComponent } from './login.component';
import { SharedNewPasswordModule } from '../new-password/new-password.module';
import { SharedModule } from '../../shared.module';
import { MaterialModule } from 'src/app/material.module';
import { SharedNemidModule } from '../nemid/nemid.module';
@NgModule({
  declarations: [
    SharedLoginComponent
  ],
  exports: [
    SharedLoginComponent
  ],
  imports: [
    LoginRoutingModule,
    CommonModule,
    UiSwitchModule.forRoot({
      size: 'small',
    }),
    TranslateModule,
    ReactiveFormsModule,
    SharedNewPasswordModule,
    SharedModule,
    MaterialModule,
    SharedNemidModule
  ]
})
export class SharedLoginModule { }
