// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { AuthenticationService } from '../../../core/services';
import { UserModel } from 'src/app/core';

@Component({
  selector: 'app-shared-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class SharedLoginComponent implements OnInit {
  @ViewChild('loginTabGroup') tabGroup: any;
  loginForm: FormGroup;
  submitted: boolean = false;
  error: string = '';
  loading: boolean = false;
  hidePassword = true;
  currentPlayer: UserModel;

  forgotPassword: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _authenticationService: AuthenticationService,
    private _localize: LocalizeRouterService,
  ) {
    this.loginForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  ngOnInit() {}

  cancel(): void {
    this._router.navigate([this._localize.translateRoute('/')]);
  }

  showForgotPassword(): void {
    this.forgotPassword = true;
  }

  login(): void {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    const username = this.f.username.value;
    const password = this.f.password.value;
    this._authenticationService.login(username, password).subscribe(
      () => {
        this.loading = false;
        this.error = '';
      },
      e => {
        this.error = e.error.error;
        this.loading = false;
      }
    );
  }

  backToLogin(): void {
    this.forgotPassword = false;
    this.tabGroup.selectedIndex = 1;
  }

  onExternalLogin(tokenType: string, token: string): void {
    this._authenticationService.loginWithToken(tokenType, token).subscribe(
      () => {
        this.loading = false;
        this.error = '';
      },
      e => {
        this.error = e.error.error;
        this.loading = false;
      }
    );
  }
}
