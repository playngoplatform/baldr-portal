// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { SharedLoginComponent } from './login.component';
import { NemidModule } from '../integrations/nemid/nemid.module';

const routes: Routes = [
  {path: '', component: SharedLoginComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    LocalizeRouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
    LocalizeRouterModule,
    NemidModule
  ]
})
export class LoginRoutingModule {}
