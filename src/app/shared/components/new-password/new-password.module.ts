// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedNewPasswordComponent } from './new-password.component';
import { NewPasswordRoutingModule } from './new-password-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared.module';

@NgModule({
  declarations: [
    SharedNewPasswordComponent
  ],
  exports: [
    SharedNewPasswordComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    TranslateModule,
    NewPasswordRoutingModule
  ]
})
export class SharedNewPasswordModule { }
