// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ResetPasswordService, UserModel } from '../../../../../core';
import { TranslateService } from '@ngx-translate/core';
import { SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-shared-modal-request-password-change',
  templateUrl: './request-password-change.component.html',
  styleUrls: ['./request-password-change.component.less']
})
export class SharedModalRequestPasswordChangeComponent implements OnInit, OnChanges {
  @Input() currentPlayer: UserModel;
  loading: boolean = false;
  isLoggedIn: boolean = false;

  playerLoginFormGroup = new FormGroup({
    playerLogin: new FormControl('', Validators.required),
  });

  constructor(
    private _resetPasswordService: ResetPasswordService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {}

  ngOnChanges() {
    if (this.currentPlayer) {
      this.playerLoginFormGroup.controls.playerLogin.setValue(this.currentPlayer.nickname);
    }
  }

  requestPasswordChange(): void {
    if (this.playerLoginFormGroup.controls.playerLogin) {
      this.loading = true;
      this._resetPasswordService.requestPasswordChange(this.playerLoginFormGroup.value.playerLogin).subscribe(() => {
        this.playerLoginFormGroup.controls.playerLogin.reset();
        this.loading = false;
        this._snackBar.open(
          this._translate.instant('Password_change.Sent_password_change_instructions'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }, () => {
        this.loading = false;
        this._snackBar.open(
          this._translate.instant('Password_change.Change_password_request_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }
}
