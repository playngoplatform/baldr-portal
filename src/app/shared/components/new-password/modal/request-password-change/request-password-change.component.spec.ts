// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModalRequestPasswordChangeComponent } from './request-password-change.component';

describe('SharedModalRequestPasswordChangeComponent', () => {
  let component: SharedModalRequestPasswordChangeComponent;
  let fixture: ComponentFixture<SharedModalRequestPasswordChangeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedModalRequestPasswordChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedModalRequestPasswordChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
