// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedNewPasswordComponent } from './new-password.component';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';

const routes: Routes = [{
  path: '',
  component: SharedNewPasswordComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes),
    LocalizeRouterModule.forChild(routes)],
  exports: [RouterModule, LocalizeRouterModule]
})
export class NewPasswordRoutingModule { }
