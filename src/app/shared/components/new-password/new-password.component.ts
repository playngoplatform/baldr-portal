// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticationService, UserModel, ResetPasswordService } from '../../../core';
import { TranslateService } from '@ngx-translate/core';
import { SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-shared-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.less']
})
export class SharedNewPasswordComponent implements OnInit {
  newPassword: FormGroup = this._formBuilder.group({
    password: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(30)]),
    confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(30)])
  }, {
    validator: this.checkPasswords
  });
  token: string;
  currentPlayer: UserModel;

  showForm: boolean = true;
  showSuccess: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _authenticationService: AuthenticationService,
    private _resetPasswordService: ResetPasswordService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.token = this._route.snapshot.queryParamMap.get('token');
    this.currentPlayer = this._authenticationService.currentUser();
  }

  showSuccessArea(): void {
    this.showForm = false;
    this.showSuccess = true;
  }

  checkPasswords(group: FormGroup): { noMatch: true } {
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : { noMatch: true };
  }

  submitUpdatePassword(): void {
    if (this.token && this.newPassword.valid && this.newPassword.value.confirmPassword) {
      this._resetPasswordService.changePassword(this.token, this.newPassword.value.password).subscribe(() => {
        this._snackBar.open(
          this._translate.instant('Password_change.Password_has_been_changed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
        this._authenticationService.logoutPlayer();
        this.showSuccessArea();
      }, () => {
        this._snackBar.open(
          this._translate.instant('Password_change.Password_change_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }
}
