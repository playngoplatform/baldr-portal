// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export class NemidConfig {
  static serverUrl: string = 'https://appletk.danid.dk';
}

export enum NemidMessageCommand {
  SendParameters = 'SendParameters',
  ChangeResponseAndSubmit = 'changeResponseAndSubmit'
}

export class NemidMessage {
  command: string;
  content: string;
}

export class NemidValidateSignatureResponse {
  pid: string;
}

export class NemidLoginParametersResponse {
  clientflow: string;
  digest_signature: string;
  params_digest: string;
  sp_cert: string;
  timestamp: string;
}
