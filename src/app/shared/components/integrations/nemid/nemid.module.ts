// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NemidLoginComponent } from './components/nemid-login/nemid-login.component';
import { NemidService } from './services/nemid.service';

@NgModule({
  declarations: [
    NemidLoginComponent
  ],
  exports: [
    NemidLoginComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    NemidService
  ]
})
export class NemidModule { }
