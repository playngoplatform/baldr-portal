// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, ViewChild, ElementRef, Inject, forwardRef, Optional } from '@angular/core';
import { NemidService } from '../../services/nemid.service';
import { NemidConfig, NemidMessageCommand, NemidMessage } from '../../models/nemid.model';
import { SharedRegistrationComponent } from '../../../../registration/registration.component';
import { ToasterService } from 'angular2-toaster';
import { SharedLoginComponent } from '../../../../login/login.component';
import { EventManager } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-nemid-login',
  templateUrl: './nemid-login.component.html',
  styleUrls: ['./nemid-login.component.less']
})
export class NemidLoginComponent implements OnInit {
  @ViewChild('nemidIframe', { static: true }) iframe: ElementRef;

  private _loginParameters: string;

  constructor(
    @Optional() @Inject(forwardRef(() => SharedRegistrationComponent))
    private _registrationComponent: SharedRegistrationComponent,
    @Optional() @Inject(forwardRef(() => SharedLoginComponent))
    private _loginComponent: SharedLoginComponent,
    private _nemidService: NemidService,
    private _toasterService: ToasterService,
    private _translate: TranslateService,
    private _eventManager: EventManager
  ) { }

  ngOnInit() {
    this._getLoginParameters();
  }

  private _getLoginParameters(): void {
    this._nemidService.getLoginParameters().subscribe(
      parameters => {
        this._loginParameters = JSON.stringify(parameters);
        this._initIframe();
      }
    );
  }

  private _initIframe(): void {
    const noCaching = Date.now();
    this.iframe.nativeElement.src = `${NemidConfig.serverUrl}/launcher/lmt/${noCaching}`;
    this._listenForMessages();
  }

  private _listenForMessages(): void {
    this._eventManager.addGlobalEventListener('window', 'message', this._onNemidMessage.bind(this));
  }

  private _onNemidMessage(event: MessageEvent): void {
    if (event.origin !== NemidConfig.serverUrl) {
      return;
    }
    const message = new NemidMessage();

    try {
      const data = JSON.parse(event.data);
      message.command = data.command;
      message.content = data.content;
    } catch (e) {
      return;
    }

    switch (message.command) {
      case NemidMessageCommand.SendParameters:
        this._sendLoginParameters();
        break;
      case NemidMessageCommand.ChangeResponseAndSubmit:
        this._validateSignatureAndGetPid(message.content);
        break;
    }
  }

  private _sendLoginParameters(): void {
    const iframeWindow = this.iframe.nativeElement.contentWindow;
    if (!iframeWindow) {
      return;
    }
    const postMessage = {
      command: 'parameters',
      content: this._loginParameters
    };
    iframeWindow.postMessage(JSON.stringify(postMessage), NemidConfig.serverUrl);
  }

  private _validateSignatureAndGetPid(signature: string): void {
    this._nemidService.validateSignature(signature).subscribe(
      result => {
        this.iframe.nativeElement.remove();
        if (this._registrationComponent) {
          this._registrationComponent.onExternalLogin(result.pid);
        } else if (this._loginComponent) {
          this._loginComponent.onExternalLogin('NemID', result.pid);
        }
      },
      () => {
        this._toasterService.pop('error', this._translate.instant('Nemid.Nemid_authentication'),
        this._translate.instant('Nemid.Nemid_authentication_failed'));
      }
    );
  }

}
