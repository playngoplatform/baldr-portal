// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/core';
import { Observable } from 'rxjs';
import { NemidValidateSignatureResponse, NemidLoginParametersResponse } from '../models/nemid.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NemidService {

  constructor(
    private _apiService: ApiService
  ) { }

  getLoginParameters(): Observable<NemidLoginParametersResponse> {
    return this._apiService.get('/v1/nemid/login-parameters').pipe(
      map(result => {
        const mappedResult = new NemidLoginParametersResponse();
        mappedResult.clientflow = result.clientflow;
        mappedResult.digest_signature = result.digest_signature;
        mappedResult.params_digest = result.params_digest;
        mappedResult.sp_cert = result.sp_cert;
        mappedResult.timestamp = result.timestamp;
        return mappedResult;
      })
    );
  }

  validateSignature(signature: string): Observable<NemidValidateSignatureResponse> {
    return this._apiService.post('/v1/nemid/validate-signature', { signature: signature }).pipe(
      map(result => {
        const mappedResult = new NemidValidateSignatureResponse();
        mappedResult.pid = result.pid;
        return mappedResult;
      })
    );
  }

  pidCprMatch(pid: string, cpr: string): Observable<any> {
    return this._apiService.post('/v1/nemid/pid-cpr-match', { pid: pid, cpr: cpr });
  }
}
