// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, DoCheck, Input, ChangeDetectorRef, OnDestroy, Inject } from '@angular/core';
import { LocalizeParser, LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import {
  AppService,
  LocalesMenuModel,
  ItemCategory,
  UtilsService,
  LocalesModel,
  Deposit,
  Withdrawal
} from '../../../core';
import { UserModel } from '../../../core/models/user.model';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { MatSidenav } from '@angular/material/sidenav';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from '../../../core/services/portal.service';
import { MEDIA_BREAK_POINT } from '../../../core/models/config.model';
import { MatDialog } from '@angular/material/dialog';
import { SharedLoginComponent } from '../login/login.component';
import { fadeIn } from '../../animations/fade.animation';
import { SharedRegistrationComponent } from '../registration/registration.component';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-shared-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  animations: [fadeIn]
})
export class SharedHeaderComponent implements OnInit, DoCheck, OnDestroy {
  @Input() mainMenuDrawer: MatSidenav;
  @Input() accountDrawer: MatSidenav;

  private _mobileQueryListener: () => void;
  private _routerEvents$: Subscription;

  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;

  menu: LocalesMenuModel = {};
  locales: string[] = [];
  categoriesGames: ItemCategory[] = [];
  currentUser: UserModel;
  deposits: Deposit[] = [];
  withdrawals: Withdrawal[] = [];

  constructor(
    @Inject(DOCUMENT) private document: any,
    private appService: AppService,
    public _dialog: MatDialog,
    public localizeParser: LocalizeParser,
    private localizeRouterService: LocalizeRouterService,
    private utilsService: UtilsService,
    private authenticationService: AuthenticationService,
    private authService: AuthenticationService,
    private _portalService: PortalService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._portalService.isMobile;
    this.isTablet = this._portalService.isTablet;
    this.isDesktop = this._portalService.isDesktop;

    this.locales = localizeParser.locales;
    this.appService.categoriesGames$.subscribe((categoriesGames) => {
      this.categoriesGames = categoriesGames;
    });
    this.appService.locales$.subscribe((data: LocalesModel) => {
      this.menu = data.menu;
    });
  }

  ngOnInit() {
    this.initData();
    this._routerEvents$ = this.localizeRouterService.routerEvents.subscribe(() => {
      this.initData();
    });
    if (this.authService.isAuthenticated()) {
      this.currentUser = this.authService.currentUser();
      this.authenticationService.refresh().subscribe(user => {
        this.currentUser = user;
      });
    }
    // Fixes IOS bug regarding viewHeight
    const vh = window.innerHeight * 0.01;
    this.document.documentElement.style.setProperty('--vh', `${vh}px`);
    window.addEventListener('resize', () => {
      // tslint:disable-next-line:no-shadowed-variable
      const vh = window.innerHeight * 0.01;
      this.document.documentElement.style.setProperty('--vh', `${vh}px`);
    });
  }

  openLoginDialog(): void {
    this._dialog.open(SharedLoginComponent, {
      height: 'calc(var(--vh, 1vh) * 100)',
      width: '100vw',
      maxWidth: '100vw',
      maxHeight: 'calc(var(--vh, 1vh) * 100)',
      panelClass: 'fullscreen-dialog'
    }).afterClosed();
  }

  openRegistrationDialog(): void {
    this._dialog.open(SharedRegistrationComponent, {
      height: 'calc(var(--vh, 1vh) * 100)',
      width: '100vw',
      maxWidth: '100vw',
      maxHeight: 'calc(var(--vh, 1vh) * 100)',
      panelClass: 'fullscreen-dialog'
    }).afterClosed();
  }

  ngDoCheck() {
    if (this.authService.isAuthenticated()) {
      this.currentUser = this.authService.currentUser();
    }
  }

  initData() {
    if (this.authService.isAuthenticated()) {
      this.appService.getDepositMethods().subscribe((data) => {
        this.deposits =  data.map(item => new Deposit(item));
        localStorage.setItem('deposits', JSON.stringify(this.deposits));
      });

      this.appService.getWithdrawalMethods().subscribe((data) => {
        this.withdrawals =  data.map(item => new Withdrawal(item));
        localStorage.setItem('withdrawals', JSON.stringify(this.withdrawals));
      });
    }

    this.appService.getLocales();
    this.appService.getLocSupport();
    this.appService.getCategoriesGames();
  }

  getItemMenu(id, defTranslate) {
    return this.menu[id] ? this.menu[id] : defTranslate;
  }

  onClickMenuItem(event, id) {
    event.stopPropagation();
    event.preventDefault();

    this.utilsService.scrollTo(`#${id}`, -80);
  }

  ngOnDestroy(): void {
    this._routerEvents$.unsubscribe();
  }
}
