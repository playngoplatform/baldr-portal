// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shared-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class SharedTermsConditionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
