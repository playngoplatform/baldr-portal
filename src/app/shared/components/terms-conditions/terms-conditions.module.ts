// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedTermsConditionsComponent } from './terms-conditions.component';
import { TermsConditionsRoutingModule } from './terms-conditions.routing.module';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SharedTermsConditionsComponent],
  exports: [SharedTermsConditionsComponent],
  imports: [
    TermsConditionsRoutingModule,
    CommonModule,
    UiSwitchModule.forRoot({
      size: 'small',
    }),
    ReactiveFormsModule
  ]
})
export class SharedTermsConditionsModule { }
