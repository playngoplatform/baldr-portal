// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { SharedNotFoundComponent } from './not-found.component';
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiService, NotFoundService } from '../../../core/services';

describe('SharedNotFoundComponent', () => {
  let component: SharedNotFoundComponent;
  let fixture: ComponentFixture<SharedNotFoundComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      declarations: [ SharedNotFoundComponent ],
      providers: [
        NotFoundService,
        ApiService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
