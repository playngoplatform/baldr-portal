// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundRoutingModule } from './not-found-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

// Components
import { SharedNotFoundComponent } from './not-found.component';


@NgModule({
  declarations: [
    SharedNotFoundComponent
  ],
  imports: [
    CommonModule,
    NotFoundRoutingModule,
    TranslateModule,
    SharedModule
  ]
})
export class SharedNotFoundModule { }
