// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';
import { NotFoundService, NotFoundModel } from '../../../core';

@Component({
  selector: 'app-shared-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class SharedNotFoundComponent implements OnInit {
  data: NotFoundModel = {};
  constructor(
    private notFoundService: NotFoundService
  ) { }

  ngOnInit() {
    this.getDataPage();
  }

  getDataPage() {
    this.notFoundService.get()
      .subscribe(data => {
        this.data = data;
      });
  }
}
