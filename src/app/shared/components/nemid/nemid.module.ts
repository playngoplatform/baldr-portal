// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NemidService } from './services/nemid.service';
import { SharedNemidLoginComponent } from './components/nemid-login/nemid-login.component';

@NgModule({
  declarations: [
    SharedNemidLoginComponent
  ],
  exports: [
    SharedNemidLoginComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    NemidService
  ]
})
export class SharedNemidModule { }
