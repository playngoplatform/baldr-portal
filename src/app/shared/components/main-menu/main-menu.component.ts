// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shared-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class SharedMainMenuComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

}
