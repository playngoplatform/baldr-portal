// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AppService, AuthenticationService, PlayerService } from '../../../core/services';
import { ProfileModel, UserModel } from '../../../core/models/user.model';
import { Country, Deposit, Withdrawal, PlayerProtectionLimitsResponse } from '../../../core/models';
import { Observable, ReplaySubject, Subscription } from 'rxjs';
import { AccountViews } from './models/account-views';
import { MatSidenav } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-shared-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class SharedAccountComponent implements OnInit, OnDestroy {
  private _playerLimits$: ReplaySubject<PlayerProtectionLimitsResponse> = new ReplaySubject(1);
  private _currentPlayer$: Subscription;
  @Input() accountDrawer: MatSidenav;
  currentPlayer: UserModel;
  profile: ProfileModel;
  locales: string[];
  countries: Country[] = [];
  login: string;
  deposits: Deposit[] = [];
  withdrawals: Withdrawal[] = [];
  showMenu: boolean = false;
  showContent: boolean = true;
  playerLimits: PlayerProtectionLimitsResponse;
  playerLimits$: Observable<PlayerProtectionLimitsResponse>;
  activeView: AccountViews = null;
  accountViews: typeof AccountViews = AccountViews;

  accountHome: boolean = true;
  bonusOffersView: boolean = false;
  transactionsView: boolean = false;
  gameHistoryView: boolean = false;
  selfExcludeView: boolean = false;
  playerLimitsView: boolean = false;
  myBetsView: boolean = false;
  playerProfileView: boolean = false;
  depositView: boolean = false;
  withdrawalView: boolean = false;


  constructor(
    private _authService: AuthenticationService,
    private _appService: AppService,
    private _playerService: PlayerService,
    public dialog: MatDialog,
    authenticationService: AuthenticationService
  ) {
    this.currentPlayer = this._authService.currentUser();
    this._currentPlayer$ = authenticationService.currentPlayer$.subscribe(res => {
      this.currentPlayer = res;
    });
    this._authService.isAuthenticated$.subscribe((() => {
        this.getDataPage();
    }));
    this.profile = new ProfileModel();
    this.playerLimits$ = this._playerLimits$.asObservable();
  }

  ngOnInit() {
    this.navigateBackToAccountHome();
    if (this._authService.isAuthenticated()) {
      this.getDataPage();
    }
    this.deposits = JSON.parse(localStorage.getItem('deposits')) || [];
  }

  get navigateBack() {
    return this.navigateBackToAccountHome.bind(this);
  }

  hideAllViews(): void {
    this.bonusOffersView = false;
    this.transactionsView = false;
    this.gameHistoryView = false;
    this.selfExcludeView = false;
    this.playerLimitsView = false;
    this.myBetsView = false;
    this.playerProfileView = false;
    this.depositView = false;
    this.withdrawalView = false;
  }

  navigateBackToAccountHome(): void {
    this.accountHome = true;
    this.hideAllViews();
  }

  showPlayerProfile(): void {
    this.accountHome = false;
    this.playerProfileView = true;
  }

  showMyBets(): void {
    this.accountHome = false;
    this.myBetsView = true;
  }

  showDeposit(): void {
    this.accountHome = false;
    this.depositView = true;
  }

  showWithdrawal(): void {
    this.accountHome = false;
    this.withdrawalView = true;
  }

  showBonusOffers(): void {
    this.accountHome = false;
    this.bonusOffersView = true;
  }

  showTransactions(): void {
    this.accountHome = false;
    this.transactionsView = true;
  }

  showGameHistory(): void {
    this.accountHome = false;
    this.gameHistoryView = true;
  }

  showSelfExclude(): void {
    this.accountHome = false;
    this.selfExcludeView = true;
  }

  showPlayerLimits(): void {
    this.accountHome = false;
    this.playerLimitsView = true;
  }

  showMobileMenu(): void {
    this.showMenu = true;
    this.showContent = false;
  }

  navigateProfile(activeView: AccountViews = null): void {
    this.activeView = activeView;
    this.showMenu = false;
    this.showContent = true;
  }

  navigateProtectionLimits(activeView: AccountViews = null): void {
    this.activeView = activeView;
    this.showMenu = false;
    this.showContent = true;
    this._playerLimits$.next(this.playerLimits);
  }

  getDataPage(): void {
    this._playerService.getProfile().subscribe((data) => {
      this.profile = data.userInfo;
      this.locales = data.locales;
      this.countries = data.countries;
      this.login = data.userInfo.login;
    });

    this._appService.getDepositMethods().subscribe((data) => {
      this.deposits =  data.map(item => new Deposit(item));
      localStorage.setItem('deposits', JSON.stringify(this.deposits));
    });

    this._appService.getWithdrawalMethods().subscribe((data) => {
      this.withdrawals =  data.map(item => new Withdrawal(item));
      localStorage.setItem('withdrawals', JSON.stringify(this.withdrawals));
    });

    this.getPlayerLimits();
  }

  getPlayerLimits(): void {
    this._playerService.getProtectionLimits().subscribe(res => {
      this.playerLimits = res;
      this._playerLimits$.next(res);
    });
  }

  logout(): void {
    this._authService.logout();
  }

  ngOnDestroy() {
    this._currentPlayer$.unsubscribe();
  }
}
