// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, Inject, forwardRef, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PlayerProtectionLimits, PlayerService } from '../../../../core';
import { SNACKBAR_DURATION } from '../../../../core/models/config.model';
import { SharedAccountComponent } from '../account.component';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-shared-account-deposit-limits',
  templateUrl: './deposit-limits.component.html',
  styleUrls: ['./deposit-limits.component.scss']
})
export class SharedAccountDepositLimitsComponent implements OnInit, OnDestroy {
  private _playerLimits$: Subscription;
  depositLimits: FormGroup = new FormGroup({
    single: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    day: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    week: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    month: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)])
  });
  playerLimits: PlayerProtectionLimits;
  pendingPlayerLimits: PlayerProtectionLimits;
  currency: string;
  constructor(
    @Inject(forwardRef(() => SharedAccountComponent))
    public accountComponent: SharedAccountComponent,
    private _playerService: PlayerService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this._playerLimits$ = this.accountComponent.playerLimits$.subscribe(res => {
      if (res) {
        this.playerLimits = res.currentLimits;
        this.pendingPlayerLimits = res.pendingLimits;
      }
    });
    this.currency = this.accountComponent.currentPlayer ? this.accountComponent.currentPlayer.currency : null;
  }

  numericOnly(event): boolean { // restrict e,+,-,E characters in  input type number
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode === 101 || charCode === 69 || charCode === 45 || charCode === 43) {
      return false;
    }
    return true;
  }

  submitDepositLimits(): void {
    if (this.pendingPlayerLimits) {
      this.pendingPlayerLimits.depositLimits.day = this.depositLimits.value.day
        ? parseInt(this.depositLimits.value.day, 10)
        : this.pendingPlayerLimits.depositLimits.day;
      this.pendingPlayerLimits.depositLimits.week = this.depositLimits.value.week
        ? parseInt(this.depositLimits.value.week, 10)
        : this.pendingPlayerLimits.depositLimits.week;
      this.pendingPlayerLimits.depositLimits.month = this.depositLimits.value.month
        ? parseInt(this.depositLimits.value.month, 10)
        : this.pendingPlayerLimits.depositLimits.month;
      this.pendingPlayerLimits.depositLimits.single = this.depositLimits.value.single
        ? parseInt(this.depositLimits.value.single, 10)
        : this.pendingPlayerLimits.depositLimits.single;
      this._playerService.updateProtectionLimits(this.pendingPlayerLimits).subscribe(() => {
        this.depositLimits.reset();
        this._snackBar.open(
          this._translate.instant('Profile.Deposit_limits_view.Deposit_limits'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }, () => {
        this._snackBar.open(
          this._translate.instant('Profile.Deposit_limits_view.Deposit_limits_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }

  ngOnDestroy(): void {
    this._playerLimits$.unsubscribe();
  }
}
