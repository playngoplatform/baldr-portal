// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';
import { BonusOfferService } from '../../../../core/services/bonus-offer.service';
import { BonusOffer, BonusOfferParams } from '../../../../core/models/bonus-offer.model';
import { AuthenticationService } from '../../../../core';
import { TranslateService } from '@ngx-translate/core';
import { fadeIn } from 'src/app/shared/animations/fade.animation';
import { SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SharedModalConfirmationComponent } from 'src/app/shared/modal/confirmation/confirmation.component';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-shared-account-bonus-offers',
  templateUrl: './bonus-offers.component.html',
  styleUrls: ['./bonus-offers.component.scss'],
  animations: [fadeIn]
})
export class SharedAccountBonusOffersComponent implements OnInit {
  bonusOffers: BonusOffer[] = [];
  canVoidAll: boolean = false;
  loadingOffers: boolean;

  constructor(
    private _bonusOfferService: BonusOfferService,
    private _authenticationService: AuthenticationService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar,
    private _dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.loadingOffers = true;
    this.getAllBonusOffers();
  }

  getAllBonusOffers(): void {
    const params = new BonusOfferParams();
    this.canVoidAll = false;
    this._bonusOfferService.getAllBonusOffers(params).subscribe(res => {
      this.bonusOffers = res.bonusOffers;
      res.bonusOffers.forEach(offer => {
        this.canVoidAll = offer.state === 'Wagering' || offer.state === 'Active' ? true : this.canVoidAll;
      });
      this.loadingOffers = false;
    }, () => {
      this._snackBar.open(
        this._translate.instant('Profile.Bonus_offer_view.Failed_fetching_bonus_offers'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
      this.loadingOffers = false;
    });
  }

  claimBonusOffer(bonusOffer: BonusOffer): void {
    this._bonusOfferService.claimBonusOffer(bonusOffer.id).subscribe(() => {
      this._snackBar.open(
        this._translate.instant('Profile.Bonus_offer_view.Bonus_offer_claimed'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
      this._refreshPlayerData();
    }, () => {
      this._snackBar.open(
        this._translate.instant('Profile.Bonus_offer_view.Failed_claiming_bonus_offer'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
    });
  }

  voidBonusOffer(bonusOffer: BonusOffer): void {
    this._bonusOfferService.voidBonusOffer(bonusOffer.id).subscribe(() => {
      this._snackBar.open(
        this._translate.instant('Profile.Bonus_offer_view.Bonus_offer_voided'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
      this._refreshPlayerData();
    }, () => {
      this._snackBar.open(
        this._translate.instant('Profile.Bonus_offer_view.Failed_to_void_bonus_offer'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
    });
  }

  submitVoidBonus(): void {
    this.confirmVoidAllBonusOffers().subscribe(res => {
      if (res) {
        this.voidAllBonusOffers();
      }
    });
  }

  confirmVoidAllBonusOffers(): Observable<MatDialog>  {
    return this._dialog
    .open(SharedModalConfirmationComponent, {
      disableClose: true,
      data: {
        title: this._translate.instant('Profile.Bonus_offer_view.Void_all_bonus_offers'),
        message: this._translate.instant('Profile.Bonus_offer_view.Confirm_void_all_text'),
        buttonActionName: this._translate.instant('Profile.Bonus_offer_view.Confirm')
       }
    }).afterClosed();
  }

  voidAllBonusOffers(): void {
    this._bonusOfferService.voidAllBonusOffers().subscribe(() => {
      this._snackBar.open(
        this._translate.instant('Profile.Bonus_offer_view.Bonus_offers_voided'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
      this._refreshPlayerData();
    }, () => {
      this._snackBar.open(
        this._translate.instant('Profile.Bonus_offer_view.Failed_to_void_bonus_offers'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
    });
  }

  private _refreshPlayerData(): void {
    this.getAllBonusOffers();
    this._authenticationService.refresh().subscribe();
  }
}
