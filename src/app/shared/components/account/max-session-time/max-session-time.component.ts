// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy, Inject, forwardRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { PlayerProtectionLimits, PlayerService } from '../../../../core';
import { SharedAccountComponent } from '../account.component';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SNACKBAR_DURATION } from '../../../../core/models/config.model';

@Component({
  selector: 'app-shared-account-max-session-time',
  templateUrl: './max-session-time.component.html',
  styleUrls: ['./max-session-time.component.scss']
})
export class SharedAccountMaxSessionTimeComponent implements OnInit, OnDestroy {
  private _playerLimits$: Subscription;
  maxSessionTimeForm: FormGroup = new FormGroup({
    maxSessionTime: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)])
  });
  playerLimits: PlayerProtectionLimits;
  pendingPlayerLimits: PlayerProtectionLimits;

  constructor(
    @Inject(forwardRef(() => SharedAccountComponent))
    public accountComponent: SharedAccountComponent,
    private _playerService: PlayerService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this._playerLimits$ = this.accountComponent.playerLimits$.subscribe(res => {
      if (res) {
        this.playerLimits = res.currentLimits;
        this.pendingPlayerLimits = res.pendingLimits;
      }
    });
  }

  numericOnly(event): boolean { // restrict e,+,-,E characters in  input type number
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode === 101 || charCode === 69 || charCode === 45 || charCode === 43) {
      return false;
    }
    return true;
  }

  submitMaxSessionTime(): void {
    if (this.pendingPlayerLimits) {
      this.pendingPlayerLimits.maxSessionTime = this.maxSessionTimeForm.value.maxSessionTime
        ? parseInt(this.maxSessionTimeForm.value.maxSessionTime, 10)
        : this.pendingPlayerLimits.maxSessionTime;
      this._playerService.updateProtectionLimits(this.pendingPlayerLimits).subscribe(() => {
        this.maxSessionTimeForm.reset();
        this._snackBar.open(
          this._translate.instant('Profile.Session_limit_view.Session_limit'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }, () => {
        this._snackBar.open(
          this._translate.instant('Profile.Session_limit_view.Session_limit_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }

  ngOnDestroy(): void {
    this._playerLimits$.unsubscribe();
  }
}
