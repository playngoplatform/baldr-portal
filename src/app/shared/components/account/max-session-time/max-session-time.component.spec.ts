// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedAccountMaxSessionTimeComponent } from './max-session-time.component';

describe('SharedAccountMaxSessionTimeComponent', () => {
  let component: SharedAccountMaxSessionTimeComponent;
  let fixture: ComponentFixture<SharedAccountMaxSessionTimeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedAccountMaxSessionTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedAccountMaxSessionTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
