// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy, Inject, forwardRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlayerProtectionLimits, PlayerService } from '../../../../core';
import { SharedAccountComponent } from '../account.component';
import { SNACKBAR_DURATION } from '../../../../core/models/config.model';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-shared-account-transfer-limits',
  templateUrl: './transfer-limits.component.html',
  styleUrls: ['./transfer-limits.component.scss']
})
export class SharedAccountTransferLimitsComponent implements OnInit, OnDestroy {
  private _playerLimits$: Subscription;
  transferLimits: FormGroup = new FormGroup({
    single: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    day: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    week: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    month: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)])
  });
  playerLimits: PlayerProtectionLimits;
  pendingPlayerLimits: PlayerProtectionLimits;
  currency: string;

  constructor(
    @Inject(forwardRef(() => SharedAccountComponent))
    public accountComponent: SharedAccountComponent,
    private _playerService: PlayerService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this._playerLimits$ = this.accountComponent.playerLimits$.subscribe(res => {
      if (res) {
        this.playerLimits = res.currentLimits;
        this.pendingPlayerLimits = res.pendingLimits;
      }
    });
    this.currency = this.accountComponent.currentPlayer ? this.accountComponent.currentPlayer.currency : null;
  }

  numericOnly(event): boolean { // restrict e,+,-,E characters in  input type number
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode === 101 || charCode === 69 || charCode === 45 || charCode === 43) {
      return false;
    }
    return true;
  }

  submitTransferLimits(): void {
    if (this.pendingPlayerLimits) {
      this.pendingPlayerLimits.transferLimits.day = this.transferLimits.value.day
        ? parseInt(this.transferLimits.value.day, 10)
        : this.pendingPlayerLimits.transferLimits.day;
      this.pendingPlayerLimits.transferLimits.week = this.transferLimits.value.week
        ? parseInt(this.transferLimits.value.week, 10)
        : this.pendingPlayerLimits.transferLimits.week;
      this.pendingPlayerLimits.transferLimits.month = this.transferLimits.value.month
        ? parseInt(this.transferLimits.value.month, 10)
        : this.pendingPlayerLimits.transferLimits.month;
      this.pendingPlayerLimits.transferLimits.single = this.transferLimits.value.single
        ? parseInt(this.transferLimits.value.single, 10)
        : this.pendingPlayerLimits.transferLimits.single;
      this._playerService.updateProtectionLimits(this.pendingPlayerLimits).subscribe(() => {
        this.transferLimits.reset();
        this._snackBar.open(
          this._translate.instant('Profile.Transfer_limits_view.Transfer_limits'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }, () => {
        this._snackBar.open(
          this._translate.instant('Profile.Transfer_limits_view.Transfer_limits_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }

  ngOnDestroy(): void {
    this._playerLimits$.unsubscribe();
  }
}
