// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';
import { PlayerService, AuthenticationService } from '../../../../core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { SELF_EXCLUDE_PERIODS, ISelfExcludePeriod } from '../../../../core/models';
import { TranslateService } from '@ngx-translate/core';
import { SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-shared-account-self-exclude',
  templateUrl: './self-exclude.component.html',
  styleUrls: ['./self-exclude.component.scss']
})
export class SharedAccountSelfExcludeComponent implements OnInit {
  activeButton: boolean = false;
  showView: boolean;
  periods: ISelfExcludePeriod[] = SELF_EXCLUDE_PERIODS;
  selfExcludeForm: FormGroup = new FormGroup({
    selectedPeriod: new FormControl(this.periods[0], Validators.required)
  });

  constructor(
    private _playerService: PlayerService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar,
    private _authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.showView = true;
  }

  toggleAcceptLock(value: boolean): void {
    this.activeButton = value;
  }

  submitSelfExclude(): void {
    const selectedPeriod = this.selfExcludeForm.value.selectedPeriod;
    if (selectedPeriod) {
      const date = moment.utc().add(selectedPeriod.amount, selectedPeriod.type);
      this._playerService.setSelfExclusion(date).subscribe(() => {
        this._snackBar.open(
          this._translate.instant('Profile.Self_exclude_view.Self_exclude_success'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
        this._authenticationService.logout();
      }, () => {
        this._snackBar.open(
          this._translate.instant('Profile.Self_exclude_view.Self_exclude_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }
}
