// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedAccountComponent } from './account.component';
import { SharedAccountGameHistoryComponent } from './game-history/game-history.component';
import { SharedAccountOrdersComponent } from './orders/orders.component';
import { ApiService, AppService, AuthenticationService, UtilsService } from '../../../core/services';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { LocalizeRouterMockService } from '../../../../../tests/localize-router-mock-service';
import { SharedDepositComponent } from './deposit/deposit.component';

describe('SharedAccountComponent', () => {
  let component: SharedAccountComponent;
  let fixture: ComponentFixture<SharedAccountComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [SharedAccountComponent,
        SharedAccountGameHistoryComponent,
        SharedAccountOrdersComponent,
        SharedDepositComponent
      ],
      providers: [
        ApiService,
        AppService,
        AuthenticationService,
        {provide: LocalizeRouterService, useValue: LocalizeRouterMockService},
        UtilsService
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
