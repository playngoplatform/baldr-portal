// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { AfterViewInit, Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { GameHistory } from '../../../../core/models/game_history.model';
import { GameService } from 'src/app/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { map, pairwise, filter, throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-shared-account-game-history',
  templateUrl: './game-history.component.html',
  styleUrls: ['./game-history.component.scss']
})
export class SharedAccountGameHistoryComponent implements OnInit, AfterViewInit {
  @ViewChild('scroller') scroller: CdkVirtualScrollViewport;

  history: GameHistory[] = [];
  gameHistoryLimit: number = 50;
  gameHistorySkip: number = null;
  loading = false;
  everythingFetch: boolean = false;

  constructor(
    private _gameService: GameService,
    private ngZone: NgZone
  ) {
    this.history = [];
   }

  ngOnInit() {
    this.getGameHistory();
  }

  ngAfterViewInit(): void {
    this.scroller.elementScrolled().pipe(
      map(() => this.scroller.measureScrollOffset('bottom')),
      pairwise(),
      filter(([y1, y2]) => (y2 < y1 && y2 < 140)),
      throttleTime(200)
    ).subscribe(() => {
      this.ngZone.run(() => {
        if (!this.everythingFetch) {
          this.getGameHistory();
        }
      });
    }
    );
  }

  getGameHistory(): void {
    this.loading = true;
    this.gameHistorySkip = this.gameHistorySkip === null ? 0 : ++this.gameHistorySkip;
    const skip = this.gameHistorySkip * this.gameHistoryLimit;
    this._gameService.getGameHistory(skip, this.gameHistoryLimit).subscribe(res => {
      this.history = this.history.concat(res);
      this.loading = false;
      if (res.length === 0) {
        this.everythingFetch = true;
      }
    }, () => {
      this.loading = false;
    });
  }
}
