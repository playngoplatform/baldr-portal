// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedAccountComponent } from './account.component';
import { SharedAccountRoutingModule } from './account-routing.module';
import { SharedAccountGameHistoryComponent } from './game-history/game-history.component';
import { SharedAccountOrdersComponent } from './orders/orders.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { SharedAccountSelfExcludeComponent } from './self-exclude/self-exclude.component';
import { SharedAccountDepositLimitsComponent } from './deposit-limits/deposit-limits.component';
import { SharedAccountLossLimitsComponent } from './loss-limits/loss-limits.component';
import { SharedAccountTransferLimitsComponent } from './transfer-limits/transfer-limits.component';
import { SharedAccountMaxSessionTimeComponent } from './max-session-time/max-session-time.component';
import { SharedAccountBonusOffersComponent } from './bonus-offers/bonus-offers.component';
import { TranslateModule } from '@ngx-translate/core';
import { NewPasswordModule } from 'src/app/casino/new-password/new-password.module';
import { SharedAccountMyBetsComponent } from './my-bets/my-bets.component';
import { SharedAccountDepositComponent } from './deposit/deposit.component';


@NgModule({
  declarations: [
    SharedAccountComponent,
    SharedAccountGameHistoryComponent,
    SharedAccountOrdersComponent,
    SharedAccountSelfExcludeComponent,
    SharedAccountDepositLimitsComponent,
    SharedAccountLossLimitsComponent,
    SharedAccountTransferLimitsComponent,
    SharedAccountMaxSessionTimeComponent,
    SharedAccountBonusOffersComponent,
    SharedAccountDepositComponent,
    SharedAccountMyBetsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SharedAccountRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    NewPasswordModule
  ]
})
export class SharedAccountModule { }
