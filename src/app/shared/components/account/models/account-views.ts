// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export enum AccountViews {
  GameHistory,
  Orders
}
