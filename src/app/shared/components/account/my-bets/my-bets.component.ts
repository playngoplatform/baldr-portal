// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit } from '@angular/core';

export interface BetItem {
  selections?: {
    fixtureName: string,
    scheduledAt: string,
    marketName: string,
    participant1: string,
    participant2: string,
    selectionName: string;
    price: number;
  }[];
  totalPrice: number;
  betAmount: number;
  betDate: string;
  currency: string;
  potentialWin: number;
}
@Component({
  selector: 'app-shared-account-my-bets',
  templateUrl: './my-bets.component.html',
  styleUrls: ['./my-bets.component.scss']
})

export class SharedAccountMyBetsComponent implements OnInit {
  selectedFilter: string;
  showUpcoming: boolean = true;
  showOngoing: boolean = true;
  showCompleted: boolean = true;

  upcomingBets: BetItem[] = [
    { selections: [
      {
        fixtureName: 'Tottenham - Liverpool',
        scheduledAt: 'Today, 21:00',
        marketName: 'Full-time winner',
        participant1: 'Tottenham',
        participant2: 'Liverpool',
        selectionName: 'Liverpool',
        price: 2.65,
      }],
      totalPrice: 2.65,
      betAmount: 100,
      betDate: '10 January, 13:15',
      potentialWin: 265,
      currency: 'DKK'
    },
    { selections: [
      {
        fixtureName: 'Arsenal - Manchester City',
        scheduledAt: 'Today, 21:00',
        marketName: 'Full-time winner',
        participant1: 'Arsenal',
        participant2: 'Manchester City',
        selectionName: 'Manchester City',
        price: 1.45,
      },
      {
        fixtureName: 'Southampton - Wolverhampton',
        scheduledAt: 'Today, 21:00',
        marketName: 'Full-time winner',
        participant1: 'Southampton',
        participant2: 'Wolverhampton',
        selectionName: 'Draw',
        price: 2.65,
      }],
      totalPrice: 3.84,
      betAmount: 100,
      betDate: '10 January, 13:15',
      potentialWin: 384,
      currency: 'DKK'
    }
  ];

  ongoingBets: BetItem[] = [
    { selections: [
      {
        fixtureName: 'Kalmar FF - Elfsborgs IF',
        scheduledAt: 'Today, 18:00',
        marketName: 'Full-time winner',
        participant1: 'Kalmar FF',
        participant2: 'Elfsborgs IF',
        selectionName: 'Kalmar FF',
        price: 3.05,
      }],
      totalPrice: 3.05,
      betAmount: 100,
      betDate: '10 January, 13:15',
      potentialWin: 305,
      currency: 'DKK'
    },
  ];

  constructor() { }

  ngOnInit() {
    this.selectedFilter = 'all';
  }

  onValChange(val: string): void {
    this.selectedFilter = val;

    if (this.selectedFilter === 'all') {
      this.showUpcoming = true;
      this.showOngoing = true;
      this.showCompleted = true;
    } else if (this.selectedFilter === 'upcoming') {
      this.showUpcoming = true;
      this.showOngoing = false;
      this.showCompleted = false;
    } else if (this.selectedFilter === 'ongoing') {
      this.showUpcoming = false;
      this.showOngoing = true;
      this.showCompleted = false;
    } else if (this.selectedFilter === 'completed') {
      this.showUpcoming = false;
      this.showOngoing = false;
      this.showCompleted = true;
    }
  }

}
