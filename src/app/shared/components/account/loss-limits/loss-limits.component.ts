// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, Inject, forwardRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PlayerProtectionLimits, PlayerService } from '../../../../core';
import { SNACKBAR_DURATION } from '../../../../core/models/config.model';
import { SharedAccountComponent } from '../account.component';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-shared-account-loss-limits',
  templateUrl: './loss-limits.component.html',
  styleUrls: ['./loss-limits.component.scss']
})
export class SharedAccountLossLimitsComponent implements OnInit, OnDestroy {
  private _playerLimits$: Subscription;
  lossLimits: FormGroup = new FormGroup({
    single: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    day: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    week: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    month: new FormControl(null, [Validators.pattern(/^-?(0|[1-9]\d*)?$/)])
  });
  playerLimits: PlayerProtectionLimits;
  pendingPlayerLimits: PlayerProtectionLimits;
  currency: string;

  constructor(
    @Inject(forwardRef(() => SharedAccountComponent))
    public accountComponent: SharedAccountComponent,
    private _playerService: PlayerService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this._playerLimits$ = this.accountComponent.playerLimits$.subscribe(res => {
      if (res) {
        this.playerLimits = res.currentLimits;
        this.pendingPlayerLimits = res.pendingLimits;
      }
    });
    this.currency = this.accountComponent.currentPlayer ? this.accountComponent.currentPlayer.currency : null;
  }

  numericOnly(event): boolean { // restrict e,+,-,E characters in  input type number
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode === 101 || charCode === 69 || charCode === 45 || charCode === 43) {
      return false;
    }
    return true;
  }

  submitLossLimits(): void {
    if (this.pendingPlayerLimits) {
      this.pendingPlayerLimits.lossLimits.day = this.lossLimits.value.day
        ? parseInt(this.lossLimits.value.day, 10)
        : this.pendingPlayerLimits.lossLimits.day;
      this.pendingPlayerLimits.lossLimits.week = this.lossLimits.value.week
        ? parseInt(this.lossLimits.value.week, 10)
        : this.pendingPlayerLimits.lossLimits.week;
      this.pendingPlayerLimits.lossLimits.month = this.lossLimits.value.month
        ? parseInt(this.lossLimits.value.month, 10)
        : this.pendingPlayerLimits.lossLimits.month;
      this.pendingPlayerLimits.lossLimits.single = this.lossLimits.value.single
        ? parseInt(this.lossLimits.value.single, 10)
        : this.pendingPlayerLimits.lossLimits.single;
      this._playerService.updateProtectionLimits(this.pendingPlayerLimits).subscribe(() => {
        this.lossLimits.reset();
        this._snackBar.open(
          this._translate.instant('Profile.Loss_limits_view.Loss_limits'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }, () => {
        this._snackBar.open(
          this._translate.instant('Profile.Loss_limits_view.Loss_limits_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }

  ngOnDestroy(): void {
    this._playerLimits$.unsubscribe();
  }
}
