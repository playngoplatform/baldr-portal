// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedWithdrawalComponent } from './withdrawal.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService, AppService, AuthenticationService } from '../../../../core/services';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { LocalizeRouterMockService } from '../../../../../../tests/localize-router-mock-service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SharedWithdrawalComponent', () => {
  let component: SharedWithdrawalComponent;
  let fixture: ComponentFixture<SharedWithdrawalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [
        SharedWithdrawalComponent
      ],
      providers: [
        ApiService,
        AppService,
        AuthenticationService,
        {provide: LocalizeRouterService, useValue: LocalizeRouterMockService},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedWithdrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
