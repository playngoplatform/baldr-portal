// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { SharedWithdrawalComponent } from './withdrawal.component';

const routes: Routes = [{
  path: '',
  component: SharedWithdrawalComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    LocalizeRouterModule.forChild(routes)

  ],
  exports: [RouterModule, LocalizeRouterModule]
})
export class SharedWithdrawalRoutingModule {
}
