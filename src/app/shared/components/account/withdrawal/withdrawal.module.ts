// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedWithdrawalRoutingModule } from './withdrawal-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { SharedAccountWithdrawalComponent } from './withdrawal.component';
import { SharedModule } from '../../../shared.module';

@NgModule({
  declarations: [SharedAccountWithdrawalComponent],
  imports: [
    CommonModule,
    SharedModule,
    SharedWithdrawalRoutingModule,
    ReactiveFormsModule,
    ToasterModule
  ]
})
export class SharedWithdrawalModule { }
