// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, Input, OnInit, SecurityContext } from '@angular/core';
import { UserModel, Withdrawal } from '../../../../core/models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService, AuthenticationService } from '../../../../core/services';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-account-shared-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class SharedAccountWithdrawalComponent implements OnInit {
  @Input() withdrawals: Withdrawal[];
  @Input() navigateBackToAccountHome: Function;
  currentStep: number = 1;
  withdrawalForm: FormGroup;
  currentMethod: Withdrawal;
  loading: boolean = false;
  error: string = '';
  user: UserModel;

  constructor(
    private _appService: AppService,
    private _formBuilder: FormBuilder,
    private _authService: AuthenticationService,
    private _snackBar: MatSnackBar,
    private sanitizeHtml: DomSanitizer,
    private _translate: TranslateService,
  ) {
    this.withdrawalForm = this._formBuilder.group({
      amount: [0, Validators.required],
      bonus_code: [localStorage.getItem('bonus')]
    });
    this._authService.isAuthenticated$.subscribe((() => {
        this.getDataPage();
    }));
  }

  get f() { return this.withdrawalForm.controls; }

  getDataPage(): void {
    this._appService.getWithdrawalMethods().subscribe((data) => {
      this.withdrawals =  data.map(item => new Withdrawal(item));
      localStorage.setItem('withdrawals', JSON.stringify(this.withdrawals));
    });
  }

  ngOnInit(): void {
    this.currentStep = 1;
    this.withdrawals = JSON.parse(localStorage.getItem('withdrawals'));
    this.withdrawals = this.withdrawals.map(withdrawal => {​​
      this.sanitizeHtml.sanitize(SecurityContext.HTML, withdrawal.description);
      return withdrawal;
    }​​);
  }

  handleStep(step: number): void {
    this.currentStep = step;
  }

  selectMethod(method: Withdrawal): void {
    this.currentMethod = method;
    this.user = this._authService.currentUser();
    this.withdrawalForm.controls.amount.setValue(this.currentMethod.min);
    this.currentStep += 1;
  }

  withdrawal(): void {
    this.loading = true;
    this._appService.postWithdrawal(
      this.currentMethod.id,
      this.f.amount.value,
      this.f.bonus_code.value
    ).subscribe(() => {
      this._snackBar.open(
        this._translate.instant('Withdrawal.Withdrawal_success'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
      this._authService.refresh().subscribe(() => {
        this.navigateBackToAccountHome();
      }, () => {
        this.navigateBackToAccountHome();
      });
    }, () => {
      this.loading = false;
      this._snackBar.open(
        this._translate.instant('Withdrawal.Withdrawal_failed'),
        this._translate.instant('Ok'),
        { duration: SNACKBAR_DURATION }
      );
      this.navigateBackToAccountHome();
      return;
    });
  }
}
