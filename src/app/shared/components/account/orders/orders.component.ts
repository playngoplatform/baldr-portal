// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { AfterViewInit, Component, NgZone, OnInit, ViewChild } from '@angular/core';

import { Order, OrderService } from '../../../../core';
import { map, pairwise, filter, throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-shared-account-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class SharedAccountOrdersComponent implements OnInit, AfterViewInit {
  @ViewChild('scroller') scroller: CdkVirtualScrollViewport;

  orders: Order[] = [];
  orderSkip: number = null;
  orderLimit: number = 50;
  loading = false;
  everythingFetch: boolean = false;

  constructor(
    private _orderService: OrderService,
    private ngZone: NgZone
  ) {
    this.orders = [];
   }

  ngOnInit() {
    this.getOrders();
   }

  ngAfterViewInit(): void {
    this.scroller.elementScrolled().pipe(
      map(() => this.scroller.measureScrollOffset('bottom')),
      pairwise(),
      filter(([y1, y2]) => (y2 < y1 && y2 < 140)),
      throttleTime(400)
    ).subscribe(() => {
      this.ngZone.run(() => {
        if (!this.everythingFetch) {
          this.getOrders();
        }
      });
    }
    );
  }

  getOrders(): void {
    this.loading = true;
    this.orderSkip = this.orderSkip === null ? 0 : ++this.orderSkip;
    const skip = this.orderSkip * this.orderLimit;
    this._orderService.getOrders(skip, this.orderLimit).subscribe(res => {
      this.orders = this.orders.concat(res);
      this.loading = false;
      if (res.length === 0) {
        this.everythingFetch = true;
      }
    }, () => {
      this.loading = false;
    });
  }
}
