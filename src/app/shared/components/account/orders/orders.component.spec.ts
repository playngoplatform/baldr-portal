// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedAccountOrdersComponent } from './orders.component';
import { ApiService, AppService } from '../../../../core/services';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SharedAccountOrdersComponent', () => {
  let component: SharedAccountOrdersComponent;
  let fixture: ComponentFixture<SharedAccountOrdersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedAccountOrdersComponent ],
      providers: [AppService, ApiService],
      imports: [HttpClientModule, HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedAccountOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
