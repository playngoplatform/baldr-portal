// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared.module';
import { SharedDepositRoutingModule } from './deposit-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { SharedAccountDepositSuccessComponent } from './deposit-success/deposit-success.component';
import { PaymentsNetsService } from '../../../../core/services/payments/nets.service';
import { TranslateModule } from '@ngx-translate/core';
import { SharedAccountDepositComponent } from './deposit.component';


@NgModule({
  declarations: [
    SharedAccountDepositSuccessComponent
  ],
  exports: [
    SharedAccountDepositComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    SharedDepositRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    ToasterModule
  ],
  providers: [
    PaymentsNetsService
  ]
})
export class SharedDepositModule { }
