// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, TestBed } from '@angular/core/testing';

import { SharedDepositSuccessComponent } from './deposit-success.component';
import {RouterTestingModule} from '@angular/router/testing';
import {ToasterModule, ToasterService} from 'angular2-toaster';

describe('SharedDepositSuccessComponent', () => {
  // let component: SharedDepositSuccessComponent;
  // let fixture: ComponentFixture<SharedDepositSuccessComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, ToasterModule],
      declarations: [ SharedDepositSuccessComponent ],
      providers: [ToasterService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    // fixture = TestBed.createComponent(SharedDepositSuccessComponent);
    // component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    // expect(component).toBeTruthy();
  });
});
