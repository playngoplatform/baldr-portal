// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaymentsNetsService } from 'src/app/core/services/payments/nets.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MEDIA_BREAK_POINT, SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { MediaMatcher } from '@angular/cdk/layout';
import { PortalService } from 'src/app/core/services/portal.service';

@Component({
  selector: 'app-shared-deposit-success',
  templateUrl: './deposit-success.component.html',
  styleUrls: ['./deposit-success.component.scss']
})
export class SharedAccountDepositSuccessComponent implements OnInit, OnDestroy {
  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;
  showSuccessText: boolean = false;
  showCancelText: boolean = false;

  constructor(
    private _snackBar: MatSnackBar,
    private _route: ActivatedRoute,
    private _translate: TranslateService,
    private _PortalService: PortalService,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private _paymentsNetsService: PaymentsNetsService
  ) {
    this.mobileQuery = media.matchMedia(`(max-width: ${MEDIA_BREAK_POINT}px)`);
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.isMobile = this._PortalService.isMobile;
    this.isTablet = this._PortalService.isTablet;
    this.isDesktop = this._PortalService.isDesktop;
  }

  ngOnInit(): void {
    if (this._route.snapshot.queryParams.transactionId && this._route.snapshot.queryParams.responseCode) {
      this._paymentsNetsService.notify(
        parseInt(this._route.snapshot.queryParams.transactionId, 10),
        this._route.snapshot.queryParams.responseCode
      ).subscribe(() => {
        this.showSuccessText = true;
        this._snackBar.open(
          this._translate.instant('Deposit.Deposit_success'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }, () => {
        this.showCancelText = true;
        this._snackBar.open(
          this._translate.instant('Deposit.Deposit_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      });
    }
  }

  closeTab(): void {
    window.top.close();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }
}
