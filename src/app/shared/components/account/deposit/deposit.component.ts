// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, Input, OnInit, SecurityContext } from '@angular/core';
import { Deposit, UserModel } from '../../../../core/models';
import { AppService, AuthenticationService } from '../../../../core/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-account-shared-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss']
})

export class SharedAccountDepositComponent implements OnInit {
  @Input() deposits: Deposit[];
  @Input() navigateBackToAccountHome: Function;

  currentStep: number = 1;
  currentMethod: Deposit;
  depositForm: FormGroup;
  user: UserModel;
  loading: boolean = false;
  error: string = '';
  code: string = '';

  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    activatedRoute: ActivatedRoute,
    private sanitizeHtml: DomSanitizer,
    private authService: AuthenticationService
  ) {
    this.depositForm = this.formBuilder.group({
      amount: [0, Validators.required],
      bonusCode: [localStorage.getItem('bonus')]
    });
    this.code = activatedRoute.snapshot.queryParams['code'];
    localStorage.removeItem('bonus');
    this.authService.isAuthenticated$.subscribe((() => {
      this.getDataPage();
    }));
  }

  ngOnInit(): void {
    this.currentStep = 1;
    this.deposits = JSON.parse(localStorage.getItem('deposits'));
    this.deposits = this.deposits.map(deposit => {​​
      this.sanitizeHtml.sanitize(SecurityContext.HTML, deposit.description);
      return deposit;
    }​​);
  }

  get f() { return this.depositForm.controls; }

  getDataPage(): void {
    this.appService.getDepositMethods().subscribe((data) => {
      this.deposits = data.map(item => new Deposit(item));
      localStorage.setItem('deposits', JSON.stringify(this.deposits));
    });
  }

  handleStep(step: number): void {
    this.currentStep = step;
  }

  selectMethod(method: Deposit): void {
    this.currentMethod = method;
    this.depositForm.controls.amount.setValidators([Validators.min(this.currentMethod.min), Validators.max(this.currentMethod.max)]);
    this.user = this.authService.currentUser();
    this.depositForm.controls.amount.setValue(this.currentMethod.min);
    this.depositForm.controls.bonusCode.setValue(this.code);
    this.currentStep += 1;
  }

  deposit(): void {
    this.loading = true;
    this.appService.postDeposit(
      this.currentMethod.id,
      this.f.amount.value,
      this.f.bonusCode.value
    ).subscribe(url => {
      window.open(url, '_blank');
    }, (response) => {
      this.loading = false;
      this.error = response.error.error;
      return;
    });
  }

  closeDeposit(): void {
    this.navigateBackToAccountHome();
  }
}
