// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedAccountDepositComponent } from './deposit.component';
import { ApiService, AppService, AuthenticationService } from '../../../../core/services';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { LocalizeRouterMockService } from '../../../../../../tests/localize-router-mock-service';

describe('SharedDepositComponent', () => {
  let component: SharedAccountDepositComponent;
  let fixture: ComponentFixture<SharedAccountDepositComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule,
        ReactiveFormsModule
      ],
      declarations: [
        SharedAccountDepositComponent
      ],
      providers: [
        ApiService,
        AppService,
        AuthenticationService,
        {provide: LocalizeRouterService, useValue: LocalizeRouterMockService},
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParams: {
                code(): string {
                  return '1';
                },
              },
            },
          },
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedAccountDepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
