// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { RegistrationModel, AuthenticationService, INTENDED_GAMBLING_VALUES_DKK } from '../../../core';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { NemidService } from '../integrations/nemid/services/nemid.service';
import { TranslateService } from '@ngx-translate/core';
import { MatStepper } from '@angular/material/stepper';
import { fadeIn } from '../../animations/fade.animation';
import { SNACKBAR_DURATION } from 'src/app/core/models/config.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-shared-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less'],
  animations: [fadeIn]
})
export class SharedRegistrationComponent implements OnInit, OnDestroy {
  @ViewChild('registrationStepper') private _registrationStepper: MatStepper;
  private birthdateControl$: Subscription;
  registerProfileForm: FormGroup;
  registerLimitsForm: FormGroup;
  registerIntendedGamblingForm: FormGroup;
  submitted: boolean = false;
  loading: boolean = true;
  error: string =  '';
  showSingle: boolean = false;
  showWeekly: boolean = true;
  showDaily: boolean = false;
  showMonthly: boolean = false;
  acceptTerms: boolean = false;
  intendedGamblingValues: string[] = INTENDED_GAMBLING_VALUES_DKK;
  externalLoginIsDone: boolean = false;

  selectedLimit: string;
  showTermsAndCondition: boolean = false;
  showConfirmStep: boolean = true;

  get birthdateControl(): AbstractControl {
    return this.registerProfileForm.controls.birthdate;
  }

  constructor(
    private formBuilder: FormBuilder,
    private _authenticationService: AuthenticationService,
    private _translate: TranslateService,
    private _snackBar: MatSnackBar,
    private _nemidService: NemidService
  ) {
    this._initRegisterProfileForm();
    this._initRegisterLimitsForm();
    this._initRegisterIntendedGamblingForm();
  }

  ngOnInit() {
    this._validateBirthdateControl();
    this.selectedLimit = 'weekly';
  }

  onValChange(val: string): void {
    this.selectedLimit = val;
  }

  private _initRegisterProfileForm(): void {
    this.registerProfileForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      birthdate: ['', [Validators.required, Validators.pattern('^[0-9]{8}$')]],
      // tslint:disable-next-line:max-line-length
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      address: ['', Validators.required],
      zip: ['', Validators.required],
      city: ['', Validators.required],
      phone: ['', Validators.required],
      nationalId: ['', Validators.required],
      externalId: ['', Validators.required],
      login: ['', Validators.required],
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required],
    }, { validator: this.checkPasswords });
  }

  private _initRegisterLimitsForm(): void {
    this.registerLimitsForm = this.formBuilder.group({
      singleLimit: [null, Validators.pattern(/^(0|[1-9]\d*)?$/)],
      weeklyLimit: [null, Validators.pattern(/^(0|[1-9]\d*)?$/)],
      dailyLimit: [null, Validators.pattern(/^(0|[1-9]\d*)?$/)],
      monthlyLimit: [null, Validators.pattern(/^(0|[1-9]\d*)?$/)],
    }, { validator: this.atLeastOneLimitHasValue.bind(this) });
  }

  private _initRegisterIntendedGamblingForm(): void {
    const defaultValue = this.intendedGamblingValues[0];
    this.registerIntendedGamblingForm = this.formBuilder.group({
      intendedGambling: [defaultValue, Validators.required]
    });
  }

  private _validateBirthdateControl(): void {
    this.birthdateControl$ = this.birthdateControl.valueChanges.subscribe(() => {
      if (this.birthdateControl.dirty && !this.birthdateControl.errors) {
        const birthdateWhen18 = moment().subtract(18, 'years');
        const birthdate = moment(this.birthdateControl.value.toString());
        if (!birthdate.isValid()) {
          this.birthdateControl.setErrors({
            pattern: true
          });
        } else if (birthdate > birthdateWhen18) {
          this.birthdateControl.setErrors({
            tooYoung: true
          });
        }
      }
    });
  }

  checkPasswords(group: FormGroup): { notSame: boolean } {
    const password = group.controls.password.value;
    const repeatPassword = group.controls.repeatPassword.value;
    return password === repeatPassword ? null : { notSame: true };
  }

  atLeastOneLimitHasValue(group: FormGroup): { message: string } {
    const dailyLimit = group.controls.dailyLimit.value;
    const weeklyLimit = group.controls.weeklyLimit.value;
    const monthlyLimit = group.controls.monthlyLimit.value;
    const oneLimitHasValue = dailyLimit > 0 || weeklyLimit > 0 || monthlyLimit > 0;
    return oneLimitHasValue ? null : { message: this._translate.instant('Registration.Please_input_at_least_one_value') };
  }

  changeEvent(event: MatCheckboxChange): void {
    this.acceptTerms = event.checked;
  }

  showTerms(): void {
    this.showTermsAndCondition = true;
    this.showConfirmStep = false;
  }

  showConfirm(): void {
    this.showTermsAndCondition = false;
    this.showConfirmStep = true;
  }

  finishRegistration(): void {
    if (!this.registerProfileForm.valid || !this.registerLimitsForm.valid || !this.registerIntendedGamblingForm.valid) {
      return;
    }
    this._pidCprMatchAndRegister();
  }

  private _pidCprMatchAndRegister(): void {
    const pid = this.registerProfileForm.controls.externalId.value;
    const cpr = this.registerProfileForm.controls.nationalId.value;
    this._nemidService.pidCprMatch(pid, cpr).subscribe(
      () => {
        this._registerPlayer();
      },
      () => {
        this._snackBar.open(
          this._translate.instant('Registration.Registration_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }
    );
  }

  private _registerPlayer(): void {
    const profileControls = this.registerProfileForm.controls;
    const limitControls = this.registerLimitsForm.controls;
    const intendedGamblingControls = this.registerIntendedGamblingForm.controls;
    const registrationModel = new RegistrationModel();
    registrationModel.address = profileControls.address.value;
    registrationModel.birthdate = moment.utc(profileControls.birthdate.value.toString());
    registrationModel.city = profileControls.city.value;
    registrationModel.email = profileControls.email.value;
    registrationModel.externalId = profileControls.externalId.value;
    registrationModel.firstName = profileControls.firstName.value;
    registrationModel.gender = profileControls.nationalId.value % 2 ? 'm' : 'f';
    registrationModel.lastName = profileControls.lastName.value;
    registrationModel.login = profileControls.login.value;
    registrationModel.nationalId = profileControls.nationalId.value;
    registrationModel.password = profileControls.password.value;
    registrationModel.phone = profileControls.phone.value;
    registrationModel.zip = profileControls.zip.value;
    registrationModel.depositLimitSingle = limitControls.singleLimit.value || null;
    registrationModel.depositLimitDaily = limitControls.dailyLimit.value || null;
    registrationModel.depositLimitWeekly = limitControls.weeklyLimit.value || null;
    registrationModel.depositLimitMonthly = limitControls.monthlyLimit.value || null;
    registrationModel.intendedGambling = intendedGamblingControls.intendedGambling.value;
    this._authenticationService.register(registrationModel).subscribe(
      () => {
        this._snackBar.open(
          this._translate.instant('Registration.Registration_success'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      },
      () => {
        this._snackBar.open(
          this._translate.instant('Registration.Registration_failed'),
          this._translate.instant('Ok'),
          { duration: SNACKBAR_DURATION }
        );
      }
    );
  }

  showSingleForm(): void {
    this.showSingle = true;
    this.showWeekly = false;
    this.showDaily = false;
    this.showMonthly = false;
  }

  showWeeklyForm(): void {
    this.showSingle = false;
    this.showWeekly = true;
    this.showDaily = false;
    this.showMonthly = false;
  }

  showDailyForm(): void {
    this.showSingle = false;
    this.showWeekly = false;
    this.showDaily = true;
    this.showMonthly = false;
  }

  showMonthlyForm(): void {
    this.showSingle = false;
    this.showWeekly = false;
    this.showDaily = false;
    this.showMonthly = true;
  }

  next(): void {
    this._registrationStepper.next();
  }

  previous(): void {
    this._registrationStepper.previous();
  }

  onExternalLogin(externalId: string): void {
    this.registerProfileForm.controls.externalId.setValue(externalId, {emitEvent: false});
    this.externalLoginIsDone = true;
    this.next();
  }

  ngOnDestroy(): void {
    if (this.birthdateControl$) {
      this.birthdateControl$.unsubscribe();
    }
  }
}
