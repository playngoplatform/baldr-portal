// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SharedRegistrationComponent } from './registration.component';
import { RegistrationRoutingModule } from './registration-routing.module';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ReactiveFormsModule } from '@angular/forms';
import { TermsConditionsModule } from '../../../casino/shared/terms-conditions/terms-conditions.module'; // needs to be moved
import { SharedModule } from '../../shared.module';
import { SharedNemidModule } from '../nemid/nemid.module';
import { SharedTermsConditionsComponent } from '../terms-conditions/terms-conditions.component';


@NgModule({
  declarations: [
    SharedRegistrationComponent,
    SharedTermsConditionsComponent
  ],
  exports: [SharedRegistrationComponent],
  imports: [
    RegistrationRoutingModule,
    CommonModule,
    UiSwitchModule.forRoot({
      size: 'small',
    }),
    ReactiveFormsModule,
    TranslateModule,
    TermsConditionsModule,
    SharedNemidModule,
    SharedModule
  ]
})
export class RegistrationModule { }
