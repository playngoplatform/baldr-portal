// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedRegistrationComponent } from './registration.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ApiService, AppService, AuthenticationService } from '../../../core/services';
import { RouterTestingModule } from '@angular/router/testing';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { LocalizeRouterMockPipe } from '../../../../../tests/localize-router-mock-pipe';
import { LocalizeRouterMockService } from '../../../../../tests/localize-router-mock-service';

describe('SharedRegistrationComponent', () => {
  let component: SharedRegistrationComponent;
  let fixture: ComponentFixture<SharedRegistrationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [UiSwitchModule, RouterTestingModule, ReactiveFormsModule, HttpClientModule, HttpClientTestingModule],
      declarations: [
        SharedRegistrationComponent,
        LocalizeRouterMockPipe
      ],
      providers: [
        AppService,
        ApiService,
        AuthenticationService,
        {provide: LocalizeRouterService, useValue: LocalizeRouterMockService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
