// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedComplianceToolbarDkComponent } from './dk-compliance-toolbar.component';

describe('SharedComplianceToolbarDkComponent', () => {
  let component: SharedComplianceToolbarDkComponent;
  let fixture: ComponentFixture<SharedComplianceToolbarDkComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedComplianceToolbarDkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedComplianceToolbarDkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
