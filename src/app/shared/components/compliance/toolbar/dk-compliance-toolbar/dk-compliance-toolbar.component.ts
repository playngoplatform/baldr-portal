// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../../../../../core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shared-compliance-toolbar-dk',
  templateUrl: './dk-compliance-toolbar.component.html',
  styleUrls: ['./dk-compliance-toolbar.component.less']
})
export class SharedComplianceToolbarDkComponent implements OnInit, OnDestroy {
  private _durationInterval: ReturnType<typeof setInterval>;
  private _clockInterval: ReturnType<typeof setInterval>;
  private _currentPlayer$: Subscription;
  displayTime: string;

  constructor(
    private _authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this._currentPlayer$ = this._authenticationService.currentPlayer$.subscribe(res => {
      clearInterval(this._clockInterval);
      clearInterval(this._durationInterval);
      if (res !== null) {
        this.startDurationInterval();
        return;
      }
      this.startClock();
    });
    if (!this._authenticationService.isAuthenticated()) {
      clearInterval(this._clockInterval);
      clearInterval(this._durationInterval);
      this.startClock();
      return;
    }
    clearInterval(this._clockInterval);
    this.startDurationInterval();
  }

  startDurationInterval(): void {
    const lastLogin = this._authenticationService.getLastLoginTime();
    const duration = moment.utc(moment.duration(moment().diff(lastLogin)).asMilliseconds());
    this.displayTime = duration.format('HH:mm:ss');
    this._durationInterval = setInterval(() => {
      duration.add(1, 'second');
      this.displayTime = duration.format('HH:mm:ss');
    }, 1000);
  }

  startClock(): void {
    const currentTime = moment();
    this._clockInterval = setInterval(() => {
      currentTime.add(1, 'second');
      this.displayTime = currentTime.format('HH:mm:ss');
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this._clockInterval);
    clearInterval(this._durationInterval);
    this._currentPlayer$.unsubscribe();
  }
}
