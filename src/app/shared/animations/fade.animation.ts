// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { animate, style, transition, trigger } from '@angular/animations';

export const fadeIn =
trigger('fadeIn', [
  transition('* => *', [
    style({opacity: 0}),
    animate('300ms ease-in', style({ opacity: 1}))
  ]),
]);

export const fadeOut =
trigger('fadeOut', [
  transition(':leave', [
    style({opacity: 1}),
    animate('300ms ease-out', style({opacity: 0})),
  ]),
]);
