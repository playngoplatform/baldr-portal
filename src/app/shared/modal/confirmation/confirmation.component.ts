// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-shared-modal-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.less']
})
export class SharedModalConfirmationComponent implements OnInit {
  title: string;
  message: string;
  buttonActionName: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.title = data.title;
    this.message = data.message;
    this.buttonActionName = data.buttonActionName;
   }

  ngOnInit() {}

}
