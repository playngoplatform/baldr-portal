// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedFooterComponent } from './components/footer/footer.component';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { SharedHeaderComponent } from './components/header/header.component';
import { SharedComplianceToolbarDkComponent } from './components/compliance/toolbar/dk-compliance-toolbar/dk-compliance-toolbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MaterialModule } from '../material.module';
import { SharedMainMenuComponent } from './components/main-menu/main-menu.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedAccountComponent } from './components/account/account.component';
import { SharedAccountBonusOffersComponent } from './components/account/bonus-offers/bonus-offers.component';
import { SharedAccountDepositLimitsComponent } from './components/account/deposit-limits/deposit-limits.component';
import { SharedAccountLossLimitsComponent } from './components/account/loss-limits/loss-limits.component';
import { SharedAccountMaxSessionTimeComponent } from './components/account/max-session-time/max-session-time.component';
import { SharedAccountOrdersComponent } from './components/account/orders/orders.component';
import { SharedAccountSelfExcludeComponent } from './components/account/self-exclude/self-exclude.component';
import { SharedAccountTransferLimitsComponent } from './components/account/transfer-limits/transfer-limits.component';
import { SharedNewPasswordComponent } from './components/new-password/new-password.component';
import { SharedModalConfirmationComponent } from './modal/confirmation/confirmation.component';
import { SharedModalRealityCheckComponent } from './modal/reality-check/reality-check.component';
import { SharedModalDepositLimitsComponent } from './modal/deposit-limits/deposit-limits.component';
import { SharedAccountGameHistoryComponent } from './components/account/game-history/game-history.component';
import { SharedAccountMyBetsComponent } from './components/account/my-bets/my-bets.component';
import { SharedAccountDepositComponent } from './components/account/deposit/deposit.component';
import { SharedAccountWithdrawalComponent } from './components/account/withdrawal/withdrawal.component';
import { SharedLoginComponent } from './components/login/login.component';
import { SharedModalRequestPasswordChangeComponent } from './components/new-password/modal/request-password-change/request-password-change.component';
import { SharedNemidModule } from './components/nemid/nemid.module';
import { SharedRegistrationComponent } from './components/registration/registration.component';
import { SharedTermsConditionsModule } from '../shared/components/terms-conditions/terms-conditions.module';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
@NgModule({
  declarations: [
    SharedComplianceToolbarDkComponent,
    SharedHeaderComponent,
    SharedFooterComponent,
    SharedMainMenuComponent,
    SharedNewPasswordComponent,
    SharedModalDepositLimitsComponent,
    SharedModalRealityCheckComponent,
    SharedAccountComponent,
    SharedAccountBonusOffersComponent,
    SharedAccountGameHistoryComponent,
    SharedAccountOrdersComponent,
    SharedAccountSelfExcludeComponent,
    SharedAccountDepositLimitsComponent,
    SharedAccountLossLimitsComponent,
    SharedAccountTransferLimitsComponent,
    SharedAccountMaxSessionTimeComponent,
    SharedAccountMyBetsComponent,
    SharedModalDepositLimitsComponent,
    SharedModalRealityCheckComponent,
    SharedModalConfirmationComponent,
    SharedAccountDepositComponent,
    SharedAccountWithdrawalComponent,
    SharedLoginComponent,
    SharedModalRequestPasswordChangeComponent,
    SharedRegistrationComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    LazyLoadImageModule,
    LocalizeRouterModule,
    ReactiveFormsModule,
    TranslateModule,
    InfiniteScrollModule,
    MaterialModule,
    FlexLayoutModule,
    SharedNemidModule,
    SharedTermsConditionsModule
  ],
  exports: [
    SharedComplianceToolbarDkComponent,
    SharedHeaderComponent,
    SharedFooterComponent,
    SharedAccountComponent,
    SharedAccountBonusOffersComponent,
    SharedAccountDepositLimitsComponent,
    SharedAccountLossLimitsComponent,
    SharedAccountMaxSessionTimeComponent,
    SharedAccountOrdersComponent,
    SharedAccountSelfExcludeComponent,
    SharedAccountTransferLimitsComponent,
    SharedAccountGameHistoryComponent,
    SharedAccountMyBetsComponent,
    SharedAccountDepositComponent,
    SharedAccountWithdrawalComponent,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    SharedMainMenuComponent,
    MaterialModule,
    FlexLayoutModule,
    SharedModalDepositLimitsComponent,
    SharedModalRealityCheckComponent,
    SharedModalConfirmationComponent,
    SharedModalRequestPasswordChangeComponent
  ],
  entryComponents: [
    SharedLoginComponent,
    SharedModalRequestPasswordChangeComponent,
    SharedRegistrationComponent,
    SharedModalConfirmationComponent
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} }
  ]
})
export class SharedModule { }
