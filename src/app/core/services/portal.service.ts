// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';

@Injectable()
export class PortalService {
  isMobile: boolean = false;
  isTablet: boolean = false;
  isDesktop: boolean = false;
  userAgent: string;

  constructor() {
    this.setDeviceType();
  }

  setDeviceType(): void {
    // this.isLandscapeMode = window.orientation ? true : false;
    this.userAgent = window.navigator.userAgent.toLowerCase();
    if (/(ipad|tablet|(android(?!.*mobile)))/i.test(this.userAgent)) {
      this.isTablet = true;
      return;
    }
    if (
      /mobile|windows phone|lumia|android|webos|iphone|ipod|blackberry|playbook|bb10|opera mini|\bcrmo\/|opera mobi/i.test(this.userAgent)
    ) {
      this.isMobile = true;
      return;
    }
    this.isDesktop = true;
  }

  getDevice(): string {
    if (this.isMobile) {
      return 'mobile';
    } else if (this.isDesktop) {
      return 'desktop';
    } else if (this.isTablet) {
      return 'tablet';
    }
    return 'unknown';
  }​​​​​
}
