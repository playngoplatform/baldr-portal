// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { EventEmitter, Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { UserModel, RegistrationModel } from '../models/user.model';
import { ReplaySubject, Observable } from 'rxjs';
import * as moment from 'moment';

@Injectable()
export class AuthenticationService {
  private _currentPlayer$: ReplaySubject<UserModel> = new ReplaySubject(1);
  public isAuthenticated$: EventEmitter<any>;
  currentPlayer$: Observable<UserModel>;

  constructor(
    private apiService: ApiService
  ) {
    this.isAuthenticated$ = new EventEmitter();
    this.currentPlayer$ = this._currentPlayer$.asObservable();
  }

  login(username: string, password: string): Observable<UserModel> {
    return this.apiService.login(username, password).pipe(
      map((user: UserModel) => {
        if (user) {
          this._setUserAsLoggedIn(user);
        }
        return user;
      })
    );
  }

  loginWithToken(tokenType: string, token: string): Observable<UserModel> {
    return this.apiService.post('/users/login_with_token', { tokenType: tokenType, token: token }).pipe(
      map((user: UserModel) => {
        if (user) {
          this._setUserAsLoggedIn(user);
        }
        return user;
      })
    );
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('auth_token');
    localStorage.removeItem('deposits');
    localStorage.removeItem('withdrawals');
    this._currentPlayer$.next(null);
    location.reload();
  }

  register(payload: RegistrationModel): Observable<UserModel> {
    return this.apiService.post('/users/register', payload).pipe(
      map((user: UserModel) => {
        if (user) {
          this._setUserAsLoggedIn(user);
        }
        return user;
      })
    );
  }

  refresh(): Observable<UserModel> {
    return this.apiService.post('/users/refresh').pipe(map(user => {
      if (user) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this._currentPlayer$.next(user);
      }
      return new UserModel().deserialize(user);
    }));
  }

  currentUser(): UserModel {
    const loggedInPlayer = JSON.parse(localStorage.getItem('currentUser'));
    return loggedInPlayer ? new UserModel().deserialize(loggedInPlayer) : null;
  }

  isAuthenticated(): boolean {
    const data = localStorage.getItem('currentUser');
    return data != null;
  }

  private _setUserAsLoggedIn(user: UserModel): void {
    localStorage.setItem('currentUser', JSON.stringify(user));
    localStorage.setItem('lastLogin', moment().unix().toString());
    this._currentPlayer$.next(user);
    this.isAuthenticated$.emit();
  }

  getLastLoginTime(): moment.Moment {
    const lastLogin = localStorage.getItem('lastLogin');
    if (lastLogin) {
      return moment.unix(parseInt(lastLogin, 10));
    }
  }

  logoutPlayer(): void {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('auth_token');
    localStorage.removeItem('deposits');
    localStorage.removeItem('withdrawals');
    this._currentPlayer$.next(null);
  }
}
