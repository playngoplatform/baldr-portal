// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

import { ApiService } from './api.service';
import { NotFoundModel } from '../models';

@Injectable()
export class NotFoundService {
  constructor(
    private apiService: ApiService
  ) {}

  get(): Observable<NotFoundModel> {
    return this.apiService.get('/pages/page', new HttpParams({ fromObject: { id: '404' } }));
  }
}
