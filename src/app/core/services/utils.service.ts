// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';

import { ItemLimit } from '../models';

@Injectable()
export class UtilsService {
  constructor() {}

  parseText(text: string = '', limits: ItemLimit[] = []) {
    const re = /\[\[[a-z_]{7}\]\]/ig;
    const itemsLimit = text.match(re);

    if (!itemsLimit) { return text; }

    let newText = text;

    itemsLimit.forEach((item) => {
      const cur = item.slice(2, 5).toLowerCase();
      const type = item.slice(6, 9).toLowerCase();
      const limit = limits.find(itemLimit => itemLimit.currency.toLowerCase() === cur);
      newText = newText.replace(item, limit[type]);
    });

    return newText;
  }

  isMobile = () => /Mobi/.test(navigator.userAgent);

  scrollTo = (selector: string, offset: number = 0) => {
    const element = $(selector)[0];
    if (element) {
      window.scrollTo({
        top: element.offsetTop + offset,
        behavior: 'smooth'
      });
    }
  }
}
