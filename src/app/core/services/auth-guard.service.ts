// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { Router, CanActivate, CanLoad, Route, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { UtilsService } from './utils.service';


@Injectable()
export class AuthGuardService implements CanActivate, CanLoad, CanActivateChild {

  constructor(public auth: AuthenticationService,
              public router: Router,
              private utilsService: UtilsService,
              private localize: LocalizeRouterService) {}

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate([this.localize.translateRoute('/')]);
      return false;
    }
    return true;
  }

  canLoad(_route: Route): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate([this.localize.translateRoute('/')]);
      return false;
    }
    return true;
  }

  canActivateChild(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): boolean {
    if (route.data.localizeRouter.path === 'registration') {
      if (this.auth.isAuthenticated()) {
        return false;
      } else {
        if (!this.utilsService.isMobile()) {
          this.router.navigate([this.localize.translateRoute('/')], {state: {registration: true}});
        }
      }
    }
    return true;
  }
}
