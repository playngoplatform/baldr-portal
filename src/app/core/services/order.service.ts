// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { Order } from '../models';
import { map } from 'rxjs/operators';

@Injectable()
export class OrderService {
  constructor(
    private apiService: ApiService
  ) {}

  getOrders(skip: number = 0, limit: number = 50): Observable<Order[]> {
    return this.apiService.get(`/profile/orders?skip=${skip}&limit=${limit}`).pipe(map(res => {
      res = res.map(order => new Order(order));
      return res;
    }));
  }
}
