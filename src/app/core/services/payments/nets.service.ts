// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';

@Injectable()
export class PaymentsNetsService {
  constructor(
    private apiService: ApiService
  ) {}

  notify(transactionId: number, responseCode: string): Observable<any> {
    return this.apiService.post('/deposit/nets/notify', { 'transaction_id': transactionId, 'response_code': responseCode });
  }
}
