// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PlayerProtectionLimits, PlayerDepositLimit, PlayerProtectionLimitsResponse, PlayerBetWinSummary, ProfileModel } from '../models';

@Injectable()
export class PlayerService {
  homeUrl: string;

  constructor(
    private _apiService: ApiService
  ) {
    this.homeUrl = 'https://' + window.location.host;
  }

  getProfile(): Observable<{ userInfo: ProfileModel, countries: { code: string, name: string }[], locales: string[] }> {
    return this._apiService.get('/profile').pipe(map(res => {
      res.userInfo = new ProfileModel().deserialize(res.userInfo);
      return res;
    }));
  }

  updateProfile(profile: ProfileModel) {
    return this._apiService.post('/profile', profile);
  }

  setSelfExclusion(lockUntil: moment.Moment): Observable<any> {
    const params = {
      'lock_until': lockUntil
    };
    return this._apiService.post('/users/self_exclude', params);
  }

  getProtectionLimits(): Observable<PlayerProtectionLimitsResponse> {
    return this._apiService.get('/users/protection_limits').pipe(map(res => {
      const limits = new PlayerProtectionLimitsResponse(res);
      limits.currentLimits = {
        depositLimits: new PlayerDepositLimit(res.currentLimits.depositLimits),
        lossLimits: new PlayerDepositLimit(res.currentLimits.lossLimits),
        transferLimits: new PlayerDepositLimit(res.currentLimits.transferLimits),
        maxSessionTime: res.currentLimits.maxSessionTime,
        realityCheckFrequency: res.currentLimits.realityCheckFrequency,
      };
      limits.pendingLimits = {
        depositLimits: new PlayerDepositLimit(res.pendingLimits.depositLimits),
        lossLimits: new PlayerDepositLimit(res.pendingLimits.lossLimits),
        transferLimits: new PlayerDepositLimit(res.pendingLimits.transferLimits),
        maxSessionTime: res.pendingLimits.maxSessionTime,
        realityCheckFrequency: res.pendingLimits.realityCheckFrequency,
      };
      return limits;
    }));
  }

  updateProtectionLimits(limits: PlayerProtectionLimits): Observable<any> {
    return this._apiService.put('/users/protection_limits', { limits });
  }

  getBetWinSummary(from: moment.Moment, to: moment.Moment): Observable<PlayerBetWinSummary> {
    const fromFormated = from.format('YYYY-MM-DDTHH:mm:ss');
    const toFormated = to.format('YYYY-MM-DDTHH:mm:ss');
    return this._apiService.get(`/users/bet_win_summary?from=${fromFormated}&to=${toFormated}`).pipe(
      map(res => new PlayerBetWinSummary().deserialize(res))
    );
  }
}
