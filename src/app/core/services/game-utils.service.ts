// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable, ElementRef } from '@angular/core';

import { UtilsService } from './utils.service';
import { ItemGame, UserModel, GameMode } from '../models';
import { AuthenticationService } from '../../core/services/authentication.service';
import { NetEntService } from './net-ent.service';
import { ToasterService } from 'angular2-toaster';
import { GameService } from './game.service';

@Injectable()
export class GameUtilsService {
  gameScriptId: string = '';
  currentUser: UserModel;

  constructor(
    private _utilsService: UtilsService,
    private _authenticationService: AuthenticationService,
    private _netEntService: NetEntService,
    private _toasterService: ToasterService,
    private _gameService: GameService
  ) {}

  getWindowGameSize(widthItem: number, heightItem: number, padding: number) {
    const maxWidth = window.innerWidth - padding * 2;
    let proportion = widthItem / heightItem;
    let heightGame: number = window.innerHeight - padding * 2;
    let widthGame: number = proportion * heightGame;

    if (widthGame > maxWidth) {
      widthGame = maxWidth;
      proportion = heightItem / widthItem;
      heightGame = proportion * widthGame;
    }

    return { widthGame, heightGame };
  }

  setWindowGameSize(width: number, height: number, itemRef: ElementRef, padding: number) {
    const { widthGame, heightGame }: { widthGame: number, heightGame: number } = this.getWindowGameSize(width, height, padding);
    const { nativeElement } = itemRef;

    nativeElement.style.maxWidth = `${widthGame}px`;
    nativeElement.style.minWidth = `${widthGame}px`;
    nativeElement.style.maxHeight = `${heightGame}px`;
    nativeElement.style.minHeight = `${heightGame}px`;
  }

  openGame(data: ItemGame, mode: GameMode, itemRef: ElementRef, onSuccessInit: Function = () => ({}), padding: number = 50) {
    const { provider, gameId, width, height } = data;
    const isDesktop = !this._utilsService.isMobile();

    this._gameService.getPlayUrl(gameId, provider, isDesktop, mode)
      .subscribe((response: any) => {
        if (!isDesktop) {
          switch (provider) {
            case 'netent':
              this._netEntService.openGame(response);
              return;
            default:
              window.location.href = response.url || response.value;
              return;
          }
        }
        this.setWindowGameSize(width, height, itemRef, padding);
        window.onresize = () => {
          this.setWindowGameSize(width, height, itemRef, padding);
        };

        onSuccessInit();

        if (response.type === 'js') {
          console.log('JS: ', response.url);
          const script = document.createElement('script');
          script.src = response.url;
          script.id = `game-${Date.now()}`;
          this.gameScriptId = script.id;
          document.body.appendChild(script);

          if (provider === 'netent') {
            this._netEntService.openGame(response, itemRef.nativeElement);
          }

        } else if (response.type === 'html') {

          console.log('HTML: ', response.value);
          const iframe = document.createElement('iframe');
          iframe.src = response.value;
          iframe.id = `game-${Date.now()}`;
          this.gameScriptId = iframe.id;
          iframe.style.width = '100%';
          iframe.style.height = '100%';
          iframe.style.maxWidth = '1280px';
          iframe.style.maxHeight = '724px';
          iframe.style.border = 'none';
          iframe.style.marginLeft = 'auto';
          iframe.style.marginRight = 'auto';
          setTimeout(() => {
            const gameContainer = itemRef.nativeElement;
            gameContainer.style.width = '1920px';
            gameContainer.style.height = '460px';
            gameContainer.appendChild(iframe);
          }, 500);
        }
      }, e => {
        const errorMessage = e.error && e.error.error
          ? e.error.error
          : 'We could not launch your game';
        this._toasterService.pop('error', 'Game launch failed!', errorMessage);
      });
  }

  closeGame() {
    window.onresize = null;
    if (document.getElementById(this.gameScriptId)) {
        document.getElementById(this.gameScriptId).remove();
        document.getElementById('the-game').innerHTML = '';
        this.currentUser = this._authenticationService.currentUser();
        this._authenticationService.refresh().subscribe(user => {
          this.currentUser = user;
        });
    }
  }
}
