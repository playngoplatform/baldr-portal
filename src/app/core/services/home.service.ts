// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { HomeModel, BannerModel } from '../models';

@Injectable()
export class HomeService {
  constructor(
    private apiService: ApiService
  ) {}

  getAll(): Observable<HomeModel> {
    return this.apiService.get('/games');
  }

  getBanners(): Observable<BannerModel[]> {
    return this.apiService.get('/images/banners');
  }
}
