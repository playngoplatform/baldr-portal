// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { QueryParams, RequestOptions } from '../models/request.model';
import { catchError } from 'rxjs/operators';

@Injectable()
export class HttpRequestService implements HttpInterceptor {
  onInterceptedRequest: Observable<null>;
  private _onInterceptedRequest$ = new Subject<null>();

  constructor(
    private _http: HttpClient,
  ) {
    this.onInterceptedRequest = this._onInterceptedRequest$.asObservable();
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (req.url.match(/proxy\/time/)) {
      // way to ignore a url
    } else {
      this._onInterceptedRequest$.next();
    }

    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      })
    );
  }

  getRequestOptions(
    queryParams?: QueryParams,
    responseType = 'json'
  ): RequestOptions {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json'
    });
    const requestOptions: RequestOptions = {
      headers: headers,
      responseType: responseType
    };
    if (queryParams) {
      requestOptions.params = queryParams;
    }
    return requestOptions;
  }

  get(
    url: string,
    queryParams?: QueryParams,
    responseType = 'json'
  ): Observable<any> {
    return this._http.get(
      url,
      this.getRequestOptions(queryParams, responseType)
    );
  }

  delete(url: string, queryParams?: QueryParams): Observable<Object> {
    return this._http.delete(
      url,
      this.getRequestOptions(queryParams)
    );
  }

  post(url: string, body?: any): Observable<Object> {
    return this._http.post(
      url,
      body,
      this.getRequestOptions()
    );
  }

  postMultiform(url: string, body?: any): Observable<Object> {
    const headers = new HttpHeaders({
      'Accept': 'application/json'
    });
    const requestOptions: RequestOptions = {
      headers: headers,
    };
    return this._http.post(
      url,
      body,
      requestOptions
    );
  }

  put(url: string, body?: any): Observable<Object> {
    return this._http.put(
      url,
      body,
      this.getRequestOptions()
    );
  }

  patch(url: string, body?: any): Observable<Object> {
    return this._http.patch(
      url,
      body,
      this.getRequestOptions()
    );
  }
}
