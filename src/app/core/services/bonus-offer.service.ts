// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import * as moment from 'moment';
import { Observable, range } from 'rxjs';
import { map, concatMap, takeWhile } from 'rxjs/operators';
import { BonusOffer, BonusOfferParams } from '../models/bonus-offer.model';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class BonusOfferService {

  constructor(
    private _apiService: ApiService
  ) {  }

  getBonusOffers(params: BonusOfferParams = new BonusOfferParams()): Observable<{ bonusOffers: BonusOffer[] }> {
    let queryParams = new HttpParams();
    for (const key in params) {
      if (params[key] !== null) {
        queryParams = queryParams.append(key, params[key].toString());
      }
    }
    return this._apiService.get('/bonus_offers', queryParams).pipe(map(res => {
      res.bonusOffers = res.bonusOffers.map(offer => {
        const mappedOffer = new BonusOffer(offer);
        mappedOffer.activated = offer.activated ? moment.utc(offer.activated) : null;
        mappedOffer.cleared = offer.cleared ? moment.utc(offer.cleared) : null;
        mappedOffer.expired = offer.expired ? moment.utc(offer.expired) : null;
        mappedOffer.expires = offer.expires ? moment.utc(offer.expires) : null;
        return mappedOffer;
      });
      return res;
    }));
  }

  getAllBonusOffers(params: BonusOfferParams = new BonusOfferParams()): Observable<{ bonusOffers: BonusOffer[] }> {
    return new Observable<{ bonusOffers: BonusOffer[], totalCount: number }>(subscriber => {
      const result = {
        bonusOffers: <BonusOffer[]>[],
        totalCount: <number>0,
      };
      let takeIteration: number;
      range(0, 1000000).pipe(
        concatMap(take => {
          params.limit = 1000;
          params.skip = params.limit * take;
          takeIteration = take;
          return this.getBonusOffers(params);
        }),
        map(res => {
          result.bonusOffers = result.bonusOffers.concat(res.bonusOffers);
          result.totalCount = result.bonusOffers.length;
          return result;
        }),
        takeWhile(res => {
          return res.bonusOffers.length === params.limit * (takeIteration + 1);
        }, true)
      ).subscribe(() => {
      }, e => {
        subscriber.error(e);
        throw e;
      }, () => {
        subscriber.next(result);
        subscriber.complete();
      });
    });
  }

  claimBonusOffer(bonusOfferId: number): Observable<any> {
    return this._apiService.post('/bonus_offers/claim', { bonusOfferId });
  }

  voidBonusOffer(bonusOfferId: number): Observable<any> {
    return this._apiService.post('/bonus_offers/void', { bonusOfferId });
  }

  voidAllBonusOffers(): Observable<any> {
    return this._apiService.post('/bonus_offers/void', { bonusOfferId: null });
  }
}
