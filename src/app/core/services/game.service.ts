// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable, ElementRef } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { ItemGame, GameHistory, GameMode } from '../models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  gameContainerRef: ElementRef;

  constructor(
    private apiService: ApiService
  ) {}

  getPlayUrl(gameId: string, provider: string, desktop: boolean, mode: GameMode): Observable<string> {
    const params = new HttpParams({
      fromObject: {
        gameId,
        provider,
        mode,
        desktop: `${desktop}`,
        ...desktop && { width: window.innerWidth.toString() },
        ...desktop && { height: window.innerHeight.toString() }
      }
    });
    return this.apiService.get('/games/play_url', params);
  }

  get(id: string): Observable<ItemGame> {
    return this.apiService.get('/games/game', new HttpParams({ fromObject: { gameId: id }}));
  }

  getGameHistory(skip: number = 0, limit: number = 50): Observable<GameHistory[]> {
    return this.apiService.get(`/profile/game_history?skip=${skip}&limit=${limit}`).pipe(map(res => {
      res = res.map(gameHistory => new GameHistory(gameHistory));
      return res;
    }));
  }
}
