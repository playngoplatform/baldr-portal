// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';

@Injectable()
export class ResetPasswordService {
  constructor(
    private _apiService: ApiService
  ) { }

  requestPasswordChange(playerLogin: string): Observable<any> {
    return this._apiService.post(`/users/reset_password`, { login: playerLogin });
  }

  changePassword(token: string, password: string): Observable<any> {
    return this._apiService.post('/users/set_password', { token, password });
  }
}
