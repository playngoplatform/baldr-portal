// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { BonusesModel, BonusModel } from '../models/bonuses.model';

@Injectable()
export class BonusService {
  constructor(
    private apiService: ApiService
  ) {}

  getAll(): Observable<BonusesModel> {
    return this.apiService.get('/bonuses/data');
  }

  get(id: string): Observable<BonusModel> {
    return this.apiService.get('/bonuses/item', new HttpParams({ fromObject: { id } }));
  }
}
