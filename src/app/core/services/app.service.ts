// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from './api.service';
import { ItemCategory, LocalesModel, SupportModel } from '../models';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Injectable()
export class AppService {
  public categoriesGames$: BehaviorSubject<ItemCategory[]>;
  private _categoriesGames: ItemCategory[] = [];
  public locales$: EventEmitter<LocalesModel>;
  public support$: EventEmitter<SupportModel>;
  homeUrl: string;

  constructor(
    private apiService: ApiService,
    private localize: LocalizeRouterService
  ) {
    this.categoriesGames$ = new BehaviorSubject([]);
    this.locales$ = new EventEmitter();
    this.support$ = new EventEmitter();
    this.homeUrl = 'https://' + window.location.host;
  }

  getCategoriesGames() {
    this.apiService.get('/games/categories').subscribe((data) => {
      this.categoriesGames = data;
      this.categoriesGames$.next(this._categoriesGames);
    });
  }

  get categoriesGames() {
    return this._categoriesGames;
  }

  set categoriesGames(data) {
    this._categoriesGames = data;
  }

  getLocales() {
    this.apiService.get('/locales').subscribe((data) => {
      this.locales$.emit(data);
    });
  }

  getLocSupport() {
    this.apiService.get('/pages/support').subscribe((data) => {
      this.support$.emit(data);
    });
  }

  getSupport(): Observable<SupportModel> {
    return this.apiService.get('/pages/support');
  }

  getRegistration() {
    return this.apiService.get('/users/register');
  }

  getDepositMethods() {
    return this.apiService.get('/deposit');
  }

  getWithdrawalMethods() {
    return this.apiService.get('/withdrawal');
  }

  postDeposit(method: string, amount: number, bonusCode: any): Observable<any> {
    return this.apiService.post('/deposit',
      {method: method,
        amount: amount,
        bonus_code: bonusCode,
        success_url: this.homeUrl + this.localize.translateRoute('/deposit/success'),
        failure_url: this.homeUrl + this.localize.translateRoute('/deposit/failure')
      });
  }

  postWithdrawal(method: string, amount: number, bonusCode: any): Observable<number> {
    return this.apiService.post('/withdrawal',
      {method: method,
        amount: amount,
        bonus_code: bonusCode
      });
  }

  postVerifyUser(userId, emailId): Observable<any> {
    return this.apiService.post('/users/verify', { user_id: userId, email_id: emailId });
  }
}
