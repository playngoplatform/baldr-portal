// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NetEntService {

  constructor() { }

  openGame(response: any, gameContainer: any = null): void {
    const netEntGameContainer = document.createElement('div');
    netEntGameContainer.id = response.config.targetElement;
    setTimeout(() => {
      gameContainer.style.width = '1920px';
      gameContainer.style.height = '460px';
      gameContainer.appendChild(netEntGameContainer);
      const success = _ => {};
      const error = _ => {};
      setTimeout(() => {
        window['netent'].launch(response.config, success, error);
      }, 1000);
    });
  }
}
