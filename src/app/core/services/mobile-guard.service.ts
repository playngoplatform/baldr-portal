// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import {CanLoad, Route} from '@angular/router';
import {UtilsService} from './utils.service';


@Injectable()
export class MobileGuardService implements CanLoad {

  constructor(private utilsService: UtilsService) {}

  canLoad(_route: Route): boolean {
    return this.utilsService.isMobile();
  }
}
