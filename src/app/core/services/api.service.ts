// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';


import { environment } from '../../../environments/environment';

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}


  // TODO: Refactor it!
  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.apiUrl}${path}`, { params: params });
  }

  post(path: string, params = null): Observable<any> {
    return this.http.post(`${environment.apiUrl}${path}`, params );
  }

  put(path: string, params = null): Observable<any> {
    return this.http.put(`${environment.apiUrl}${path}`, params );
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/users/login`, {username: username, password: password });
  }

  rePassword(username: string): Observable<any> {
    return this.http.post(`${environment.apiUrl}/users/reset_password`, {login: username});
  }

}
