// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class DefHeaderInterceptor implements HttpInterceptor {
  constructor(private translate: TranslateService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headersConfig = {
      'Accept-Language': this.translate.currentLang || this.translate.getBrowserLang(),
      'Auth-Token': localStorage.getItem('auth_token') || ''
    };
    const request = req.clone({ setHeaders: headersConfig });
    return next.handle(request);
  }
}

