// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {AuthenticationService} from '../services';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(public authenticationService: AuthenticationService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.headers.get('Auth-Token')) {
            localStorage.setItem('auth_token', event.headers.get('Auth-Token'));
          }
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401 ) {
          if (this.authenticationService.isAuthenticated()) {
            this.authenticationService.logout();
            location.reload();
          }
        }
        return throwError(error);
      })
    );
  }
}
