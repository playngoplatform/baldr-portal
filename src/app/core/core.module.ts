// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {NgModule, Optional, SkipSelf} from '@angular/core';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProgressBarService } from './services/progress-bar.service';
import { DefHeaderInterceptor } from './interceptors/def.header.interceptor';
import { ProgressInterceptor } from './interceptors/progress.interceptor';

import {
  ApiService, HomeService, AppService, BonusService, UtilsService,
  GameUtilsService, GameService, NotFoundService, AuthenticationService, NetEntService, PlayerService,
  ResetPasswordService, BonusOfferService, OrderService, AuthGuardService
} from './services';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { MobileGuardService } from './services/mobile-guard.service';
import { HttpRequestService } from './services/http-request.service';
import { PortalService } from './services/portal.service';



@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ProgressInterceptor, multi: true, deps: [ProgressBarService]},
    {provide: HTTP_INTERCEPTORS, useClass: DefHeaderInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    ApiService,
    HomeService,
    AppService,
    BonusService,
    GameUtilsService,
    UtilsService,
    GameService,
    NotFoundService,
    AuthenticationService,
    MobileGuardService,
    NetEntService,
    PlayerService,
    BonusOfferService,
    ResetPasswordService,
    OrderService,
    HttpRequestService,
    PortalService,
    AuthGuardService
  ]
})

export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
