// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded.Import Core modules in the AppModule only.`);
  }
}
