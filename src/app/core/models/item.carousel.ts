// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface ItemCarousel {
  category: string;
  action: string;
  name: string;
  thumbnail: string;
}
