// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ItemLimit } from './index';

export type ParseTextModel = (text: string, limits: ItemLimit[]) => string;
