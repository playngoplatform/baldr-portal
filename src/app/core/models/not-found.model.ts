// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface NotFoundModel {
  content?: string;
  title?: string;
  meta_title?: string;
  meta_description?: string;
}
