// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface ItemNews {
  image: string;
  title: string;
  gameId: string;
}
