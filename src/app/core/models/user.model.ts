// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {Deserializable} from './deserializable';

export class UserModel implements Deserializable {
  nickname: string;
  total_balance: number;
  bonus_balance: number;
  withdrawable_balance: number;
  wagering_requirement: number;
  bonus_wagered_fraction: number;
  protection_limit_set: boolean;

  currency: string;

  constructor() {
    this.nickname = '';
    this.total_balance = 0;
    this.bonus_balance = 0;
    this.withdrawable_balance = 0;
    this.wagering_requirement = 0;
    this.bonus_wagered_fraction = 0;
    this.currency = '';
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}

export class RegistrationModel implements Deserializable {
  address: string;
  birthdate: moment.Moment;
  bonusCode: string;
  city: string;
  country: string;
  currency: string;
  email: string;
  externalId: string;
  firstName: string;
  gender: string;
  lastName: string;
  login: string;
  nationalId: string;
  password: string;
  phone: string;
  zip: string;
  depositLimitSingle: number;
  depositLimitWeekly: number;
  depositLimitDaily: number;
  depositLimitMonthly: number;
  intendedGambling: string;

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}

export class ProfileModel implements Deserializable {
  address1: string;
  city: string;
  country: string;
  email: string;
  firstName: string;
  lastName: string;
  login: string;
  language: string;
  mobileNumber: string;
  nickname: string;
  password: string;
  phone: string;
  zip: string;
  private _gender: string = 'm';

  constructor() { }

  get gender(): string {
    return this._gender;
  }
  set gender(value: string) {
    if (['m', 'f'].indexOf(value) > 0) {
      this._gender = value;
    } else {
      this._gender = 'm';
    }
  }

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}

export interface ISelfExcludePeriod {
  name: string;
  amount: number;
  type: string;
}

export const SELF_EXCLUDE_PERIODS: ISelfExcludePeriod[] = [
  {
    name: '24 hours',
    amount: 24,
    type: 'hours'
  },
  {
    name: '1 month',
    amount: 1,
    type: 'months'
  },
  {
    name: 'Permanent',
    amount: 100,
    type: 'years'
  }
];

export class PlayerBetWinSummary implements Deserializable {
  bonusMoneyBet: number;
  bonusMoneyWin: number;
  playerCurrency: string;
  realMoneyBet: number;
  realMoneyWin: number;

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}

export const INTENDED_GAMBLING_VALUES_DKK = [
  '< 250 DKK',
  '250 - 500 DKK',
  '501 - 1000 DKK',
  '1001 - 2500 DKK',
  '> 2500 DKK'
];
