// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import {PageModel} from './page.model';
import {Deserializable} from './deserializable';

export class PaymentLimitsModel {
  levels: string[];
  methods: any;

  constructor(obj?: any) {
    this.levels = obj.levels;
    this.methods = obj.methods;
  }
}

export class SupportModel implements Deserializable {
  pages: PageModel[];
  deposit_limits?: PaymentLimitsModel;
  withdrawal_limits?: PaymentLimitsModel;

  constructor() {
    this.pages = [];
  }

  deserialize(input: any): this {
    this.pages = input.pages.map(page => new PageModel(page));
    if (input.deposit_limits) {
      this.deposit_limits = input.deposit_limits =  new PaymentLimitsModel(input.deposit_limits);
    }
    if (input.withdrawal_limits) {
      this.withdrawal_limits = input.withdrawal_limits = new PaymentLimitsModel(input.withdrawal_limits);
    }
    return this;
  }
}
