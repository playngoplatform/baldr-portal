// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import * as moment from 'moment';

export class GameHistory {
  product: string;
  game: string;
  started: moment.Moment;
  finished: moment.Moment;
  bet: string;
  won: string;

  constructor(obj?: any) {
    this.game = obj.game;
    this.product = obj.product;
    this.started = obj.started ? moment.utc(obj.started) : null;
    this.finished = obj.finished ? moment.utc(obj.finished) : null;
    this.bet = `${obj.bet.value} ${obj.bet.currency}`;
    this.won = `${obj.won.value} ${obj.won.currency}`;
  }
}
