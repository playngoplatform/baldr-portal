// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface ItemGame {
  category?: string;
  locked?: boolean;
  provider?: string;
  name?: string;
  gameId?: string;
  isMobile?: boolean;
  isDesktop?: boolean;
  imageAlt?: string;
  thumbnail?: string;
  width?: number;
  height?: number;
}
