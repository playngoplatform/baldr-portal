// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { HttpHeaders } from '@angular/common/http';

export interface RequestOptions {
  headers: HttpHeaders;
  params?: QueryParams;
  responseType?: any;
}

export class QueryParams {
  [param: string]: string;
  constructor(obj?: {}) {
    if (obj) {
      Object.assign(this, obj);
    }
  }
}
