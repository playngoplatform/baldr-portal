// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface ItemLimit {
  currency: string;
  min: number;
  max: number;
}

export interface ItemBonus {
  id: string;
  eligible: boolean;
  code: string;
  fixed_amount: boolean;
  limits: ItemLimit[];
  title: string;
  thumbnail: string;
  description: string;
}

export interface BonusesModel {
  title?: string;
  header?: string;
  footer?: string;
  items?: ItemBonus[];
  meta_title?: string;
  meta_description?: string;
  promo?: string;
}

export interface BonusModel {
  id?: string;
  title?: string;
  content?: string;
  thumbnail?: string;
  description?: string;
  code?: string;
  fixed_amount?: boolean;
  limits?: ItemLimit[];
  eligible?: boolean;
  meta_title?: string;
  meta_description?: string;
}
