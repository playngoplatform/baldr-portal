// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export class Config {
  realityCheck: ConfigRealityCheck = {
    enabled: true,
    timer: 3600000
  };
}

export class ConfigRealityCheck {
  enabled: boolean;
  timer: number;
}

export const SNACKBAR_DURATION = 5000;
export const MEDIA_BREAK_POINT = 959;
