// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0

export class PaymentMethodLimitModel {
  currency: string;
  level_name: string;
  min: number;
  max: number;

  constructor(obj?: any) {
    Object.assign(this, obj);
  }
}


export class PaymentMethodModel {
  name: string;
  limits: PaymentMethodLimitModel[];

  constructor(obj?: any) {
    this.name = obj.name;
    this.limits = obj.limits.map(item => new PaymentMethodLimitModel((item)));
  }
}


export class Deposit {
  id: string;
  name: string;
  description: string;
  thumbnail: string;
  min: number;
  max: number;
  request_account: boolean;

  constructor(obj?: any) {
    Object.assign(this, obj);
  }
}

export class Withdrawal {
  id: string;
  name: string;
  description: string;
  thumbnail: string;
  min: number;
  max: number;

  constructor(obj?: any) {
    Object.assign(this, obj);
  }
}
