// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export class PageModel {
  name: string;
  title: string;
  content: string;

  constructor(obj: any) {
    this.name = obj.name;
    this.title = obj.title;
    this.content = obj.content;
  }
}
