// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import { ItemCategory } from './item.category';
import { ItemGame } from './item.game';
import { ItemCarousel } from './item.carousel';

export interface HomeModel {
  games?: ItemGame[];
  categories?: ItemCategory[];
  carousels?: ItemCarousel[];
  providers?: Object[];
  settings?: Object;
}
