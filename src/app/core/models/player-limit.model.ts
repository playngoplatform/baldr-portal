// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export class PlayerDepositLimit {
  week: number = null;
  day: number = null;
  month: number = null;
  single: number = null;
  constructor(obj: Partial<PlayerDepositLimit> = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class PlayerLossLimit {
  week: number = null;
  day: number = null;
  month: number = null;
  single: number = null;
  constructor(obj: Partial<PlayerLossLimit> = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class PlayerTransferLimit {
  week: number = null;
  day: number = null;
  month: number = null;
  single: number = null;
  constructor(obj: Partial<PlayerTransferLimit> = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class PlayerProtectionLimits {
  depositLimits: PlayerDepositLimit;
  lossLimits: PlayerLossLimit;
  maxSessionTime: number;
  realityCheckFrequency: number;
  transferLimits: PlayerTransferLimit;
  constructor(obj: Partial<PlayerTransferLimit> = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class PlayerProtectionLimitsResponse {
  currentLimits: {
    depositLimits: PlayerDepositLimit,
    lossLimits: PlayerLossLimit,
    maxSessionTime: number,
    realityCheckFrequency: number,
    transferLimits: PlayerTransferLimit
  };
  pendingLimits: {
    depositLimits: PlayerDepositLimit,
    lossLimits: PlayerLossLimit,
    maxSessionTime: number,
    realityCheckFrequency: number,
    transferLimits: PlayerTransferLimit
  };
  constructor(obj: Partial<PlayerTransferLimit> = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}
