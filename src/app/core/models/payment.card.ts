// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface PaymentCard {
  image: string;
  title: string;
  class: string;
}
