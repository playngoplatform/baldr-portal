// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import * as moment from 'moment';

export class Order {
  amount: number;
  currency: string;
  endDate: moment.Moment;
  startDate: moment.Moment;
  provider: string;
  state: string;

  constructor(obj?: any) {
    this.amount = obj.amount;
    this.currency = obj.currency;
    this.endDate = obj.endDate ? moment.utc(obj.endDate) : null;
    this.startDate = obj.startDate ? moment.utc(obj.startDate) : null;
    this.provider = obj.provider;
    this.state = obj.state;
  }
}
