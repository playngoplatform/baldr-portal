// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface BannerModel {
  action?: string;
  object_id?: string;
  thumbnail?: string;
}
