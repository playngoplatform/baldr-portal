// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
import * as moment from 'moment';

export enum BONUS_OFFER_STATES {
  Pending,
  Active,
  Wagering,
  Converted,
  Lost,
  Voided,
  Expired
}

export class BonusOfferParams {
  state: string = null;
  limit: number = null;
  skip: number = null;

  constructor(obj: Partial<BonusOfferParams> = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}

export class BonusOffer {
  id: number = null;
  name: string = null;
  description: string;
  state: string = null;
  activated: moment.Moment = null;
  cleared: moment.Moment = null;
  expired: moment.Moment = null;
  pointsUsed: number = null;
  pointsMax: number = null;
  pointsRequired: number = null;
  pointsAvailable: number = null;
  expires: moment.Moment = null;
  bonusAmount: number = null;
  bonusFraction: number = null;
  wageringReq: number = null;
  wagered: number = null;
  wageringOrder: number = null;
  bonusAmountMax: number = null;

  constructor(obj: Partial<BonusOffer> = {}) {
    if (obj) {
      for (const key in obj) {
        if (this.hasOwnProperty(key)) {
          this[key] = obj[key];
        }
      }
    }
  }
}
