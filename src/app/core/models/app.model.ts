// Copyright 2021 Bejoynd AB, Licensed under Apache-2.0
export interface LocalesMenuModel {
  support?: string;
  bonuses?: string;
  registration?: string;
}

export interface LocalesAgeValidationModel {
  accept?: string;
  decline?: string;
  question?: string;
}

export interface LocalesTableModel {
  deposits?: string;
  withdrawals?: string;
}

export interface LocalesModel {
  menu: LocalesMenuModel;
  ageValidation: LocalesAgeValidationModel;
  table: LocalesTableModel;
}

export class Country {
  code: string;
  name: string;

  constructor(object: any) {
    Object.assign(this, object);
  }
}
