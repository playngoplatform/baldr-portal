export const environment = {
  production: true,
  baseHref: '/',
  apiUrl: 'https://some-url.com/api'
};
