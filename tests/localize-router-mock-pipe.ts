import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'localize'})
export class LocalizeRouterMockPipe implements PipeTransform {
  transform(value: number): number {
    return value;
  }
}
