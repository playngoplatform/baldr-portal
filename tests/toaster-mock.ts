import {Component, NgModule} from '@angular/core';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@Component({
  selector: 'test-toaster-component',
  template: '<toaster-container></toaster-container>',
})
export class TestToasterComponent {
  toasterService: ToasterService;

  constructor(toasterService: ToasterService) {
    this.toasterService = toasterService;
  }
}

@NgModule({
  imports: [ToasterModule.forRoot(), BrowserAnimationsModule],
  declarations: [TestToasterComponent]
})
export class TestToasterComponentModule {}
